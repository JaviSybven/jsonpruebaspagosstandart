class dataPruebas {

  dataQueMeDejoLoco1() {
    return {
      "accion": "APROCESAR",
      "campoMoneda": "",
      "campoMonto": "",
      "componentes": [
        {
          "tipo": "label",
          "nombre": "",
          "atributos": [
            {
              "max": "",
              "parametro": "",
              "values": [],
              "secureTextEntry": "",
              "titulo": "Seleccione la deuda a ser pagada:",
              "nombre": "",
              "maxTextLength": "",
              "formatCaracterSet": "",
              "multipleSeleccion": "",
              "camposDependientes": [],
              "minTextLength": "",
              "min": "",
              "idPtnConfirmacion": "Seleccione la deuda a ser pagada:",
              "selectDependienteParametro": "",
              "restrictCharactersSet": "",
              "dependiente": "false",
              "placeholder": "",
              "textInputMode": ""
            }
          ]
        },
        {
          "tipo": "table",
          "nombre": "",
          "atributos": [
            {
              "max": "",
              "parametro": "seleccion",
              "values": [
                {
                  "lblV0": {
                    "parametro": "nro",
                    "text": "5775-1",
                    "isVisible": "true"
                  },
                  "lblV1": {
                    "parametro": "monto",
                    "text": "206.00",
                    "isVisible": "true"
                  },
                  "lblT0": {
                    "parametro": "",
                    "text": "Nro",
                    "isVisible": "true"
                  },
                  "lblV2": {
                    "parametro": "periodo",
                    "text": "null",
                    "isVisible": "false"
                  },
                  "lblT1": {
                    "parametro": "",
                    "text": "Monto",
                    "isVisible": "true"
                  },
                  "lblV3": {
                    "parametro": "nombre",
                    "text": "GONZALES R,FRANCISCO",
                    "isVisible": "true"
                  },
                  "lblT2": {
                    "parametro": "",
                    "text": "Periodo",
                    "isVisible": "false"
                  },
                  "lblV4": {
                    "parametro": "direccion",
                    "text": "R GUTIERREZ 882",
                    "isVisible": "true"
                  },
                  "lblT3": {
                    "parametro": "",
                    "text": "Nombre",
                    "isVisible": "true"
                  },
                  "lblV5": {
                    "parametro": "tipoBusqueda",
                    "text": "5775-1",
                    "isVisible": "false"
                  },
                  "lblT4": {
                    "parametro": "",
                    "text": "Direccion",
                    "isVisible": "true"
                  },
                  "lblV6": {
                    "parametro": "unic_value_string",
                    "text": "{\"mapVar\":{\"nro\":\"5775-1\",\"periodo\":\"null\",\"monto\":\"206.00\"},\"nombre\":[\"nombre\",\"dirección\",\"tipoBusqueda\"],\"valor\":[\"GONZALES R,FRANCISCO\",\"\",\"5775-1\"]}",
                    "isVisible": "false"
                  },
                  "lblT5": {
                    "parametro": "",
                    "text": "Búsqueda",
                    "isVisible": "false"
                  },
                  "lblT6": {
                    "parametro": "",
                    "text": "unic_value_string",
                    "isVisible": "false"
                  }
                }
              ],
              "secureTextEntry": "",
              "titulo": "Facturas",
              "nombre": "",
              "maxTextLength": "",
              "formatCaracterSet": "",
              "multipleSeleccion": "true",
              "camposDependientes": [],
              "minTextLength": "",
              "min": "",
              "idPtnConfirmacion": "Facturas",
              "selectDependienteParametro": "",
              "restrictCharactersSet": "",
              "dependiente": "true",
              "placeholder": "Seleccionar Facturas",
              "textInputMode": "",
              "accion": "APROCESAR"
            }
          ]
        }
      ],
      "tipoServicio": "1",
      "monto": "monto",
      "pasoActual": 5,
      "envioFactura": "false",
      "idService": "10",
      "moneda": "BOB",
      "pasosTotales": 5,
      "nombre": "Deudas"
    }
  }

  dataRespaldoObjetoServicio() {

    return [

      {
        paso: 1,
        data: {
          "accion": "ACTION01",
          "idService": "1",
          "pasosTotales": "6",
          "pasoActual": "1",
          "envioFactura": "false",
          "moneda": "BOB",
          "monto": "monto",
          "campoMoneda": null,
          "campoMonto": null,
          "nombre": "",
          "tipoServicio": "1",
          "componentes": [
            {
              "nombre": "",
              "tipo": "table",
              "atributos": [
                {
                  "dependiente": "",
                  "camposDependientes": [],
                  "textInputMode": "",
                  "parametro": "seleccione",
                  "maxTextLength": "",
                  "idPtnConfirmacion": "Categoria",
                  "minTextLength": "",
                  "restrictCharactersSet": "",
                  "formatCaracterSet": "",
                  "placeholder": "",
                  "secureTextEntry": "",
                  "selectDependienteParametro": "",
                  "values": [{

                    "lblT0": { "text": "Nombre", "isVisible": "true" },

                    "lblV0": { "text": "Servicios básicos", "parametro": "codCuenta", "isVisible": "true" },

                    "lblT1": { "text": "Descripcion", "isVisible": "true" },

                    "lblV1": { "text": "Servicios básicos: electricidad, agua, gas, etc.", "parametro": "descripcion", "isVisible": "true" },

                    "lblT2": { "text": "idCategoria", "isVisible": "false" },

                    "lblV2": { "text": "1", "parametro": "idCategoria", "isVisible": "false" }
                  },
                  {

                    "lblT0": { "text": "Nombre", "isVisible": "true" },

                    "lblV0": { "text": "Telecomunicaciones", "parametro": "codCuenta", "isVisible": "true" },

                    "lblT1": { "text": "Descripcion", "isVisible": "true" },

                    "lblV1": { "text": "Servicios de Telefonía, Internet, Cable, etc", "parametro": "descripcion", "isVisible": "true" },

                    "lblT2": { "text": "idCategoria", "isVisible": "false" },

                    "lblV2": { "text": "2", "parametro": "idCategoria", "isVisible": "false" }
                  },
                  {

                    "lblT0": { "text": "Nombre", "isVisible": "true" },

                    "lblV0": { "text": "Educación", "parametro": "codCuenta", "isVisible": "true" },

                    "lblT1": { "text": "Descripcion", "isVisible": "true" },

                    "lblV1": { "text": "Servicios educativos: colegios, universidades e Institutos", "parametro": "descripcion", "isVisible": "true" },

                    "lblT2": { "text": "idCategoria", "isVisible": "false" },

                    "lblV2": { "text": "3", "parametro": "idCategoria", "isVisible": "false" }
                  },
                  {

                    "lblT0": { "text": "Nombre", "isVisible": "true" },

                    "lblV0": { "text": "Banca", "parametro": "codCuenta", "isVisible": "true" },

                    "lblT1": { "text": "Descripcion", "isVisible": "true" },

                    "lblV1": { "text": "Servicios bancarios: pago de créditos, impuestos, etc", "parametro": "descripcion", "isVisible": "true" },

                    "lblT2": { "text": "idCategoria", "isVisible": "false" },

                    "lblV2": { "text": "4", "parametro": "idCategoria", "isVisible": "false" }
                  }
                  ],
                  "multipleSeleccion": "false",
                  "titulo": "Seleccione una Categoria ",
                }
              ]
            }
          ]
        }
      }

    ]

  }

  pruebasNuevoSimple() {

    return [{ "paso": 1, "tipo": "table_list", "idComp": "table_list1", "accion": "AEMPRESAS", "data": { "nombre": "seleccionC", "valor": [{ "select": [{ "nombre": "id", "valor": "2", "visible": false, "tipo": "table_list", "tituloVista": "Categoría", "titulo": "Servicios básicos: electricidad, agua, gas, etc." }] }] }, "dataVisible": {} }, { "paso": 2, "tipo": "table_list", "idComp": "table_list5", "accion": "ASERVICIOS", "data": { "nombre": "seleccionE", "valor": [{ "select": [{ "nombre": "id", "valor": "155", "visible": false, "tipo": "table_list", "tituloVista": "Empresa", "titulo": "ELFEC." }] }] }, "dataVisible": { "lblV0": { "parametro": "", "text": "ELFEC", "isVisible": "true" }, "lblV1": { "parametro": "id", "text": "155", "isVisible": "false" }, "lblT0": { "parametro": "", "text": "Empresa", "isVisible": "false" }, "lblT1": { "parametro": "", "text": "ELFEC.", "isVisible": "false" } } }, { "paso": 3, "tipo": "table_list", "idComp": "table_list6", "accion": "ABUSQUEDA", "data": { "nombre": "seleccionS", "valor": [{ "select": [{ "nombre": "id", "valor": "12", "visible": false, "tipo": "table_list", "tituloVista": "Servicio", "titulo": "ELFEC" }] }] }, "dataVisible": { "lblV0": { "parametro": "", "text": "POSTPAGO ELFEC", "isVisible": "true" }, "lblV1": { "parametro": "id", "text": "12", "isVisible": "false" }, "lblT0": { "parametro": "", "text": "Servicio", "isVisible": "false" }, "lblT1": { "parametro": "", "text": "ELFEC", "isVisible": "false" } } }, { "paso": 4, "tipo": "texbox", "idComp": "texbox2", "accion": "ADEUDAS", "data": [{ "nombre": "codAcceso2", "valor": "691842", "tituloVista": "Nro. NUS" }] }, { "paso": 4, "tipo": "texbox", "idComp": "texbox3", "accion": "ADEUDAS", "data": [{ "nombre": "codAcceso3", "valor": "2200703000", "tituloVista": "Nro. Cuenta" }] }, { "paso": 6, "tipo": "ListMultiple", "idComp": "ListMultiple2", "accion": "APROCESAR", "data": { "nombre": "seleccion", "valor": [{ "select": [{ "nombre": "pIdCbte", "valor": "111107973", "visible": true, "tipo": "ListMultiple", "tituloVista": "Facturas", "titulo": "Id. Cbte" }, { "nombre": "pTotal", "valor": "17.40", "visible": false, "tipo": "ListMultiple", "tituloVista": "Facturas", "titulo": "Total" }, { "nombre": "unic_value_string", "valor": "{\"mapVar\":{\"nro\":\"312294\",\"periodo\":\"08\\/2019\",\"monto\":\"17.40\"},\"nombre\":[\"pNus\",\"pCuenta\",\"pCategoria\",\"pAnio\",\"pMes\",\"pVencimiento\",\"pIdCbte\",\"pNroFactura\",\"pTotal\"],\"valor\":[\"691842\",\"2200703000\",\"RESPB\",\"2019\",\"8\",\"24\\/09\\/2019\",\"111107973\",\"312294\",\"17.40\"]}", "visible": false, "tipo": "ListMultiple", "tituloVista": "Facturas", "titulo": "unic_value_string" }, { "nombre": "pAnio", "valor": "2019", "visible": false, "tipo": "ListMultiple", "tituloVista": "Facturas", "titulo": "Año" }, { "nombre": "pNroFactura", "valor": "312294", "visible": false, "tipo": "ListMultiple", "tituloVista": "Facturas", "titulo": "Nro. Factura" }, { "nombre": "pVencimiento", "valor": "24/09/2019", "visible": true, "tipo": "ListMultiple", "tituloVista": "Facturas", "titulo": "Vencimiento" }, { "nombre": "pNus", "valor": "691842", "visible": false, "tipo": "ListMultiple", "tituloVista": "Facturas", "titulo": "NUS" }, { "nombre": "monto", "valor": "17.40", "visible": true, "tipo": "ListMultiple", "tituloVista": "Facturas", "titulo": "Monto" }, { "nombre": "pCategoria", "valor": "RESPB", "visible": false, "tipo": "ListMultiple", "tituloVista": "Facturas", "titulo": "Categoria" }, { "nombre": "nro", "valor": "312294", "visible": true, "tipo": "ListMultiple", "tituloVista": "Facturas", "titulo": "unic_value_string" }, { "nombre": "pMes", "valor": "8", "visible": false, "tipo": "ListMultiple", "tituloVista": "Facturas", "titulo": "Mes" }, { "nombre": "periodo", "valor": "08/2019", "visible": true, "tipo": "ListMultiple", "tituloVista": "Facturas", "titulo": "Periodo" }, { "nombre": "pCuenta", "valor": "2200703000", "visible": false, "tipo": "ListMultiple", "tituloVista": "Facturas", "titulo": "Cuenta" }] }] } }]


  }

  ghostBeta2() {

    return [
      {
        "paso": 1,
        "tipo": "table_list",
        "idComp": "table_list1",
        "accion": "ADEUDAS",
        "data": {
          "nombre": "seleccion",
          "valor": [
            {
              "select": [
                {
                  "nombre": "modalidadDePago",
                  "valor": "",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "Cuentas",
                  "titulo": "Modalidad De Pago"
                },
                {
                  "nombre": "chalequero",
                  "valor": "",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "Cuentas",
                  "titulo": "Chalequero"
                },
                {
                  "nombre": "sucursal",
                  "valor": "",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "Cuentas",
                  "titulo": "Sucursal"
                },
                {
                  "nombre": "estado",
                  "valor": "AC",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "Cuentas",
                  "titulo": "Estado"
                },
                {
                  "nombre": "descripcionPlanDeConsumo",
                  "valor": "Sin Limite 100",
                  "visible": true,
                  "tipo": "table_list",
                  "tituloVista": "Cuentas",
                  "titulo": "Descripci�n Plan De Consumo"
                },
                {
                  "nombre": "localidad",
                  "valor": "",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "Cuentas",
                  "titulo": "Localidad"
                },
                {
                  "nombre": "planDeConsumo",
                  "valor": "HIB",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "Cuentas",
                  "titulo": "Plan De Consumo"
                },
                {
                  "nombre": "contrato",
                  "valor": "4155049",
                  "visible": true,
                  "tipo": "table_list",
                  "tituloVista": "Cuentas",
                  "titulo": "Contrato"
                },
                {
                  "nombre": "nombreEnFactura",
                  "valor": "CARLOS JOSE RADOSEVIC GIANELLA ",
                  "visible": true,
                  "tipo": "table_list",
                  "tituloVista": "Cuentas",
                  "titulo": "Nombre En Factura"
                },
                {
                  "nombre": "descripcion",
                  "valor": "Sin Limite 100",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "Cuentas",
                  "titulo": "Descripci�n"
                },
                {
                  "nombre": "finalDeRegistro",
                  "valor": "",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "Cuentas",
                  "titulo": "Final De Registro"
                },
                {
                  "nombre": "nit",
                  "valor": "                    ",
                  "visible": true,
                  "tipo": "table_list",
                  "tituloVista": "Cuentas",
                  "titulo": "NIT"
                },
                {
                  "nombre": "usuarioPortador",
                  "valor": "",
                  "visible": true,
                  "tipo": "table_list",
                  "tituloVista": "Cuentas",
                  "titulo": "Usuario Portador"
                },
                {
                  "nombre": "unic_value_string",
                  "valor": "{\"mapVar\":{\"codCuenta\":\"77623397\"},\"nombre\":[\"codAcceso2\"],\"valor\":[\"\"]}",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "Cuentas",
                  "titulo": "unic_value_string"
                },
                {
                  "nombre": "codCuenta",
                  "valor": "77623397",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "Cuentas",
                  "titulo": "Cod. Cuenta"
                },
                {
                  "nombre": "descripcionEstado",
                  "valor": "",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "Cuentas",
                  "titulo": "Descripci�n Estado"
                },
                {
                  "nombre": "cliente",
                  "valor": "1767783",
                  "visible": true,
                  "tipo": "table_list",
                  "tituloVista": "Cuentas",
                  "titulo": "Cliente"
                },
                {
                  "nombre": "secuencial",
                  "valor": "ICJ37JIQGD",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "Cuentas",
                  "titulo": "Secuencial"
                },
                {
                  "nombre": "razonSocial",
                  "valor": "CARLOS JOSE RADOSEVIC GIANELLA ",
                  "visible": true,
                  "tipo": "table_list",
                  "tituloVista": "Cuentas",
                  "titulo": "Raz�n Social"
                }
              ]
            }
          ]
        },
        "dataVisible": {
          "lblT0": {
            "text": "Cod. Cuenta",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV0": {
            "text": "77623397",
            "parametro": "codCuenta",
            "isVisible": "false"
          },
          "lblT1": {
            "text": "Descripci�n",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV1": {
            "text": "Sin Limite 100",
            "parametro": "descripcion",
            "isVisible": "false"
          },
          "lblT2": {
            "text": "Cliente",
            "parametro": "",
            "isVisible": "true"
          },
          "lblV2": {
            "text": "1767783",
            "parametro": "cliente",
            "isVisible": "true"
          },
          "lblT3": {
            "text": "Contrato",
            "parametro": "",
            "isVisible": "true"
          },
          "lblV3": {
            "text": "4155049",
            "parametro": "contrato",
            "isVisible": "true"
          },
          "lblT4": {
            "text": "Raz�n Social",
            "parametro": "",
            "isVisible": "true"
          },
          "lblV4": {
            "text": "CARLOS JOSE RADOSEVIC GIANELLA ",
            "parametro": "razonSocial",
            "isVisible": "true"
          },
          "lblT5": {
            "text": "Usuario Portador",
            "parametro": "",
            "isVisible": "true"
          },
          "lblV5": {
            "text": "",
            "parametro": "usuarioPortador",
            "isVisible": "true"
          },
          "lblT6": {
            "text": "Estado",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV6": {
            "text": "AC",
            "parametro": "estado",
            "isVisible": "false"
          },
          "lblT7": {
            "text": "Descripci�n Estado",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV7": {
            "text": "",
            "parametro": "descripcionEstado",
            "isVisible": "false"
          },
          "lblT8": {
            "text": "Plan De Consumo",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV8": {
            "text": "HIB",
            "parametro": "planDeConsumo",
            "isVisible": "false"
          },
          "lblT9": {
            "text": "Descripci�n Plan De Consumo",
            "parametro": "",
            "isVisible": "true"
          },
          "lblV9": {
            "text": "Sin Limite 100",
            "parametro": "descripcionPlanDeConsumo",
            "isVisible": "true"
          },
          "lblT10": {
            "text": "Chalequero",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV10": {
            "text": "",
            "parametro": "chalequero",
            "isVisible": "false"
          },
          "lblT11": {
            "text": "Modalidad De Pago",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV11": {
            "text": "",
            "parametro": "modalidadDePago",
            "isVisible": "false"
          },
          "lblT12": {
            "text": "Sucursal",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV12": {
            "text": "",
            "parametro": "sucursal",
            "isVisible": "false"
          },
          "lblT13": {
            "text": "Localidad",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV13": {
            "text": "",
            "parametro": "localidad",
            "isVisible": "false"
          },
          "lblT14": {
            "text": "NIT",
            "parametro": "",
            "isVisible": "true"
          },
          "lblV14": {
            "text": "                    ",
            "parametro": "nit",
            "isVisible": "true"
          },
          "lblT15": {
            "text": "Nombre En Factura",
            "parametro": "",
            "isVisible": "true"
          },
          "lblV15": {
            "text": "CARLOS JOSE RADOSEVIC GIANELLA ",
            "parametro": "nombreEnFactura",
            "isVisible": "true"
          },
          "lblT16": {
            "text": "Final De Registro",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV16": {
            "text": "",
            "parametro": "finalDeRegistro",
            "isVisible": "false"
          },
          "lblT17": {
            "text": "Secuencial",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV17": {
            "text": "ICJ37JIQGD",
            "parametro": "secuencial",
            "isVisible": "false"
          },
          "lblT18": {
            "text": "unic_value_string",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV18": {
            "text": "{\"mapVar\":{\"codCuenta\":\"77623397\"},\"nombre\":[\"codAcceso2\"],\"valor\":[\"\"]}",
            "parametro": "unic_value_string",
            "isVisible": "false"
          }
        }
      },
      {
        "paso": 2,
        "tipo": "ListMultiple",
        "idComp": "ListMultiple1",
        "accion": "APROCESAR",
        "data": {
          "nombre": "seleccion",
          "valor": [
            {
              "select": [
                {
                  "nombre": "estadoComprobante",
                  "valor": "PC",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Estado Comprobante"
                },
                {
                  "nombre": "saldoComprobante",
                  "valor": "100.0",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Saldo Comprobante"
                },
                {
                  "nombre": "cliente",
                  "valor": "1767783",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Cliente"
                },
                {
                  "nombre": "periodoFacturacion",
                  "valor": "06-2020",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Per�odo Facturaci�n"
                },
                {
                  "nombre": "montoComprobante",
                  "valor": "100.0",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Monto Comprobante"
                },
                {
                  "nombre": "contrato",
                  "valor": "4155049",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Contrato"
                },
                {
                  "nombre": "fechaVencimiento",
                  "valor": "Tue Jun 30 00:00:00 BOT 2020",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Fecha Vencimiento"
                },
                {
                  "nombre": "tipoComprobante",
                  "valor": "FCM",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Tipo Comprobante"
                },
                {
                  "nombre": "secuencial",
                  "valor": "ICJ37JIQGD",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Secuencial"
                },
                {
                  "nombre": "monto",
                  "valor": "100.00",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Monto"
                },
                {
                  "nombre": "unic_value_string",
                  "valor": "{\"mapVar\":{\"nro\":\"77623397\",\"periodo\":\"\",\"monto\":\"100.0\"},\"nombre\":[\"tipoComprobante\",\"serieComprobante\",\"numeroComprobante\",\"periodoFacturacion\",\"fechaEmision\",\"fechaVencimiento\",\"montoComprobante\",\"saldoComprobante\",\"estadoComprobante\",\"cliente\",\"contrato\",\"valorBusqueda\",\"secuencial\",\"monto\"],\"valor\":[\"FCM\",\"DC\",\"245627\",\"06-2020\",\"01\\/07\\/2020\",\"Tue Jun 30 00:00:00 BOT 2020\",\"100.0\",\"100.0\",\"PC\",\"1767783\",\"4155049\",\"77623397\",\"\",\"100.0\"]}",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "unic_value_string"
                },
                {
                  "nombre": "valorBusqueda",
                  "valor": "77623397",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Valor B�squeda"
                },
                {
                  "nombre": "numeroComprobante",
                  "valor": "245627",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Nro. Comprobante"
                },
                {
                  "nombre": "nro",
                  "valor": "77623397",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "unic_value_string"
                },
                {
                  "nombre": "fechaEmision",
                  "valor": "01/07/2020",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Fecha Emisi�n"
                },
                {
                  "nombre": "periodo",
                  "valor": "",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Periodo"
                },
                {
                  "nombre": "serieComprobante",
                  "valor": "DC",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Serie Comprobante"
                }
              ]
            },
            {
              "select": [
                {
                  "nombre": "estadoComprobante",
                  "valor": "PC",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Estado Comprobante"
                },
                {
                  "nombre": "saldoComprobante",
                  "valor": "100.0",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Saldo Comprobante"
                },
                {
                  "nombre": "cliente",
                  "valor": "1767783",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Cliente"
                },
                {
                  "nombre": "periodoFacturacion",
                  "valor": "06-2020",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Per�odo Facturaci�n"
                },
                {
                  "nombre": "montoComprobante",
                  "valor": "100.0",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Monto Comprobante"
                },
                {
                  "nombre": "contrato",
                  "valor": "4155049",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Contrato"
                },
                {
                  "nombre": "fechaVencimiento",
                  "valor": "Fri Jul 31 00:00:00 BOT 2020",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Fecha Vencimiento"
                },
                {
                  "nombre": "tipoComprobante",
                  "valor": "FCM",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Tipo Comprobante"
                },
                {
                  "nombre": "secuencial",
                  "valor": "ICJ37JIQGD",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Secuencial"
                },
                {
                  "nombre": "monto",
                  "valor": "100.00",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Monto"
                },
                {
                  "nombre": "unic_value_string",
                  "valor": "{\"mapVar\":{\"nro\":\"77623397\",\"periodo\":\"\",\"monto\":\"100.0\"},\"nombre\":[\"tipoComprobante\",\"serieComprobante\",\"numeroComprobante\",\"periodoFacturacion\",\"fechaEmision\",\"fechaVencimiento\",\"montoComprobante\",\"saldoComprobante\",\"estadoComprobante\",\"cliente\",\"contrato\",\"valorBusqueda\",\"secuencial\",\"monto\"],\"valor\":[\"FCM\",\"DC\",\"373699\",\"06-2020\",\"01\\/08\\/2020\",\"Fri Jul 31 00:00:00 BOT 2020\",\"100.0\",\"100.0\",\"PC\",\"1767783\",\"4155049\",\"77623397\",\"\",\"100.0\"]}",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "unic_value_string"
                },
                {
                  "nombre": "valorBusqueda",
                  "valor": "77623397",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Valor B�squeda"
                },
                {
                  "nombre": "numeroComprobante",
                  "valor": "373699",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Nro. Comprobante"
                },
                {
                  "nombre": "nro",
                  "valor": "77623397",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "unic_value_string"
                },
                {
                  "nombre": "fechaEmision",
                  "valor": "01/08/2020",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Fecha Emisi�n"
                },
                {
                  "nombre": "periodo",
                  "valor": "",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Periodo"
                },
                {
                  "nombre": "serieComprobante",
                  "valor": "DC",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas",
                  "titulo": "Serie Comprobante"
                }
              ]
            }
          ]
        }
      }
    ]

  }

  ghostBeta() {

    return [
      {
        "paso": 1,
        "tipo": "table_list",
        "idComp": "table_list1",
        "accion": "ADEUDAS",
        "data": {
          "nombre": "seleccion",
          "valor": [
            {
              "select": [
                {
                  "nombre": "modalidadDePago",
                  "valor": "",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "Modalidad De Pago"
                },
                {
                  "nombre": "chalequero",
                  "valor": "",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "Chalequero"
                },
                {
                  "nombre": "sucursal",
                  "valor": "",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "Sucursal"
                },
                {
                  "nombre": "estado",
                  "valor": "AC",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "Estado"
                },
                {
                  "nombre": "descripcionPlanDeConsumo",
                  "valor": "Sin Limite 100",
                  "visible": true,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "Descripci�n Plan De Consumo"
                },
                {
                  "nombre": "localidad",
                  "valor": "",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "Localidad"
                },
                {
                  "nombre": "planDeConsumo",
                  "valor": "HIB",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "Plan De Consumo"
                },
                {
                  "nombre": "contrato",
                  "valor": "4155049",
                  "visible": true,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "Contrato"
                },
                {
                  "nombre": "nombreEnFactura",
                  "valor": "CARLOS JOSE RADOSEVIC GIANELLA ",
                  "visible": true,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "Nombre En Factura"
                },
                {
                  "nombre": "descripcion",
                  "valor": "Sin Limite 100",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "Descripci�n"
                },
                {
                  "nombre": "finalDeRegistro",
                  "valor": "",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "Final De Registro"
                },
                {
                  "nombre": "nit",
                  "valor": "                    ",
                  "visible": true,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "NIT"
                },
                {
                  "nombre": "usuarioPortador",
                  "valor": "",
                  "visible": true,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "Usuario Portador"
                },
                {
                  "nombre": "unic_value_string",
                  "valor": "{\"mapVar\":{\"codCuenta\":\"77623397\"},\"nombre\":[\"codAcceso2\"],\"valor\":[\"\"]}",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "unic_value_string"
                },
                {
                  "nombre": "codCuenta",
                  "valor": "77623397",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "Cod. Cuenta"
                },
                {
                  "nombre": "descripcionEstado",
                  "valor": "",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "Descripci�n Estado"
                },
                {
                  "nombre": "cliente",
                  "valor": "1767783",
                  "visible": true,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "Cliente"
                },
                {
                  "nombre": "secuencial",
                  "valor": "IGZR6WA8MY",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "Secuencial"
                },
                {
                  "nombre": "razonSocial",
                  "valor": "CARLOS JOSE RADOSEVIC GIANELLA ",
                  "visible": true,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "Raz�n Social"
                }
              ]
            }
          ]
        },
        "dataVisible": {
          "lblT0": {
            "text": "Cod. Cuenta",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV0": {
            "text": "77623397",
            "parametro": "codCuenta",
            "isVisible": "false"
          },
          "lblT1": {
            "text": "Descripci�n",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV1": {
            "text": "Sin Limite 100",
            "parametro": "descripcion",
            "isVisible": "false"
          },
          "lblT2": {
            "text": "Cliente",
            "parametro": "",
            "isVisible": "true"
          },
          "lblV2": {
            "text": "1767783",
            "parametro": "cliente",
            "isVisible": "true"
          },
          "lblT3": {
            "text": "Contrato",
            "parametro": "",
            "isVisible": "true"
          },
          "lblV3": {
            "text": "4155049",
            "parametro": "contrato",
            "isVisible": "true"
          },
          "lblT4": {
            "text": "Raz�n Social",
            "parametro": "",
            "isVisible": "true"
          },
          "lblV4": {
            "text": "CARLOS JOSE RADOSEVIC GIANELLA ",
            "parametro": "razonSocial",
            "isVisible": "true"
          },
          "lblT5": {
            "text": "Usuario Portador",
            "parametro": "",
            "isVisible": "true"
          },
          "lblV5": {
            "text": "",
            "parametro": "usuarioPortador",
            "isVisible": "true"
          },
          "lblT6": {
            "text": "Estado",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV6": {
            "text": "AC",
            "parametro": "estado",
            "isVisible": "false"
          },
          "lblT7": {
            "text": "Descripci�n Estado",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV7": {
            "text": "",
            "parametro": "descripcionEstado",
            "isVisible": "false"
          },
          "lblT8": {
            "text": "Plan De Consumo",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV8": {
            "text": "HIB",
            "parametro": "planDeConsumo",
            "isVisible": "false"
          },
          "lblT9": {
            "text": "Descripci�n Plan De Consumo",
            "parametro": "",
            "isVisible": "true"
          },
          "lblV9": {
            "text": "Sin Limite 100",
            "parametro": "descripcionPlanDeConsumo",
            "isVisible": "true"
          },
          "lblT10": {
            "text": "Chalequero",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV10": {
            "text": "",
            "parametro": "chalequero",
            "isVisible": "false"
          },
          "lblT11": {
            "text": "Modalidad De Pago",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV11": {
            "text": "",
            "parametro": "modalidadDePago",
            "isVisible": "false"
          },
          "lblT12": {
            "text": "Sucursal",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV12": {
            "text": "",
            "parametro": "sucursal",
            "isVisible": "false"
          },
          "lblT13": {
            "text": "Localidad",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV13": {
            "text": "",
            "parametro": "localidad",
            "isVisible": "false"
          },
          "lblT14": {
            "text": "NIT",
            "parametro": "",
            "isVisible": "true"
          },
          "lblV14": {
            "text": "                    ",
            "parametro": "nit",
            "isVisible": "true"
          },
          "lblT15": {
            "text": "Nombre En Factura",
            "parametro": "",
            "isVisible": "true"
          },
          "lblV15": {
            "text": "CARLOS JOSE RADOSEVIC GIANELLA ",
            "parametro": "nombreEnFactura",
            "isVisible": "true"
          },
          "lblT16": {
            "text": "Final De Registro",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV16": {
            "text": "",
            "parametro": "finalDeRegistro",
            "isVisible": "false"
          },
          "lblT17": {
            "text": "Secuencial",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV17": {
            "text": "IGZR6WA8MY",
            "parametro": "secuencial",
            "isVisible": "false"
          },
          "lblT18": {
            "text": "unic_value_string",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV18": {
            "text": "{\"mapVar\":{\"codCuenta\":\"77623397\"},\"nombre\":[\"codAcceso2\"],\"valor\":[\"\"]}",
            "parametro": "unic_value_string",
            "isVisible": "false"
          }
        }
      },
      {
        "paso": 2,
        "tipo": "ListMultiple",
        "idComp": "ListMultiple1",
        "accion": "APROCESAR",
        "data": {
          "nombre": "seleccion",
          "valor": [
            {
              "select": [
                {
                  "nombre": "estadoComprobante",
                  "valor": "PC",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Estado Comprobante"
                },
                {
                  "nombre": "saldoComprobante",
                  "valor": "100.0",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Saldo Comprobante"
                },
                {
                  "nombre": "cliente",
                  "valor": "1767783",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Cliente"
                },
                {
                  "nombre": "periodoFacturacion",
                  "valor": "06-2020",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Per�odo Facturaci�n"
                },
                {
                  "nombre": "montoComprobante",
                  "valor": "100.0",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Monto Comprobante"
                },
                {
                  "nombre": "contrato",
                  "valor": "4155049",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Contrato"
                },
                {
                  "nombre": "fechaVencimiento",
                  "valor": "Tue Jun 30 00:00:00 BOT 2020",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Fecha Vencimiento"
                },
                {
                  "nombre": "tipoComprobante",
                  "valor": "FCM",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Tipo Comprobante"
                },
                {
                  "nombre": "secuencial",
                  "valor": "IGZR6WA8MY",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Secuencial"
                },
                {
                  "nombre": "monto",
                  "valor": "100.00",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Monto"
                },
                {
                  "nombre": "unic_value_string",
                  "valor": "{\"mapVar\":{\"nro\":\"77623397\",\"periodo\":\"\",\"monto\":\"100.0\"},\"nombre\":[\"tipoComprobante\",\"serieComprobante\",\"numeroComprobante\",\"periodoFacturacion\",\"fechaEmision\",\"fechaVencimiento\",\"montoComprobante\",\"saldoComprobante\",\"estadoComprobante\",\"cliente\",\"contrato\",\"valorBusqueda\",\"secuencial\",\"monto\"],\"valor\":[\"FCM\",\"DC\",\"245627\",\"06-2020\",\"01\\/07\\/2020\",\"Tue Jun 30 00:00:00 BOT 2020\",\"100.0\",\"100.0\",\"PC\",\"1767783\",\"4155049\",\"77623397\",\"\",\"100.0\"]}",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "unic_value_string"
                },
                {
                  "nombre": "valorBusqueda",
                  "valor": "77623397",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Valor B�squeda"
                },
                {
                  "nombre": "numeroComprobante",
                  "valor": "245627",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Nro. Comprobante"
                },
                {
                  "nombre": "nro",
                  "valor": "77623397",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "unic_value_string"
                },
                {
                  "nombre": "fechaEmision",
                  "valor": "01/07/2020",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Fecha Emisi�n"
                },
                {
                  "nombre": "periodo",
                  "valor": "",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Periodo"
                },
                {
                  "nombre": "serieComprobante",
                  "valor": "DC",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Serie Comprobante"
                }
              ]
            },
            {
              "select": [
                {
                  "nombre": "estadoComprobante",
                  "valor": "PC",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Estado Comprobante"
                },
                {
                  "nombre": "saldoComprobante",
                  "valor": "100.0",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Saldo Comprobante"
                },
                {
                  "nombre": "cliente",
                  "valor": "1767783",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Cliente"
                },
                {
                  "nombre": "periodoFacturacion",
                  "valor": "06-2020",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Per�odo Facturaci�n"
                },
                {
                  "nombre": "montoComprobante",
                  "valor": "100.0",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Monto Comprobante"
                },
                {
                  "nombre": "contrato",
                  "valor": "4155049",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Contrato"
                },
                {
                  "nombre": "fechaVencimiento",
                  "valor": "Fri Jul 31 00:00:00 BOT 2020",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Fecha Vencimiento"
                },
                {
                  "nombre": "tipoComprobante",
                  "valor": "FCM",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Tipo Comprobante"
                },
                {
                  "nombre": "secuencial",
                  "valor": "IGZR6WA8MY",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Secuencial"
                },
                {
                  "nombre": "monto",
                  "valor": "100.00",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Monto"
                },
                {
                  "nombre": "unic_value_string",
                  "valor": "{\"mapVar\":{\"nro\":\"77623397\",\"periodo\":\"\",\"monto\":\"100.0\"},\"nombre\":[\"tipoComprobante\",\"serieComprobante\",\"numeroComprobante\",\"periodoFacturacion\",\"fechaEmision\",\"fechaVencimiento\",\"montoComprobante\",\"saldoComprobante\",\"estadoComprobante\",\"cliente\",\"contrato\",\"valorBusqueda\",\"secuencial\",\"monto\"],\"valor\":[\"FCM\",\"DC\",\"373699\",\"06-2020\",\"01\\/08\\/2020\",\"Fri Jul 31 00:00:00 BOT 2020\",\"100.0\",\"100.0\",\"PC\",\"1767783\",\"4155049\",\"77623397\",\"\",\"100.0\"]}",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "unic_value_string"
                },
                {
                  "nombre": "valorBusqueda",
                  "valor": "77623397",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Valor B�squeda"
                },
                {
                  "nombre": "numeroComprobante",
                  "valor": "373699",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Nro. Comprobante"
                },
                {
                  "nombre": "nro",
                  "valor": "77623397",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "unic_value_string"
                },
                {
                  "nombre": "fechaEmision",
                  "valor": "01/08/2020",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Fecha Emisi�n"
                },
                {
                  "nombre": "periodo",
                  "valor": "",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Periodo"
                },
                {
                  "nombre": "serieComprobante",
                  "valor": "DC",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Serie Comprobante"
                }
              ]
            }
          ]
        }
      }
    ];

  }

  dataResponseFlujoTigoConSusPaso1() {

    return { "objeto": [{ "accion": "ADEUDAS", "idService": "6", "pasosTotales": 2, "pasoActual": 1, "envioFactura": "false", "moneda": "", "monto": "", "campoMoneda": "", "campoMonto": "", "nombre": "Cuentas", "tipoServicio": "", "componentes": [{ "nombre": "", "tipo": "label", "atributos": [{ "idPtnConfirmacion": "Seleccione una cuenta:", "dependiente": "false", "camposDependientes": "[]", "textInputMode": "", "parametro": "", "min": "", "max": "", "maxTextLength": "", "minTextLength": "", "restrictCharactersSet": "", "formatCaracterSet": "", "placeholder": "", "secureTextEntry": "", "selectDependienteParametro": "", "values": "[]", "multipleSeleccion": "", "titulo": "Seleccione una cuenta:" }] }, { "nombre": "", "tipo": "table_list", "atributos": [{ "idPtnConfirmacion": "", "dependiente": "false", "camposDependientes": "[]", "textInputMode": "", "parametro": "seleccion", "min": "", "max": "", "maxTextLength": "", "minTextLength": "", "restrictCharactersSet": "", "formatCaracterSet": "", "placeholder": "Seleccionar Cuenta", "secureTextEntry": "", "selectDependienteParametro": "", "values": "[{\"lblT0\":{\"text\":\"Cod. Cuenta\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV0\":{\"text\":\"77623397\",\"parametro\":\"codCuenta\",\"isVisible\":\"false\"},\"lblT1\":{\"text\":\"Descripci�n\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV1\":{\"text\":\"Sin Limite 100\",\"parametro\":\"descripcion\",\"isVisible\":\"false\"},\"lblT2\":{\"text\":\"Cliente\",\"parametro\":\"\",\"isVisible\":\"true\"},\"lblV2\":{\"text\":\"1767783\",\"parametro\":\"cliente\",\"isVisible\":\"true\"},\"lblT3\":{\"text\":\"Contrato\",\"parametro\":\"\",\"isVisible\":\"true\"},\"lblV3\":{\"text\":\"4155049\",\"parametro\":\"contrato\",\"isVisible\":\"true\"},\"lblT4\":{\"text\":\"Raz�n Social\",\"parametro\":\"\",\"isVisible\":\"true\"},\"lblV4\":{\"text\":\"CARLOS JOSE RADOSEVIC GIANELLA \",\"parametro\":\"razonSocial\",\"isVisible\":\"true\"},\"lblT5\":{\"text\":\"Usuario Portador\",\"parametro\":\"\",\"isVisible\":\"true\"},\"lblV5\":{\"text\":\"\",\"parametro\":\"usuarioPortador\",\"isVisible\":\"true\"},\"lblT6\":{\"text\":\"Estado\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV6\":{\"text\":\"AC\",\"parametro\":\"estado\",\"isVisible\":\"false\"},\"lblT7\":{\"text\":\"Descripci�n Estado\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV7\":{\"text\":\"\",\"parametro\":\"descripcionEstado\",\"isVisible\":\"false\"},\"lblT8\":{\"text\":\"Plan De Consumo\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV8\":{\"text\":\"HIB\",\"parametro\":\"planDeConsumo\",\"isVisible\":\"false\"},\"lblT9\":{\"text\":\"Descripci�n Plan De Consumo\",\"parametro\":\"\",\"isVisible\":\"true\"},\"lblV9\":{\"text\":\"Sin Limite 100\",\"parametro\":\"descripcionPlanDeConsumo\",\"isVisible\":\"true\"},\"lblT10\":{\"text\":\"Chalequero\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV10\":{\"text\":\"\",\"parametro\":\"chalequero\",\"isVisible\":\"false\"},\"lblT11\":{\"text\":\"Modalidad De Pago\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV11\":{\"text\":\"\",\"parametro\":\"modalidadDePago\",\"isVisible\":\"false\"},\"lblT12\":{\"text\":\"Sucursal\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV12\":{\"text\":\"\",\"parametro\":\"sucursal\",\"isVisible\":\"false\"},\"lblT13\":{\"text\":\"Localidad\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV13\":{\"text\":\"\",\"parametro\":\"localidad\",\"isVisible\":\"false\"},\"lblT14\":{\"text\":\"NIT\",\"parametro\":\"\",\"isVisible\":\"true\"},\"lblV14\":{\"text\":\"                    \",\"parametro\":\"nit\",\"isVisible\":\"true\"},\"lblT15\":{\"text\":\"Nombre En Factura\",\"parametro\":\"\",\"isVisible\":\"true\"},\"lblV15\":{\"text\":\"CARLOS JOSE RADOSEVIC GIANELLA \",\"parametro\":\"nombreEnFactura\",\"isVisible\":\"true\"},\"lblT16\":{\"text\":\"Final De Registro\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV16\":{\"text\":\"\",\"parametro\":\"finalDeRegistro\",\"isVisible\":\"false\"},\"lblT17\":{\"text\":\"Secuencial\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV17\":{\"text\":\"I3GMV5QGU6\",\"parametro\":\"secuencial\",\"isVisible\":\"false\"},\"lblT18\":{\"text\":\"unic_value_string\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV18\":{\"text\":\"{\\\"mapVar\\\":{\\\"codCuenta\\\":\\\"77623397\\\"},\\\"nombre\\\":[\\\"codAcceso2\\\"],\\\"valor\\\":[\\\"\\\"]}\",\"parametro\":\"unic_value_string\",\"isVisible\":\"false\"}}]", "multipleSeleccion": "false", "titulo": "Cuentas" }] }] }], "codigo": "OK", "mensaje": "�xito en la Transacci�n", "opstatus": 0, "httpStatusCode": 200 };

  }

  dataResponseFlujoTigoConSusPaso2() {

    return {
      "codigo": "OK", "opstatus": 0, "objeto":
        [{ "accion": "APROCESAR", "campoMoneda": "", "campoMonto": "", "componentes": [{ "tipo": "label", "nombre": "", "atributos": [{ "max": "", "parametro": "", "values": [], "secureTextEntry": "", "titulo": "Seleccione la deuda a ser pagada:", "nombre": "", "maxTextLength": "", "formatCaracterSet": "", "multipleSeleccion": "", "camposDependientes": [], "minTextLength": "", "min": "", "idPtnConfirmacion": "Seleccione la deuda a ser pagada:", "selectDependienteParametro": "", "restrictCharactersSet": "", "dependiente": "false", "placeholder": "", "textInputMode": "" }] }, { "tipo": "table", "nombre": "", "atributos": [{ "max": "", "parametro": "seleccion", "values": [{ "lblT11": { "parametro": "", "text": "Estado Comprobante", "isVisible": "true" }, "lblV10": { "parametro": "saldoComprobante", "text": "100.0", "isVisible": "true" }, "lblT12": { "parametro": "", "text": "Cliente", "isVisible": "false" }, "lblT10": { "parametro": "", "text": "Saldo Comprobante", "isVisible": "true" }, "lblV13": { "parametro": "contrato", "text": "4155049", "isVisible": "false" }, "lblT15": { "parametro": "", "text": "Secuencial", "isVisible": "false" }, "lblV14": { "parametro": "valorBusqueda", "text": "77623397", "isVisible": "false" }, "lblT16": { "parametro": "", "text": "unic_value_string", "isVisible": "false" }, "lblV11": { "parametro": "estadoComprobante", "text": "PC", "isVisible": "true" }, "lblT13": { "parametro": "", "text": "Contrato", "isVisible": "false" }, "lblV12": { "parametro": "cliente", "text": "1767783", "isVisible": "false" }, "lblT14": { "parametro": "", "text": "Valor B�squeda", "isVisible": "false" }, "lblV15": { "parametro": "secuencial", "text": "I3GMV5QGU6", "isVisible": "false" }, "lblV16": { "parametro": "unic_value_string", "text": "{\"mapVar\":{\"nro\":\"77623397\",\"periodo\":\"\",\"monto\":\"100.0\"},\"nombre\":[\"tipoComprobante\",\"serieComprobante\",\"numeroComprobante\",\"periodoFacturacion\",\"fechaEmision\",\"fechaVencimiento\",\"montoComprobante\",\"saldoComprobante\",\"estadoComprobante\",\"cliente\",\"contrato\",\"valorBusqueda\",\"secuencial\",\"monto\"],\"valor\":[\"FCM\",\"DC\",\"245627\",\"06-2020\",\"01\\/07\\/2020\",\"Tue Jun 30 00:00:00 BOT 2020\",\"100.0\",\"100.0\",\"PC\",\"1767783\",\"4155049\",\"77623397\",\"\",\"100.0\"]}", "isVisible": "false" }, "lblV0": { "parametro": "nro", "text": "77623397", "isVisible": "false" }, "lblV1": { "parametro": "monto", "text": "100.00", "isVisible": "true" }, "lblT0": { "parametro": "", "text": "Factura", "isVisible": "false" }, "lblV2": { "parametro": "periodo", "text": "", "isVisible": "true" }, "lblT1": { "parametro": "", "text": "Monto", "isVisible": "true" }, "lblV3": { "parametro": "tipoComprobante", "text": "FCM", "isVisible": "true" }, "lblT2": { "parametro": "", "text": "Periodo", "isVisible": "true" }, "lblV4": { "parametro": "serieComprobante", "text": "DC", "isVisible": "true" }, "lblT3": { "parametro": "", "text": "Tipo Comprobante", "isVisible": "true" }, "lblV5": { "parametro": "numeroComprobante", "text": "245627", "isVisible": "true" }, "lblT4": { "parametro": "", "text": "Serie Comprobante", "isVisible": "true" }, "lblV6": { "parametro": "periodoFacturacion", "text": "06-2020", "isVisible": "false" }, "lblT5": { "parametro": "", "text": "Nro. Comprobante", "isVisible": "true" }, "lblV7": { "parametro": "fechaEmision", "text": "01/07/2020", "isVisible": "true" }, "lblT6": { "parametro": "", "text": "Per�odo Facturaci�n", "isVisible": "false" }, "lblV8": { "parametro": "fechaVencimiento", "text": "Tue Jun 30 00:00:00 BOT 2020", "isVisible": "true" }, "lblT7": { "parametro": "", "text": "Fecha Emisi�n", "isVisible": "true" }, "lblV9": { "parametro": "montoComprobante", "text": "100.0", "isVisible": "true" }, "lblT8": { "parametro": "", "text": "Fecha Vencimiento", "isVisible": "true" }, "lblT9": { "parametro": "", "text": "Monto Comprobante", "isVisible": "true" } }, { "lblT11": { "parametro": "", "text": "Estado Comprobante", "isVisible": "true" }, "lblV10": { "parametro": "saldoComprobante", "text": "100.0", "isVisible": "true" }, "lblT12": { "parametro": "", "text": "Cliente", "isVisible": "false" }, "lblT10": { "parametro": "", "text": "Saldo Comprobante", "isVisible": "true" }, "lblV13": { "parametro": "contrato", "text": "4155049", "isVisible": "false" }, "lblT15": { "parametro": "", "text": "Secuencial", "isVisible": "false" }, "lblV14": { "parametro": "valorBusqueda", "text": "77623397", "isVisible": "false" }, "lblT16": { "parametro": "", "text": "unic_value_string", "isVisible": "false" }, "lblV11": { "parametro": "estadoComprobante", "text": "PC", "isVisible": "true" }, "lblT13": { "parametro": "", "text": "Contrato", "isVisible": "false" }, "lblV12": { "parametro": "cliente", "text": "1767783", "isVisible": "false" }, "lblT14": { "parametro": "", "text": "Valor B�squeda", "isVisible": "false" }, "lblV15": { "parametro": "secuencial", "text": "I3GMV5QGU6", "isVisible": "false" }, "lblV16": { "parametro": "unic_value_string", "text": "{\"mapVar\":{\"nro\":\"77623397\",\"periodo\":\"\",\"monto\":\"100.0\"},\"nombre\":[\"tipoComprobante\",\"serieComprobante\",\"numeroComprobante\",\"periodoFacturacion\",\"fechaEmision\",\"fechaVencimiento\",\"montoComprobante\",\"saldoComprobante\",\"estadoComprobante\",\"cliente\",\"contrato\",\"valorBusqueda\",\"secuencial\",\"monto\"],\"valor\":[\"FCM\",\"DC\",\"373699\",\"06-2020\",\"01\\/08\\/2020\",\"Fri Jul 31 00:00:00 BOT 2020\",\"100.0\",\"100.0\",\"PC\",\"1767783\",\"4155049\",\"77623397\",\"\",\"100.0\"]}", "isVisible": "false" }, "lblV0": { "parametro": "nro", "text": "77623397", "isVisible": "false" }, "lblV1": { "parametro": "monto", "text": "100.00", "isVisible": "true" }, "lblT0": { "parametro": "", "text": "Factura", "isVisible": "false" }, "lblV2": { "parametro": "periodo", "text": "", "isVisible": "true" }, "lblT1": { "parametro": "", "text": "Monto", "isVisible": "true" }, "lblV3": { "parametro": "tipoComprobante", "text": "FCM", "isVisible": "true" }, "lblT2": { "parametro": "", "text": "Periodo", "isVisible": "true" }, "lblV4": { "parametro": "serieComprobante", "text": "DC", "isVisible": "true" }, "lblT3": { "parametro": "", "text": "Tipo Comprobante", "isVisible": "true" }, "lblV5": { "parametro": "numeroComprobante", "text": "373699", "isVisible": "true" }, "lblT4": { "parametro": "", "text": "Serie Comprobante", "isVisible": "true" }, "lblV6": { "parametro": "periodoFacturacion", "text": "06-2020", "isVisible": "false" }, "lblT5": { "parametro": "", "text": "Nro. Comprobante", "isVisible": "true" }, "lblV7": { "parametro": "fechaEmision", "text": "01/08/2020", "isVisible": "true" }, "lblT6": { "parametro": "", "text": "Per�odo Facturaci�n", "isVisible": "false" }, "lblV8": { "parametro": "fechaVencimiento", "text": "Fri Jul 31 00:00:00 BOT 2020", "isVisible": "true" }, "lblT7": { "parametro": "", "text": "Fecha Emisi�n", "isVisible": "true" }, "lblV9": { "parametro": "montoComprobante", "text": "100.0", "isVisible": "true" }, "lblT8": { "parametro": "", "text": "Fecha Vencimiento", "isVisible": "true" }, "lblT9": { "parametro": "", "text": "Monto Comprobante", "isVisible": "true" } }], "secureTextEntry": "", "titulo": "Facturas", "nombre": "", "maxTextLength": "", "formatCaracterSet": "", "multipleSeleccion": "true", "camposDependientes": [], "minTextLength": "", "min": "", "idPtnConfirmacion": "", "selectDependienteParametro": "", "restrictCharactersSet": "", "dependiente": "false", "placeholder": "Seleccionar Facturas", "textInputMode": "" }] }], "tipoServicio": "", "monto": "monto", "pasoActual": 2, "envioFactura": "false", "idService": "6", "moneda": "BOB", "pasosTotales": 2, "nombre": "Deudas" }], "mensaje": "�xito en la Transacci�n", "httpStatusCode": 200
    };

  }

  dataProbarValidacionObjetoResponseGetFormularioCorrecto() {

    return {
      "codigo": "OK",
      "opstatus": 0,
      "objeto": [
        {
          "accion": "ACTION01",
          "idService": "1",
          "pasosTotales": "6",
          "pasoActual": "1",
          "envioFactura": "false",
          "moneda": "BOB",
          "monto": "monto",
          "campoMoneda": null,
          "campoMonto": null,
          "nombre": "",
          "tipoServicio": "1",
          "componentes": [
            {
              "nombre": "",
              "tipo": "table",
              "atributos": [
                {
                  "dependiente": "",
                  "camposDependientes": [],
                  "textInputMode": "",
                  "parametro": "seleccione",
                  "maxTextLength": "",
                  "idPtnConfirmacion": "Categoria",
                  "minTextLength": "",
                  "restrictCharactersSet": "",
                  "formatCaracterSet": "",
                  "placeholder": "",
                  "secureTextEntry": "",
                  "selectDependienteParametro": "",
                  "values": [{

                    "lblT0": { "text": "Nombre", "isVisible": "true" },

                    "lblV0": { "text": "Servicios básicos", "parametro": "codCuenta", "isVisible": "true" },

                    "lblT1": { "text": "Descripcion", "isVisible": "true" },

                    "lblV1": { "text": "Servicios básicos: electricidad, agua, gas, etc.", "parametro": "descripcion", "isVisible": "true" },

                    "lblT2": { "text": "idCategoria", "isVisible": "false" },

                    "lblV2": { "text": "1", "parametro": "idCategoria", "isVisible": "false" }
                  },
                  {

                    "lblT0": { "text": "Nombre", "isVisible": "true" },

                    "lblV0": { "text": "Telecomunicaciones", "parametro": "codCuenta", "isVisible": "true" },

                    "lblT1": { "text": "Descripcion", "isVisible": "true" },

                    "lblV1": { "text": "Servicios de Telefonía, Internet, Cable, etc", "parametro": "descripcion", "isVisible": "true" },

                    "lblT2": { "text": "idCategoria", "isVisible": "false" },

                    "lblV2": { "text": "2", "parametro": "idCategoria", "isVisible": "false" }
                  },
                  {

                    "lblT0": { "text": "Nombre", "isVisible": "true" },

                    "lblV0": { "text": "Educación", "parametro": "codCuenta", "isVisible": "true" },

                    "lblT1": { "text": "Descripcion", "isVisible": "true" },

                    "lblV1": { "text": "Servicios educativos: colegios, universidades e Institutos", "parametro": "descripcion", "isVisible": "true" },

                    "lblT2": { "text": "idCategoria", "isVisible": "false" },

                    "lblV2": { "text": "3", "parametro": "idCategoria", "isVisible": "false" }
                  },
                  {

                    "lblT0": { "text": "Nombre", "isVisible": "true" },

                    "lblV0": { "text": "Banca", "parametro": "codCuenta", "isVisible": "true" },

                    "lblT1": { "text": "Descripcion", "isVisible": "true" },

                    "lblV1": { "text": "Servicios bancarios: pago de créditos, impuestos, etc", "parametro": "descripcion", "isVisible": "true" },

                    "lblT2": { "text": "idCategoria", "isVisible": "false" },

                    "lblV2": { "text": "4", "parametro": "idCategoria", "isVisible": "false" }
                  }
                  ],
                  "multipleSeleccion": "false",
                  "titulo": "Seleccione una Categoria ",
                }
              ]
            }
          ]
        }
      ],
      "mensaje": "Exito en la TransacciÃ³n",
      "httpStatusCode": 200
    };

  }

  dataFuncionTesting() {

    return [{ "paso": 1, "tipo": "table_list", "idComp": "table_list1", "accion": "AEMPRESAS", "data": { "nombre": "seleccionC", "valor": [{ "select": [{ "nombre": "id", "valor": "2", "visible": false, "tipo": "table_list", "tituloVista": "Categor&iacute;a", "titulo": "Descripci&oacute;n" }] }] } }, { "paso": 2, "tipo": "table_list", "idComp": "table_list2", "accion": "ASERVICIOS", "data": { "nombre": "seleccionE", "valor": [{ "select": [{ "nombre": "id", "valor": "7", "visible": false, "tipo": "table_list", "tituloVista": "Empresa", "titulo": "Descripci&oacute;n" }] }] } }, { "paso": 3, "tipo": "table_list", "idComp": "table_list3", "accion": "ABUSQUEDA", "data": { "nombre": "seleccionS", "valor": [{ "select": [{ "nombre": "id", "valor": "2", "visible": false, "tipo": "table_list", "tituloVista": "Servicio", "titulo": "Descripci&oacute;n" }] }] } }, { "paso": 4, "tipo": "table_list", "idComp": "table_list4", "accion": "ACUENTAS", "data": { "nombre": "valorBusqueda", "valor": [{ "select": [{ "nombre": "valorBusqueda", "valor": "3", "visible": false, "tipo": "table_list", "tituloVista": "", "titulo": "Tel&eacute;fono" }] }] } }, { "paso": 4, "tipo": "texbox", "idComp": "texbox1", "accion": "ACUENTAS", "data": [{ "nombre": "codAcceso", "valor": "1231", "tituloVista": "" }] }, { "paso": 5, "tipo": "ListSimple", "idComp": "ListSimple5", "accion": "ADEUDAS", "data": { "nombre": "Cuentas", "valor": [{ "select": [{ "nombre": "unic_value_string", "valor": "{\"mapVar\":{\"descripcion\":\"TROPICAL TOURS LTDA.\",\"idSeguimiento\":\"20067\",\"codCuenta\":\"30193917\"},\"nombre\":[\"montoDeuda\",\"tipoCuenta\",\"nombre\",\"cuenta\"],\"valor\":[\"53921.4\",\"TC_POST\",\"TROPICAL TOURS LTDA.\",\"30193917\"]}", "visible": false, "tipo": "ListSimple", "tituloVista": "", "titulo": "unic_value_string" }, { "nombre": "nombre", "valor": "TROPICAL TOURS LTDA.", "visible": true, "tipo": "ListSimple", "tituloVista": "", "titulo": "Nombre" }, { "nombre": "montoDeuda", "valor": "53,921.4", "visible": true, "tipo": "ListSimple", "tituloVista": "", "titulo": "Monto Deuda" }, { "nombre": "cuenta", "valor": "30193917", "visible": true, "tipo": "ListSimple", "tituloVista": "", "titulo": "Cuenta" }] }] } }, { "paso": 6, "tipo": "ListMultiple", "idComp": "ListMultiple1", "accion": "APROCESAR", "data": { "nombre": "Facturas", "valor": [{ "select": [{ "nombre": "unic_value_string", "valor": "{\"mapVar\":{\"razonSocial\":\"TROPICAL TOURS LTDA.\",\"monto\":\"24869.95\",\"periodo\":\"201311\",\"idSeguimiento\":\"20067\",\"nit\":\"1013389020\",\"nro\":\"45664793\"},\"nombre\":[\"periodo\",\"loteDosificacion\",\"numeroRenta\",\"agrupador\",\"montoAPagar\",\"tipoFactura\",\"razonSocial\",\"nit\",\"factura\",\"detalle\",\"cantidad\"],\"valor\":[\"201311\",\"S30 - T48 - B15 - A14\",\"88142 - 2100 - 2440 - 448\",\"1\",\"24869.95\",\"ENTEL - TELECEL MOVIL - BOLIVIATEL MOVIL - AXS MOVIL\",\"TROPICAL TOURS LTDA.\",\"1013389020\",\"45450135 - 45449165 - 45667247 - 45664793\",\"ENTEL = 24845.91 Bs - TELECEL MOVIL = 14.87 Bs - BOLIVIATEL MOVIL = 6.1 Bs - AXS MOVIL = 3.07 Bs\",\"4\"]}", "visible": false, "tipo": "ListMultiple", "tituloVista": "", "titulo": "unic_value_string" }, { "nombre": "monto", "valor": "24,869.95", "visible": true, "tipo": "ListMultiple", "tituloVista": "", "titulo": "Monto" }, { "nombre": "monto", "valor": "24,869.95", "tipo": "ListMultiple", "tituloVista": "", "titulo": "24,869.95" }, { "nombre": "factura", "valor": "45450135 - 45449165 - 45667247 - 45664793", "visible": true, "tipo": "ListMultiple", "tituloVista": "", "titulo": "Factura" }, { "nombre": "periodo", "valor": "201311", "visible": true, "tipo": "ListMultiple", "tituloVista": "", "titulo": "Periodo" }] }] } }];

  }

  dataProbarNuevoFiltro() {

    return [
      {
        "paso": 1,
        "tipo": "table_list",
        "idComp": "table_list1",
        "accion": "AEMPRESAS",
        "data": {
          "nombre": "seleccionC",
          "valor": [
            {
              "select": [
                {
                  "nombre": "id",
                  "valor": "2",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "Categoría",
                  "titulo": "Servicios básicos: electricidad, agua, gas, etc."
                }
              ]
            }
          ]
        },
        "dataVisible": {
          "lblT0": {
            "text": "Categoría",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV0": {
            "text": "Servicios básicos",
            "parametro": "",
            "isVisible": "true"
          },
          "lblT1": {
            "text": "Servicios básicos: electricidad, agua, gas, etc.",
            "parametro": "",
            "isVisible": "false"
          },
          "lblV1": {
            "text": "2",
            "parametro": "id",
            "isVisible": "false"
          }
        }
      },
      {
        "paso": 2,
        "tipo": "table_list",
        "idComp": "table_list2",
        "accion": "ASERVICIOS",
        "data": {
          "nombre": "seleccionE",
          "valor": [
            {
              "select": [
                {
                  "nombre": "id",
                  "valor": "6",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "Empresa",
                  "titulo": "SAGUAPAC."
                }
              ]
            }
          ]
        },
        "dataVisible": {
          "lblV0": {
            "parametro": "",
            "text": "SAGUAPAC",
            "isVisible": "true"
          },
          "lblV1": {
            "parametro": "id",
            "text": "6",
            "isVisible": "false"
          },
          "lblT0": {
            "parametro": "",
            "text": "Empresa",
            "isVisible": "false"
          },
          "lblT1": {
            "parametro": "",
            "text": "SAGUAPAC.",
            "isVisible": "false"
          }
        }
      },
      {
        "paso": 3,
        "tipo": "table_list",
        "idComp": "table_list3",
        "accion": "ABUSQUEDA",
        "data": {
          "nombre": "seleccionS",
          "valor": [
            {
              "select": [
                {
                  "nombre": "id",
                  "valor": "9",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "Servicio",
                  "titulo": "POSTPAGO"
                }
              ]
            }
          ]
        },
        "dataVisible": {
          "lblV0": {
            "parametro": "",
            "text": "POSTPAGO SAGUAPAC",
            "isVisible": "true"
          },
          "lblV1": {
            "parametro": "id",
            "text": "9",
            "isVisible": "false"
          },
          "lblT0": {
            "parametro": "",
            "text": "Servicio",
            "isVisible": "false"
          },
          "lblT1": {
            "parametro": "",
            "text": "POSTPAGO",
            "isVisible": "false"
          }
        }
      },
      {
        "paso": 4,
        "tipo": "texbox",
        "idComp": "texbox1",
        "accion": "ACUENTAS",
        "data": [
          {
            "nombre": "codAcceso",
            "valor": "912933000",
            "tituloVista": "Código Asociado"
          }
        ]
      },
      {
        "paso": 5,
        "tipo": "table_list",
        "idComp": "table_list4",
        "accion": "ADEUDAS",
        "data": {
          "nombre": "seleccion",
          "valor": [
            {
              "select": [
                {
                  "nombre": "mensaje",
                  "valor": "",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "Mensaje"
                },
                {
                  "nombre": "descripcion",
                  "valor": "CERVI . ANDREA",
                  "visible": true,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "Descripción"
                },
                {
                  "nombre": "codCuenta",
                  "valor": "912933000",
                  "visible": true,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "Cod. Cuenta"
                },
                {
                  "nombre": "direccion",
                  "valor": "",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "Dirección"
                },
                {
                  "nombre": "unic_value_string",
                  "valor": "{\"mapVar\":{\"codCuenta\":\"912933000\"},\"nombre\":[],\"valor\":[]}",
                  "visible": false,
                  "tipo": "table_list",
                  "tituloVista": "",
                  "titulo": "unic_value_string"
                }
              ]
            }
          ]
        },
        "dataVisible": {
          "lblV0": {
            "parametro": "codCuenta",
            "text": "912933000",
            "isVisible": "true"
          },
          "lblV1": {
            "parametro": "descripcion",
            "text": "CERVI . ANDREA",
            "isVisible": "true"
          },
          "lblT0": {
            "parametro": "",
            "text": "Cod. Cuenta",
            "isVisible": "true"
          },
          "lblV2": {
            "parametro": "direccion",
            "text": "",
            "isVisible": "false"
          },
          "lblT1": {
            "parametro": "",
            "text": "Descripción",
            "isVisible": "true"
          },
          "lblV3": {
            "parametro": "mensaje",
            "text": "",
            "isVisible": "false"
          },
          "lblT2": {
            "parametro": "",
            "text": "Dirección",
            "isVisible": "false"
          },
          "lblV4": {
            "parametro": "unic_value_string",
            "text": "{\"mapVar\":{\"codCuenta\":\"912933000\"},\"nombre\":[],\"valor\":[]}",
            "isVisible": "false"
          },
          "lblT3": {
            "parametro": "",
            "text": "Mensaje",
            "isVisible": "false"
          },
          "lblT4": {
            "parametro": "",
            "text": "unic_value_string",
            "isVisible": "false"
          }
        }
      },
      {
        "paso": 6,
        "tipo": "ListMultiple",
        "idComp": "ListMultiple1",
        "accion": "APROCESAR",
        "data": {
          "nombre": "seleccion",
          "valor": [
            {
              "select": [
                {
                  "nombre": "nombre",
                  "valor": " ",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Nombre"
                },
                {
                  "nombre": "monto",
                  "valor": "45.73",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Monto"
                },
                {
                  "nombre": "unic_value_string",
                  "valor": "{\"mapVar\":{\"nro\":\"003117419146\",\"periodo\":\"\",\"monto\":\"45.73\"},\"nombre\":[\"nombre\",\"mensaje\"],\"valor\":[\" \",\" \"]}",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "unic_value_string"
                },
                {
                  "nombre": "nro",
                  "valor": "003117419146",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "unic_value_string"
                },
                {
                  "nombre": "periodo",
                  "valor": "",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Período"
                },
                {
                  "nombre": "mensaje",
                  "valor": "",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Mensaje"
                }
              ]
            },
            {
              "select": [
                {
                  "nombre": "nombre",
                  "valor": " ",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Nombre"
                },
                {
                  "nombre": "monto",
                  "valor": "34.02",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Monto"
                },
                {
                  "nombre": "unic_value_string",
                  "valor": "{\"mapVar\":{\"nro\":\"003117719076\",\"periodo\":\"\",\"monto\":\"34.02\"},\"nombre\":[\"nombre\",\"mensaje\"],\"valor\":[\" \",\" \"]}",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "unic_value_string"
                },
                {
                  "nombre": "nro",
                  "valor": "003117719076",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "unic_value_string"
                },
                {
                  "nombre": "periodo",
                  "valor": "",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Período"
                },
                {
                  "nombre": "mensaje",
                  "valor": "",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Mensaje"
                }
              ]
            }
          ]
        }
      }
    ];

  }

  arreglarPorCulpaDeGenteQueNoCoopera() {

    return { "codigo": "OK", "opstatus": 0, "objeto": { "accion": "ASERVICIOS", "campoMoneda": "", "campoMonto": "", "componentes": [{ "tipo": "table_list", "nombre": "", "atributos": { "max": "", "parametro": "seleccionE", "values": [{ "lblV0": { "parametro": "", "text": "ENTEL", "isVisible": "true" }, "lblV1": { "parametro": "id", "text": "1", "isVisible": "false" }, "lblT0": { "parametro": "", "text": "Empresa", "isVisible": "false" }, "lblT1": { "parametro": "", "text": "Empresa Nacional de Telecomunicaciones S.A.", "isVisible": "false" } }, { "lblV0": { "parametro": "", "text": "VIVA", "isVisible": "true" }, "lblV1": { "parametro": "id", "text": "2", "isVisible": "false" }, "lblT0": { "parametro": "", "text": "Empresa", "isVisible": "false" }, "lblT1": { "parametro": "", "text": "VIVA.", "isVisible": "false" } }, { "lblV0": { "parametro": "", "text": "TIGO", "isVisible": "true" }, "lblV1": { "parametro": "id", "text": "3", "isVisible": "false" }, "lblT0": { "parametro": "", "text": "Empresa", "isVisible": "false" }, "lblT1": { "parametro": "", "text": "TIGO.", "isVisible": "false" } }, { "lblV0": { "parametro": "", "text": "VIVA-BOLSA", "isVisible": "true" }, "lblV1": { "parametro": "id", "text": "14", "isVisible": "false" }, "lblT0": { "parametro": "", "text": "Empresa", "isVisible": "false" }, "lblT1": { "parametro": "", "text": "VIVA-BOLSA.", "isVisible": "false" } }], "secureTextEntry": "", "titulo": "Seleccionar Empresa:", "nombre": "", "maxTextLength": "", "formatCaracterSet": "", "multipleSeleccion": "false", "camposDependientes": [], "minTextLength": "", "min": "", "idPtnConfirmacion": "Empresa", "selectDependienteParametro": "", "restrictCharactersSet": "", "dependiente": "", "placeholder": "", "textInputMode": "" } }], "tipoServicio": "", "monto": "", "pasoActual": 2, "envioFactura": "", "idService": "", "moneda": "", "pasosTotales": 6, "nombre": "Empresas" }, "mensaje": "Éxito en la Transacción", "httpStatusCode": 200, "httpresponse": { "headers": { "Cache-Control": "no-store, no-cache, must-revalidate", "Pragma": "no-cache", "Access-Control-Allow-Methods": "GET, HEAD, POST, TRACE, OPTIONS, PUT, DELETE, PATCH", "X-Kony-Service-Opstatus": "0", "X-Kony-RequestId": "220eb4f4-a2c5-488e-a6c4-c032f80e07ec", "X-Android-Response-Source": "NETWORK 200", "X-Android-Sent-Millis": "1603828254755", "Content-Type": "text/plain;charset=UTF-8", "Date": "Tue, 27 Oct 2020 19:50:55 GMT", "Server": "Kony", "Content-Length": "1729", "X-Kony-Service-Message": "", "X-Android-Received-Millis": "1603828255125", "Access-Control-Allow-Origin": "*" }, "url": "https://dbx426.sybven.com:8443/services/PagoEstandar/getFormulario", "responsecode": 200 } }

  }

  detalle() {

    return [{ "paso": 1, "tipo": "ListSimple", "idComp": "ListSimple1", "accion": "ACTION01", "data": { "nombre": "seleccione", "valor": [{ "select": [{ "nombre": "descripcion", "valor": "Servicios básicos: electricidad, agua, gas, etc.", "visible": true, "tipo": "ListSimple", "tituloVista": "Categoria", "titulo": "Descripcion" }, { "nombre": "codCuenta", "valor": "Servicios básicos", "visible": true, "tipo": "ListSimple", "tituloVista": "Categoria", "titulo": "Nombre" }, { "nombre": "idCategoria", "valor": "1", "visible": false, "tipo": "ListSimple", "tituloVista": "Categoria", "titulo": "idCategoria" }] }] }, "dataVisible": {} }, { "paso": 2, "tipo": "ListSimple", "idComp": "ListSimple2", "accion": "ACTION01", "data": { "nombre": "seleccione", "valor": [{ "select": [{ "nombre": "idEmpresa", "valor": "1", "visible": false, "tipo": "ListSimple", "tituloVista": "Empresas", "titulo": "idEmpresa" }, { "nombre": "descripcion", "valor": "Empresa de Luz y Fuerza Eléctrica Cochabamba S.A.", "visible": true, "tipo": "ListSimple", "tituloVista": "Empresas", "titulo": "Descripcion" }, { "nombre": "codCuenta", "valor": "ELFEC", "visible": true, "tipo": "ListSimple", "tituloVista": "Empresas", "titulo": "Nombre" }, { "nombre": "razonSocial", "valor": "Empresa de Luz y Fuerza Eléctrica Cochabamba S.A.", "visible": true, "tipo": "ListSimple", "tituloVista": "Empresas", "titulo": "RazonSocial" }] }] }, "dataVisible": {} }, { "paso": 3, "tipo": "ListSimple", "idComp": "ListSimple3", "accion": "ACTION01", "data": { "nombre": "seleccione", "valor": [{ "select": [{ "nombre": "codigoServicio", "valor": "76", "visible": false, "tipo": "ListSimple", "tituloVista": "Servicios", "titulo": "codigoServicio" }, { "nombre": "descripcion", "valor": "POSTPAGO", "visible": true, "tipo": "ListSimple", "tituloVista": "Servicios", "titulo": "Descripcion" }, { "nombre": "codCuenta", "valor": "POSTPAGO_ELFEC", "visible": true, "tipo": "ListSimple", "tituloVista": "Servicios", "titulo": "Nombre" }, { "nombre": "idServicio", "valor": "1", "visible": false, "tipo": "ListSimple", "tituloVista": "Servicios", "titulo": "idServicio" }, { "nombre": "idEmpresa", "valor": "1", "visible": false, "tipo": "ListSimple", "tituloVista": "Servicios", "titulo": "idEmpresa" }] }] }, "dataVisible": {} }, { "paso": 4, "tipo": "texbox", "idComp": "texbox1", "accion": "ACTION01", "data": [{ "nombre": "codAcceso", "valor": "1234567890", "tituloVista": "Codigo Fijó" }] }, { "paso": 5, "tipo": "table_select", "idComp": "DropList4", "accion": "ACTION01", "data": { "nombre": "seleccione", "valor": [{ "select": [{ "nombre": " codParam ", "valor": "text Line3", "visible": false, "tipo": "table_select", "tituloVista": "Tipo de Busqueda", "titulo": " cod param ", "tituloVisible": "Cuenta" }, { "nombre": "valorBusqueda", "valor": "001", "visible": false, "tipo": "table_select", "tituloVista": "Tipo de Busqueda", "titulo": "Valor busqueda", "tituloVisible": "Cuenta" }] }] }, "dataVisible": {} }]

  }

  ghost2() {

    return [{ "paso": 1, "tipo": "table_list", "idComp": "table_list1", "accion": "AEMPRESAS", "data": { "nombre": "seleccionC", "valor": [{ "select": [{ "nombre": "id", "valor": "1", "visible": false, "tipo": "table_list", "tituloVista": "Categoría", "propiedades": [{ "isVisible": false }, { "isVisible": false }, { "isVisible": false }, { "isVisible": false }, { "isVisible": false }, { "isVisible": false }, { "isVisible": false }, { "isVisible": false }, { "isVisible": false }, { "isVisible": false }, { "isVisible": false, "text": "Servicios de Telefonía, Internet, Cable, etc", "parametro": "" }, { "isVisible": false }, { "isVisible": false }, { "isVisible": false }, { "isVisible": false }, { "isVisible": false }, { "isVisible": false, "text": "1", "parametro": "id" }, { "isVisible": false }, { "isVisible": false }, {}, { "isVisible": true }, { "isVisible": true, "text": "Telecomunicaciones", "parametro": "" }, { "isVisible": false }, { "isVisible": false }, { "isVisible": false }, { "isVisible": false }, { "isVisible": false }, "radiobuttoninactivo.png", { "isVisible": false }, { "isVisible": false, "text": "Categoría", "parametro": "" }, { "isVisible": false }, { "isVisible": false }, { "isVisible": false }, { "isVisible": false }, { "isVisible": false }], "titulo": "Servicios de Telefonía, Internet, Cable, etc" }] }] }, "dataVisible": { "lblT0": { "text": "Categoría", "parametro": "", "isVisible": "false" }, "lblV0": { "text": "Telecomunicaciones", "parametro": "", "isVisible": "true" }, "lblT1": { "text": "Servicios de Telefonía, Internet, Cable, etc", "parametro": "", "isVisible": "false" }, "lblV1": { "text": "1", "parametro": "id", "isVisible": "false" } } }]

  }


  quesillo() {

    return [{ "paso": 1, "tipo": "table_list", "idComp": "table_list1", "accion": "AEMPRESAS", "data": { "nombre": "seleccionC", "valor": [{ "select": [{ "nombre": "id", "valor": "1", "visible": false, "tipo": "table_list", "tituloVista": "Categoría", "titulo": "Servicios de Telefonía, Internet, Cable, etc" }] }] } }];

  }

  ghost() {

    return { "objeto": [{ "accion": "AEMPRESAS", "idService": "", "pasosTotales": 6, "pasoActual": 1, "envioFactura": "", "moneda": "", "monto": "", "campoMoneda": "", "campoMonto": "", "nombre": "Categorías", "tipoServicio": "", "componentes": [{ "nombre": "", "tipo": "table_list", "atributos": [{ "idPtnConfirmacion": "Categoría", "dependiente": "", "camposDependientes": "[]", "textInputMode": "", "parametro": "seleccionC", "min": "", "max": "", "maxTextLength": "", "minTextLength": "", "restrictCharactersSet": "", "formatCaracterSet": "", "placeholder": "", "secureTextEntry": "", "selectDependienteParametro": "", "values": "[{\"lblT0\":{\"text\":\"Categoría\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV0\":{\"text\":\"Telecomunicaciones\",\"parametro\":\"\",\"isVisible\":\"true\"},\"lblT1\":{\"text\":\"Servicios de Telefonía, Internet, Cable, etc\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV1\":{\"text\":\"1\",\"parametro\":\"id\",\"isVisible\":\"false\"}},{\"lblT0\":{\"text\":\"Categoría\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV0\":{\"text\":\"Servicios básicos\",\"parametro\":\"\",\"isVisible\":\"true\"},\"lblT1\":{\"text\":\"Servicios básicos: electricidad, agua, gas, etc.\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV1\":{\"text\":\"2\",\"parametro\":\"id\",\"isVisible\":\"false\"}}]", "multipleSeleccion": "false", "titulo": "Seleccionar Categoría:" }] }, { "nombre": "", "tipo": "table_list", "atributos": [{ "idPtnConfirmacion": "Categoría", "dependiente": "", "camposDependientes": "[]", "textInputMode": "", "parametro": "seleccionC", "min": "", "max": "", "maxTextLength": "", "minTextLength": "", "restrictCharactersSet": "", "formatCaracterSet": "", "placeholder": "", "secureTextEntry": "", "selectDependienteParametro": "", "values": "[{\"lblT0\":{\"text\":\"Categoría\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV0\":{\"text\":\"Telecomunicaciones\",\"parametro\":\"\",\"isVisible\":\"true\"},\"lblT1\":{\"text\":\"Servicios de Telefonía, Internet, Cable, etc\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV1\":{\"text\":\"3\",\"parametro\":\"id2\",\"isVisible\":\"false\"}},{\"lblT0\":{\"text\":\"Categoría\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV0\":{\"text\":\"Servicios básicos\",\"parametro\":\"\",\"isVisible\":\"true\"},\"lblT1\":{\"text\":\"Servicios básicos: electricidad, agua, gas, etc.\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV1\":{\"text\":\"4\",\"parametro\":\"id2\",\"isVisible\":\"false\"}}]", "multipleSeleccion": "false", "titulo": "Seleccionar Categoría:" }] }, { "nombre": "", "tipo": "table_list", "atributos": [{ "idPtnConfirmacion": "Categoría", "dependiente": "", "camposDependientes": "[]", "textInputMode": "", "parametro": "seleccionC", "min": "", "max": "", "maxTextLength": "", "minTextLength": "", "restrictCharactersSet": "", "formatCaracterSet": "", "placeholder": "", "secureTextEntry": "", "selectDependienteParametro": "", "values": "[{\"lblT0\":{\"text\":\"Categoría\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV0\":{\"text\":\"Telecomunicaciones\",\"parametro\":\"\",\"isVisible\":\"true\"},\"lblT1\":{\"text\":\"Servicios de Telefonía, Internet, Cable, etc\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV1\":{\"text\":\"5\",\"parametro\":\"id3\",\"isVisible\":\"false\"}},{\"lblT0\":{\"text\":\"Categoría\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV0\":{\"text\":\"Servicios básicos\",\"parametro\":\"\",\"isVisible\":\"true\"},\"lblT1\":{\"text\":\"Servicios básicos: electricidad, agua, gas, etc.\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV1\":{\"text\":\"6\",\"parametro\":\"id3\",\"isVisible\":\"false\"}}]", "multipleSeleccion": "false", "titulo": "Seleccionar Categoría:" }] }] }], "codigo": "OK", "mensaje": "Éxito en la Transacción", "opstatus": 0, "httpStatusCode": 200, "httpresponse": { "headers": { "Cache-Control": "no-cache,no-store,must-revalidate", "Expires": "0", "Pragma": "no-cache", "Access-Control-Allow-Methods": "GET, HEAD, POST, TRACE, OPTIONS, PUT, DELETE, PATCH", "X-Kony-RequestId": "8bf3f494-5caf-4bb0-81a0-5746e4d0ef8d", "X-Android-Response-Source": "NETWORK 200", "X-Android-Sent-Millis": "1603718145274", "Content-Type": "application/json;charset=UTF-8", "Date": "Mon, 26 Oct 2020 13:15:46 GMT", "X-Content-Type-Options": "nosniff", "Server": "Kony", "Content-Length": "1431", "Strict-Transport-Security": "max-age=expireTime; includeSubDomains", "X-Android-Received-Millis": "1603718146031", "X-XSS-Protection": "1; mode=block", "X-Frame-Options": "SAMEORIGIN", "Access-Control-Allow-Origin": "*" }, "url": "https://dbx426.sybven.com:8443/services/data/v1/RBObjectsBMSC/operations/PagoEstandar/GetFormulario", "responsecode": 200 } };

  }

  requestDelServicioFino() {
    return [
      {
        "paso": 1,
        "tipo": "table_list",
        "idComp": "table_list1",
        "accion": "AEMPRESAS",
        "data": {
          "nombre": "seleccionC",
          "valor": [
            {
              "select": [
                {
                  "nombre": "id",
                  "valor": "2"
                }
              ]
            }
          ]
        }
      },
      {
        "paso": 2,
        "tipo": "table_list",
        "idComp": "table_list2",
        "accion": "ASERVICIOS",
        "data": {
          "nombre": "seleccionE",
          "valor": [
            {
              "select": [
                {
                  "nombre": "id",
                  "valor": "2"
                }
              ]
            }
          ]
        }
      },
      {
        "paso": 3,
        "tipo": "table_list",
        "idComp": "table_list3",
        "accion": "ABUSQUEDA",
        "data": {
          "nombre": "seleccionS",
          "valor": [
            {
              "select": [
                {
                  "nombre": "id",
                  "valor": "2"
                }
              ]
            }
          ]
        }
      },
      {
        "paso": 4,
        "tipo": "table_list",
        "idComp": "table_list4",
        "accion": "ACUENTAS",
        "data": {
          "nombre": "valorBusqueda",
          "valor": [
            {
              "select": [
                {
                  "nombre": "valorBusqueda",
                  "valor": "2"
                }
              ]
            }
          ]
        }
      },
      {
        "paso": 4,
        "tipo": "texbox",
        "idComp": "texbox1",
        "accion": "ACUENTAS",
        "data": [
          {
            "nombre": "codAcceso",
            "valor": "123465"
          }
        ]
      },
      {
        "paso": 5,
        "tipo": "ListSimple",
        "idComp": "ListSimple5",
        "accion": "ADEUDAS",
        "data": {
          "nombre": "Cuentas",
          "valor": [
            {
              "select": [
                {
                  "nombre": "unic_value_string",
                  "valor": "{\"mapVar\":{\"descripcion\":\"TROPICAL TOURS LTDA.\",\"idSeguimiento\":\"20067\",\"codCuenta\":\"30193917\"},\"nombre\":[\"montoDeuda\",\"tipoCuenta\",\"nombre\",\"cuenta\"],\"valor\":[\"53921.4\",\"TC_POST\",\"TROPICAL TOURS LTDA.\",\"30193917\"]}"
                },
                {
                  "nombre": "nombre",
                  "valor": "TROPICAL TOURS LTDA."
                },
                {
                  "nombre": "montoDeuda",
                  "valor": "53,921.4"
                },
                {
                  "nombre": "cuenta",
                  "valor": "30193917"
                }
              ]
            }
          ]
        }
      },
      {
        "paso": 6,
        "tipo": "ListMultiple",
        "idComp": "ListMultiple1",
        "accion": "APROCESAR",
        "data": {
          "nombre": "Facturas",
          "valor": [
            {
              "select": [
                {
                  "nombre": "unic_value_string",
                  "valor": "{\"mapVar\":{\"razonSocial\":\"TROPICAL TOURS LTDA.\",\"monto\":\"24869.95\",\"periodo\":\"201311\",\"idSeguimiento\":\"20067\",\"nit\":\"1013389020\",\"nro\":\"45664793\"},\"nombre\":[\"periodo\",\"loteDosificacion\",\"numeroRenta\",\"agrupador\",\"montoAPagar\",\"tipoFactura\",\"razonSocial\",\"nit\",\"factura\",\"detalle\",\"cantidad\"],\"valor\":[\"201311\",\"S30 - T48 - B15 - A14\",\"88142 - 2100 - 2440 - 448\",\"1\",\"24869.95\",\"ENTEL - TELECEL MOVIL - BOLIVIATEL MOVIL - AXS MOVIL\",\"TROPICAL TOURS LTDA.\",\"1013389020\",\"45450135 - 45449165 - 45667247 - 45664793\",\"ENTEL = 24845.91 Bs - TELECEL MOVIL = 14.87 Bs - BOLIVIATEL MOVIL = 6.1 Bs - AXS MOVIL = 3.07 Bs\",\"4\"]}",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "unic_value_string"
                },
                {
                  "nombre": "monto",
                  "valor": "24,869.95",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Monto"
                },
                {
                  "nombre": "monto",
                  "valor": "24,869.95",
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "24,869.95"
                },
                {
                  "nombre": "factura",
                  "valor": "45450135 - 45449165 - 45667247 - 45664793",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Factura"
                },
                {
                  "nombre": "periodo",
                  "valor": "201311",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "",
                  "titulo": "Periodo"
                }
              ]
            }
          ]
        }
      }
    ];
  }

  dataSinsuscripcion01() {
    return {
      "codigo": "OK",
      "mensaje": "Exito en la Transacci&oacute;n",
      "objeto": [{
        "accion": "AEMPRESAS",
        "idService": "",
        "pasosTotales": 6,
        "pasoActual": 1,
        "tipoServicio": "",
        "nombre": "Categor&iacute;as",
        "envioFactura": "",
        "campoMoneda": "",
        "moneda": "",
        "monto": "",
        "campoMonto": "",
        "componentes": [{
          "nombre": "",
          "tipo": "table_list",
          "atributos": [{
            "nombre": "",
            "dependiente": "",
            "camposDependientes": [],
            "selectDependienteParametro": "",
            "textInputMode": "",
            "parametro": "seleccionC",
            "maxTextLength": "",
            "minTextLength": "",
            "min": "",
            "max": "",
            "restrictCharactersSet": "",
            "formatCaracterSet": "",
            "placeholder": "",
            "secureTextEntry": "",
            "values": [
              {
                "lblT0": {
                  "text": "Categor&iacute;a",
                  "parametro": "",
                  "isVisible": "false"
                },
                "lblV0": {
                  "text": "Servicios b&aacute;sicos",
                  "parametro": "",
                  "isVisible": "true"
                },
                "lblT1": {
                  "text": "Descripci&oacute;n",
                  "parametro": "",
                  "isVisible": "false"
                },
                "lblV1": {
                  "text": "1",
                  "parametro": "id",
                  "isVisible": "false"
                }
              },
              {
                "lblT0": {
                  "text": "Categor&iacute;a",
                  "parametro": "",
                  "isVisible": "false"
                },
                "lblV0": {
                  "text": "Telecomunicaciones",
                  "parametro": "",
                  "isVisible": "true"
                },
                "lblT1": {
                  "text": "Descripci&oacute;n",
                  "parametro": "",
                  "isVisible": "false"
                },
                "lblV1": {
                  "text": "2",
                  "parametro": "id",
                  "isVisible": "false"
                }
              },
              {
                "lblT0": {
                  "text": "Categor&iacute;a",
                  "parametro": "",
                  "isVisible": "false"
                },
                "lblV0": {
                  "text": "Educaci&oacute;n",
                  "parametro": "",
                  "isVisible": "true"
                },
                "lblT1": {
                  "text": "Descripci&oacute;n",
                  "parametro": "",
                  "isVisible": "false"
                },
                "lblV1": {
                  "text": "3",
                  "parametro": "id",
                  "isVisible": "false"
                }
              },
              {
                "lblT0": {
                  "text": "Categor&iacute;a",
                  "parametro": "",
                  "isVisible": "false"
                },
                "lblV0": {
                  "text": "Transporte",
                  "parametro": "",
                  "isVisible": "true"
                },
                "lblT1": {
                  "text": "Descripci&oacute;n",
                  "parametro": "",
                  "isVisible": "false"
                },
                "lblV1": {
                  "text": "5",
                  "parametro": "id",
                  "isVisible": "false"
                }
              }
            ],
            "multipleSeleccion": "false",
            "titulo": "Seleccionar Categor&iacute;a:",
            "idPtnConfirmacion": "Categor&iacute;a"
          }]
        }]
      }]
    };
  }
  dataSinsuscripcion02() {
    return {
      "codigo": "OK",
      "mensaje": "Exito en la Transacci&oacute;n",
      "objeto": [{
        "accion": "ASERVICIOS",
        "idService": "",
        "pasosTotales": 6,
        "pasoActual": 2,
        "tipoServicio": "",
        "nombre": "Empresas",
        "envioFactura": "",
        "campoMoneda": "",
        "moneda": "",
        "monto": "",
        "campoMonto": "",
        "componentes": [{
          "nombre": "",
          "tipo": "table_list",
          "atributos": [{
            "nombre": "",
            "dependiente": "",
            "camposDependientes": [],
            "selectDependienteParametro": "",
            "textInputMode": "",
            "parametro": "seleccionE",
            "maxTextLength": "",
            "minTextLength": "",
            "min": "",
            "max": "",
            "restrictCharactersSet": "",
            "formatCaracterSet": "",
            "placeholder": "",
            "secureTextEntry": "",
            "values": [
              {
                "lblT0": {
                  "text": "Empresa",
                  "parametro": "",
                  "isVisible": "false"
                },
                "lblV0": {
                  "text": "ENTEL",
                  "parametro": "",
                  "isVisible": "true"
                },
                "lblT1": {
                  "text": "Descripci&oacute;n",
                  "parametro": "",
                  "isVisible": "false"
                },
                "lblV1": {
                  "text": "2",
                  "parametro": "id",
                  "isVisible": "false"
                }
              },
              {
                "lblT0": {
                  "text": "Empresa",
                  "parametro": "",
                  "isVisible": "false"
                },
                "lblV0": {
                  "text": "COTEL",
                  "parametro": "",
                  "isVisible": "true"
                },
                "lblT1": {
                  "text": "Descripci&oacute;n",
                  "parametro": "",
                  "isVisible": "false"
                },
                "lblV1": {
                  "text": "7",
                  "parametro": "id",
                  "isVisible": "false"
                }
              },
              {
                "lblT0": {
                  "text": "Empresa",
                  "parametro": "",
                  "isVisible": "false"
                },
                "lblV0": {
                  "text": "COTAS",
                  "parametro": "",
                  "isVisible": "true"
                },
                "lblT1": {
                  "text": "Descripci&oacute;n",
                  "parametro": "",
                  "isVisible": "false"
                },
                "lblV1": {
                  "text": "10",
                  "parametro": "id",
                  "isVisible": "false"
                }
              }
            ],
            "multipleSeleccion": "false",
            "titulo": "Seleccionar Empresa:",
            "idPtnConfirmacion": "Empresa"
          }]
        }]
      }]
    };
  }
  dataSinsuscripcion03() {
    return {
      "codigo": "OK",
      "mensaje": "Exito en la Transacci&oacute;n",
      "objeto": [{
        "accion": "ABUSQUEDA",
        "idService": "",
        "pasosTotales": 6,
        "pasoActual": 3,
        "tipoServicio": "",
        "nombre": "Servicios",
        "envioFactura": "",
        "campoMoneda": "",
        "moneda": "",
        "monto": "",
        "campoMonto": "",
        "componentes": [{
          "nombre": "",
          "tipo": "table_list",
          "atributos": [{
            "nombre": "",
            "dependiente": "",
            "camposDependientes": [],
            "selectDependienteParametro": "",
            "textInputMode": "",
            "parametro": "seleccionS",
            "maxTextLength": "",
            "minTextLength": "",
            "min": "",
            "max": "",
            "restrictCharactersSet": "",
            "formatCaracterSet": "",
            "placeholder": "",
            "secureTextEntry": "",
            "values": [
              {
                "lblT0": {
                  "text": "Servicio",
                  "parametro": "",
                  "isVisible": "false"
                },
                "lblV0": {
                  "text": "ENTEL Prepago",
                  "parametro": "",
                  "isVisible": "true"
                },
                "lblT1": {
                  "text": "Descripci&oacute;n",
                  "parametro": "",
                  "isVisible": " false "
                },
                "lblV1": {
                  "text": "2",
                  "parametro": "id",
                  "isVisible": "false"
                }
              },
              {
                "lblT0": {
                  "text": "Servicio",
                  "parametro": "",
                  "isVisible": "false"
                },
                "lblV0": {
                  "text": "ENTEL Postpago",
                  "parametro": "",
                  "isVisible": ""
                },
                "lblT1": {
                  "text": "Descripci&oacute;n",
                  "parametro": "",
                  "isVisible": "false"
                },
                "lblV1": {
                  "text": "3",
                  "parametro": "id",
                  "isVisible": "false"
                }
              }
            ],
            "multipleSeleccion": "false",
            "titulo": "Seleccionar Servicio:",
            "idPtnConfirmacion": "Servicio"
          }]
        }]
      }]
    };
  }
  dataSinsuscripcion04() {
    return {
      "codigo": "OK",
      "mensaje": "Exito en la Transacci&oacute;n",
      "objeto": [{
        "accion": "ACUENTAS",
        "idService": "3",
        "pasosTotales": 6,
        "pasoActual": 4,
        "tipoServicio": "2",
        "nombre": "ENTEL Postpago (Paso 1)",
        "envioFactura": "false",
        "campoMoneda": "",
        "moneda": "BOB",
        "monto": "monto",
        "campoMonto": "",
        "componentes": [
          {
            "nombre": "",
            "tipo": "label",
            "atributos": [{
              "nombre": "",
              "dependiente": "false",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "",
              "parametro": "",
              "maxTextLength": "",
              "minTextLength": "",
              "min": "",
              "max": "",
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "",
              "secureTextEntry": "",
              "values": [],
              "multipleSeleccion": "",
              "titulo": "Seleccione el tipo de b&uacute;squeda e introduzca la cuenta correspondiente:",
              "idPtnConfirmacion": "Tipo de b&uacute;squeda"
            }]
          },
          {
            "nombre": "",
            "tipo": "table_list",
            "atributos": [{
              "nombre": "",
              "dependiente": "false",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "",
              "parametro": "valorBusqueda",
              "maxTextLength": "",
              "minTextLength": "",
              "min": "",
              "max": "",
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "",
              "secureTextEntry": "",
              "values": [
                {
                  "lblT0": {
                    "text": "Cliente",
                    "parametro": "",
                    "isVisible": "true"
                  },
                  "lblV0": {
                    "text": "1",
                    "parametro": "valorBusqueda",
                    "isVisible": "false"
                  }
                },
                {
                  "lblT0": {
                    "text": "Cuenta",
                    "parametro": "",
                    "isVisible": "true"
                  },
                  "lblV0": {
                    "text": "2",
                    "parametro": "valorBusqueda",
                    "isVisible": "false"
                  }
                },
                {
                  "lblT0": {
                    "text": "Tel&eacute;fono",
                    "parametro": "",
                    "isVisible": "true"
                  },
                  "lblV0": {
                    "text": "3",
                    "parametro": "valorBusqueda",
                    "isVisible": "false"
                  }
                }
              ],
              "multipleSeleccion": "false",
              "titulo": "",
              "idPtnConfirmacion": ""
            }]
          },
          {
            "nombre": "Cuenta",
            "tipo": "input",
            "atributos": [{
              "nombre": "",
              "dependiente": "false",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "N",
              "parametro": "codAcceso",
              "maxTextLength": "15",
              "minTextLength": "1",
              "min": "",
              "max": "",
              "restrictCharactersSet": "!@#$%",
              "formatCaracterSet": "^\\d+$",
              "placeholder": "Ingresar Cuenta",
              "secureTextEntry": "false",
              "values": [],
              "multipleSeleccion": "",
              "titulo": "",
              "idPtnConfirmacion": ""
            }]
          }
        ]
      }]
    };
  }
  dataSinsuscripcion05() {
    return {
      "codigo": "OK",
      "mensaje": "Exito en la Transacci&oacute;n",
      "objeto": [{
        "accion": "ADEUDAS",
        "idService": "3",
        "pasosTotales": 6,
        "pasoActual": 5,
        "tipoServicio": "2",
        "nombre": "ENTEL Postpago (Paso 2)",
        "envioFactura": "false",
        "campoMoneda": "",
        "moneda": "BOB",
        "monto": "monto",
        "campoMonto": "",
        "componentes": [
          {
            "nombre": "",
            "tipo": "label",
            "atributos": [{
              "nombre": "",
              "dependiente": "false",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "",
              "parametro": "",
              "maxTextLength": "",
              "minTextLength": "",
              "min": "",
              "max": "",
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "",
              "secureTextEntry": "",
              "values": [],
              "multipleSeleccion": "",
              "titulo": "Seleccione una cuenta:",
              "idPtnConfirmacion": "Cuenta"
            }]
          },
          {
            "nombre": "",
            "tipo": "table",
            "atributos": [{
              "nombre": "",
              "dependiente": "false",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "",
              "parametro": "Cuentas",
              "maxTextLength": "",
              "minTextLength": "",
              "min": "",
              "max": "",
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "",
              "secureTextEntry": "",
              "values": [{
                "lblT0": {
                  "text": "Monto Deuda",
                  "parametro": "",
                  "isVisible": "true"
                },
                "lblV0": {
                  "text": "53,921.4",
                  "parametro": "montoDeuda",
                  "isVisible": "true"
                },
                "lblT1": {
                  "text": "Nombre",
                  "parametro": "",
                  "isVisible": "true"
                },
                "lblV1": {
                  "text": "TROPICAL TOURS LTDA.",
                  "parametro": "nombre",
                  "isVisible": "true"
                },
                "lblT2": {
                  "text": "Cuenta",
                  "parametro": "",
                  "isVisible": "true"
                },
                "lblV2": {
                  "text": "30193917",
                  "parametro": "cuenta",
                  "isVisible": "true"
                },
                "lblT3": {
                  "text": "unic_value_string",
                  "parametro": "",
                  "isVisible": "false"
                },
                "lblV3": {
                  "text": "{\"mapVar\":{\"descripcion\":\"TROPICAL TOURS LTDA.\",\"idSeguimiento\":\"20067\",\"codCuenta\":\"30193917\"},\"nombre\":[\"montoDeuda\",\"tipoCuenta\",\"nombre\",\"cuenta\"],\"valor\":[\"53921.4\",\"TC_POST\",\"TROPICAL TOURS LTDA.\",\"30193917\"]}",
                  "parametro": "unic_value_string",
                  "isVisible": "false"
                }
              }],
              "multipleSeleccion": "false",
              "titulo": "",
              "idPtnConfirmacion": ""
            }]
          }
        ]
      }]
    };
  }
  dataSinsuscripcion06() {
    return {
      "codigo": "OK",
      "mensaje": "Exito en la Transacci&oacute;n",
      "objeto": [{
        "accion": "APROCESAR",
        "idService": "3",
        "pasosTotales": 6,
        "pasoActual": 6,
        "tipoServicio": "2",
        "nombre": "ENTEL Postpago (Paso 3)",
        "envioFactura": "false",
        "campoMoneda": "",
        "moneda": "BOB",
        "monto": "monto",
        "campoMonto": "",
        "componentes": [
          {
            "nombre": "",
            "tipo": "label",
            "atributos": [{
              "nombre": "",
              "dependiente": "false",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "",
              "parametro": "",
              "maxTextLength": "",
              "minTextLength": "",
              "min": "",
              "max": "",
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "",
              "secureTextEntry": "",
              "values": [],
              "multipleSeleccion": "",
              "titulo": "Ahora con el BMSC puedes hacer pagos agendados...",
              "idPtnConfirmacion": "Pagos agendados"
            }]
          },
          {
            "nombre": "",
            "tipo": "label",
            "atributos": [{
              "nombre": "",
              "dependiente": "false",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "",
              "parametro": "",
              "maxTextLength": "",
              "minTextLength": "",
              "min": "",
              "max": "",
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "",
              "secureTextEntry": "",
              "values": [],
              "multipleSeleccion": "",
              "titulo": "Seleccione la deuda a ser pagada:",
              "idPtnConfirmacion": "Deuda"
            }]
          },
          {
            "nombre": "",
            "tipo": "table",
            "atributos": [{
              "nombre": "",
              "dependiente": "false",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "",
              "parametro": "Facturas",
              "maxTextLength": "",
              "minTextLength": "",
              "min": "",
              "max": "",
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "",
              "secureTextEntry": "",
              "values": [
                {
                  "lblT0": {
                    "text": "Factura",
                    "parametro": "",
                    "isVisible": "true"
                  },
                  "lblV0": {
                    "text": "45450135 - 45449165 - 45667247 - 45664793",
                    "parametro": "factura",
                    "isVisible": "true"
                  },
                  "lblT1": {
                    "text": "Monto",
                    "parametro": "",
                    "isVisible": "true"
                  },
                  "lblV1": {
                    "text": "24,869.95",
                    "parametro": "monto",
                    "isVisible": "true"
                  },
                  "lblT2": {
                    "text": "Periodo",
                    "parametro": "",
                    "isVisible": "true"
                  },
                  "lblV2": {
                    "text": "201311",
                    "parametro": "periodo",
                    "isVisible": "true"
                  },
                  "lblT3": {
                    "text": "unic_value_string",
                    "parametro": "",
                    "isVisible": "false"
                  },
                  "lblV3": {
                    "text": "{\"mapVar\":{\"razonSocial\":\"TROPICAL TOURS LTDA.\",\"monto\":\"24869.95\",\"periodo\":\"201311\",\"idSeguimiento\":\"20067\",\"nit\":\"1013389020\",\"nro\":\"45664793\"},\"nombre\":[\"periodo\",\"loteDosificacion\",\"numeroRenta\",\"agrupador\",\"montoAPagar\",\"tipoFactura\",\"razonSocial\",\"nit\",\"factura\",\"detalle\",\"cantidad\"],\"valor\":[\"201311\",\"S30 - T48 - B15 - A14\",\"88142 - 2100 - 2440 - 448\",\"1\",\"24869.95\",\"ENTEL - TELECEL MOVIL - BOLIVIATEL MOVIL - AXS MOVIL\",\"TROPICAL TOURS LTDA.\",\"1013389020\",\"45450135 - 45449165 - 45667247 - 45664793\",\"ENTEL = 24845.91 Bs - TELECEL MOVIL = 14.87 Bs - BOLIVIATEL MOVIL = 6.1 Bs - AXS MOVIL = 3.07 Bs\",\"4\"]}",
                    "parametro": "unic_value_string",
                    "isVisible": "false"
                  }
                },
                {
                  "lblT0": {
                    "text": "Factura",
                    "parametro": "",
                    "isVisible": "true"
                  },
                  "lblV0": {
                    "text": "45841676 - 45840761",
                    "parametro": "factura",
                    "isVisible": "true"
                  },
                  "lblT1": {
                    "text": "Monto",
                    "parametro": "",
                    "isVisible": "true"
                  },
                  "lblV1": {
                    "text": "29,051.45",
                    "parametro": "monto",
                    "isVisible": "true"
                  },
                  "lblT2": {
                    "text": "Periodo",
                    "parametro": "",
                    "isVisible": "true"
                  },
                  "lblV2": {
                    "text": "201312",
                    "parametro": "periodo",
                    "isVisible": "true"
                  },
                  "lblT3": {
                    "text": "unic_value_string",
                    "parametro": "",
                    "isVisible": "false"
                  },
                  "lblV3": {
                    "text": "{\"mapVar\":{\"razonSocial\":\"TROPICAL TOURS LTDA.\",\"monto\":\"29051.45\",\"periodo\":\"201312\",\"idSeguimiento\":\"20067\",\"nit\":\"1013389020\",\"nro\":\"45840761\"},\"nombre\":[\"periodo\",\"loteDosificacion\",\"numeroRenta\",\"agrupador\",\"montoAPagar\",\"tipoFactura\",\"razonSocial\",\"nit\",\"factura\",\"detalle\",\"cantidad\"],\"valor\":[\"201312\",\"S30 - T48\",\"375720 - 2942\",\"1\",\"29051.45\",\"ENTEL - TELECEL MOVIL\",\"TROPICAL TOURS LTDA.\",\"1013389020\",\"45841676 - 45840761\",\"ENTEL = 29045.97 Bs - TELECEL MOVIL = 5.48 Bs\",\"2\"]}",
                    "parametro": "unic_value_string",
                    "isVisible": "false"
                  }
                }
              ],
              "multipleSeleccion": "true",
              "titulo": "",
              "idPtnConfirmacion": ""
            }]
          }
        ]
      }]
    };
  }
  dataConsuscripcion01() {
    return {
      "codigo": "OK",
      "mensaje": "Exito en la Transacci&oacute;n",
      "objeto": [{
        "accion": "ADEUDAS",
        "idService": "3",
        "pasosTotales": 2,
        "pasoActual": 1,
        "tipoServicio": "2",
        "nombre": "ENTEL Postpago (Paso 2)",
        "envioFactura": "false",
        "campoMoneda": "",
        "moneda": "BOB",
        "monto": "monto",
        "campoMonto": "",
        "componentes": [
          {
            "nombre": "",
            "tipo": "label",
            "atributos": [{
              "nombre": "",
              "dependiente": "false",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "",
              "parametro": "",
              "maxTextLength": "",
              "minTextLength": "",
              "min": "",
              "max": "",
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "",
              "secureTextEntry": "",
              "values": [],
              "multipleSeleccion": "",
              "titulo": "Seleccione una cuenta:",
              "idPtnConfirmacion": "Cuenta"
            }]
          },
          {
            "nombre": "",
            "tipo": "table",
            "atributos": [{
              "nombre": "",
              "dependiente": "false",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "",
              "parametro": "Cuentas",
              "maxTextLength": "",
              "minTextLength": "",
              "min": "",
              "max": "",
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "",
              "secureTextEntry": "",
              "values": [{
                "lblT0": {
                  "text": "Monto Deuda",
                  "parametro": "",
                  "isVisible": "true"
                },
                "lblV0": {
                  "text": "53,921.4",
                  "parametro": "montoDeuda",
                  "isVisible": "true"
                },
                "lblT1": {
                  "text": "Nombre",
                  "parametro": "",
                  "isVisible": "true"
                },
                "lblV1": {
                  "text": "TROPICAL TOURS LTDA.",
                  "parametro": "nombre",
                  "isVisible": "true"
                },
                "lblT2": {
                  "text": "Cuenta",
                  "parametro": "",
                  "isVisible": "true"
                },
                "lblV2": {
                  "text": "30193917",
                  "parametro": "cuenta",
                  "isVisible": "true"
                },
                "lblT3": {
                  "text": "unic_value_string",
                  "parametro": "",
                  "isVisible": "false"
                },
                "lblV3": {
                  "text": "{\"mapVar\":{\"descripcion\":\"TROPICAL TOURS LTDA.\",\"idSeguimiento\":\"20067\",\"codCuenta\":\"30193917\"},\"nombre\":[\"montoDeuda\",\"tipoCuenta\",\"nombre\",\"cuenta\"],\"valor\":[\"53921.4\",\"TC_POST\",\"TROPICAL TOURS LTDA.\",\"30193917\"]}",
                  "parametro": "unic_value_string",
                  "isVisible": "false"
                }
              }],
              "multipleSeleccion": "false",
              "titulo": "",
              "idPtnConfirmacion": ""
            }]
          }
        ]
      }]
    };
  }
  dataConsuscripcion02() {
    return {
      "codigo": "OK",
      "mensaje": "Exito en la Transacci&oacute;n",
      "objeto": [{
        "accion": "APROCESAR",
        "idService": "3",
        "pasosTotales": 2,
        "pasoActual": 2,
        "tipoServicio": "2",
        "nombre": "ENTEL Postpago (Paso 3)",
        "envioFactura": "false",
        "campoMoneda": "",
        "moneda": "BOB",
        "monto": "monto",
        "campoMonto": "",
        "componentes": [
          {
            "nombre": "",
            "tipo": "label",
            "atributos": [{
              "nombre": "",
              "dependiente": "false",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "",
              "parametro": "",
              "maxTextLength": "",
              "minTextLength": "",
              "min": "",
              "max": "",
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "",
              "secureTextEntry": "",
              "values": [],
              "multipleSeleccion": "",
              "titulo": "Ahora con el BMSC puedes hacer pagos agendados...",
              "idPtnConfirmacion": "Pagos agendados"
            }]
          },
          {
            "nombre": "",
            "tipo": "label",
            "atributos": [{
              "nombre": "",
              "dependiente": "false",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "",
              "parametro": "",
              "maxTextLength": "",
              "minTextLength": "",
              "min": "",
              "max": "",
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "",
              "secureTextEntry": "",
              "values": [],
              "multipleSeleccion": "",
              "titulo": "Seleccione la deuda a ser pagada:",
              "idPtnConfirmacion": "Deuda"
            }]
          },
          {
            "nombre": "",
            "tipo": "table",
            "atributos": [{
              "nombre": "",
              "dependiente": "false",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "",
              "parametro": "Facturas",
              "maxTextLength": "",
              "minTextLength": "",
              "min": "",
              "max": "",
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "",
              "secureTextEntry": "",
              "values": [
                {
                  "lblT0": {
                    "text": "Factura",
                    "parametro": "",
                    "isVisible": "true"
                  },
                  "lblV0": {
                    "text": "45450135 - 45449165 - 45667247 - 45664793",
                    "parametro": "factura",
                    "isVisible": "true"
                  },
                  "lblT1": {
                    "text": "Monto",
                    "parametro": "",
                    "isVisible": "true"
                  },
                  "lblV1": {
                    "text": "24,869.95",
                    "parametro": "monto",
                    "isVisible": "true"
                  },
                  "lblT2": {
                    "text": "Periodo",
                    "parametro": "",
                    "isVisible": "true"
                  },
                  "lblV2": {
                    "text": "201311",
                    "parametro": "periodo",
                    "isVisible": "true"
                  },
                  "lblT3": {
                    "text": "unic_value_string",
                    "parametro": "",
                    "isVisible": "false"
                  },
                  "lblV3": {
                    "text": "{\"mapVar\":{\"razonSocial\":\"TROPICAL TOURS LTDA.\",\"monto\":\"24869.95\",\"periodo\":\"201311\",\"idSeguimiento\":\"20067\",\"nit\":\"1013389020\",\"nro\":\"45664793\"},\"nombre\":[\"periodo\",\"loteDosificacion\",\"numeroRenta\",\"agrupador\",\"montoAPagar\",\"tipoFactura\",\"razonSocial\",\"nit\",\"factura\",\"detalle\",\"cantidad\"],\"valor\":[\"201311\",\"S30 - T48 - B15 - A14\",\"88142 - 2100 - 2440 - 448\",\"1\",\"24869.95\",\"ENTEL - TELECEL MOVIL - BOLIVIATEL MOVIL - AXS MOVIL\",\"TROPICAL TOURS LTDA.\",\"1013389020\",\"45450135 - 45449165 - 45667247 - 45664793\",\"ENTEL = 24845.91 Bs - TELECEL MOVIL = 14.87 Bs - BOLIVIATEL MOVIL = 6.1 Bs - AXS MOVIL = 3.07 Bs\",\"4\"]}",
                    "parametro": "unic_value_string",
                    "isVisible": "false"
                  }
                },
                {
                  "lblT0": {
                    "text": "Factura",
                    "parametro": "",
                    "isVisible": "true"
                  },
                  "lblV0": {
                    "text": "45841676 - 45840761",
                    "parametro": "factura",
                    "isVisible": "true"
                  },
                  "lblT1": {
                    "text": "Monto",
                    "parametro": "",
                    "isVisible": "true"
                  },
                  "lblV1": {
                    "text": "29,051.45",
                    "parametro": "monto",
                    "isVisible": "true"
                  },
                  "lblT2": {
                    "text": "Periodo",
                    "parametro": "",
                    "isVisible": "true"
                  },
                  "lblV2": {
                    "text": "201312",
                    "parametro": "periodo",
                    "isVisible": "true"
                  },
                  "lblT3": {
                    "text": "unic_value_string",
                    "parametro": "",
                    "isVisible": "false"
                  },
                  "lblV3": {
                    "text": "{\"mapVar\":{\"razonSocial\":\"TROPICAL TOURS LTDA.\",\"monto\":\"29051.45\",\"periodo\":\"201312\",\"idSeguimiento\":\"20067\",\"nit\":\"1013389020\",\"nro\":\"45840761\"},\"nombre\":[\"periodo\",\"loteDosificacion\",\"numeroRenta\",\"agrupador\",\"montoAPagar\",\"tipoFactura\",\"razonSocial\",\"nit\",\"factura\",\"detalle\",\"cantidad\"],\"valor\":[\"201312\",\"S30 - T48\",\"375720 - 2942\",\"1\",\"29051.45\",\"ENTEL - TELECEL MOVIL\",\"TROPICAL TOURS LTDA.\",\"1013389020\",\"45841676 - 45840761\",\"ENTEL = 29045.97 Bs - TELECEL MOVIL = 5.48 Bs\",\"2\"]}",
                    "parametro": "unic_value_string",
                    "isVisible": "false"
                  }
                }
              ],
              "multipleSeleccion": "true",
              "titulo": "",
              "idPtnConfirmacion": ""
            }]
          }
        ]
      }]
    };
  }

  requestCompleto() {
    return {
      "accion": "APROCESAR",
      "idservicio": "6",
      "values":
        [
          {
            "nombre": "Facturas",
            "valor": [
              {
                "select": [
                  {
                    "nombre": "factura",
                    "valor": "45450135 - 45449165 - 45667247 - 45664793"
                  },
                  {
                    "nombre": "monto",
                    "valor": "24869.95"
                  },
                  {
                    "nombre": "periodo",
                    "valor": "201311"
                  },
                  {
                    "nombre": "unic_value_string",
                    "valor": "{\"mapVar\":{\"razonSocial\":\"TROPICAL TOURS LTDA.\",\"monto\":\"24869.95\",\"periodo\":\"201311\",\"idSeguimiento\":\"20067\",\"nit\":\"1013389020\",\"nro\":\"45664793\"},\"nombre\":[\"periodo\",\"loteDosificacion\",\"numeroRenta\",\"agrupador\",\"montoAPagar\",\"tipoFactura\",\"razonSocial\",\"nit\",\"factura\",\"detalle\",\"cantidad\"],\"valor\":[\"201311\",\"S30 - T48 - B15 - A14\",\"88142 - 2100 - 2440 - 448\",\"1\",\"24869.95\",\"ENTEL - TELECEL MOVIL - BOLIVIATEL MOVIL - AXS MOVIL\",\"TROPICAL TOURS LTDA.\",\"1013389020\",\"45450135 - 45449165 - 45667247 - 45664793\",\"ENTEL = 24845.91 Bs - TELECEL MOVIL = 14.87 Bs - BOLIVIATEL MOVIL = 6.1 Bs - AXS MOVIL = 3.07 Bs\",\"4\"]}"
                  }
                ]
              },
              {
                "select": [
                  {
                    "nombre": "factura",
                    "valor": "45841676 - 45840761"
                  },
                  {
                    "nombre": "monto",
                    "valor": "29051.45"
                  },
                  {
                    "nombre": "periodo",
                    "valor": "201312"
                  },
                  {
                    "nombre": "unic_value_string",
                    "valor": "{\"mapVar\":{\"razonSocial\":\"TROPICAL TOURS LTDA.\",\"monto\":\"29051.45\",\"periodo\":\"201312\",\"idSeguimiento\":\"20067\",\"nit\":\"1013389020\",\"nro\":\"45840761\"},\"nombre\":[\"periodo\",\"loteDosificacion\",\"numeroRenta\",\"agrupador\",\"montoAPagar\",\"tipoFactura\",\"razonSocial\",\"nit\",\"factura\",\"detalle\",\"cantidad\"],\"valor\":[\"201312\",\"S30 - T48\",\"375720 - 2942\",\"1\",\"29051.45\",\"ENTEL - TELECEL MOVIL\",\"TROPICAL TOURS LTDA.\",\"1013389020\",\"45841676 - 45840761\",\"ENTEL = 29045.97 Bs - TELECEL MOVIL = 5.48 Bs\",\"2\"]}"
                  }
                ]
              }
            ]
          }
        ]
    }
  }

  ghostPruebas() {

    return [{
      nombre: 'Facturas',
      valor:
        '[{"select":[{"nombre":"factura","valor":"45450135 - 45449165 - 45667247 - 45664793"},{"nombre":"monto","valor":"24869.95"},{"nombre":"periodo","valor":"201311"},{"nombre":"unic_value_string","valor":"{&#92;&#92;&#34;mapVar&#92;&#92;&#34;:{&#92;&#92;&#34;razonSocial&#92;&#92;&#34;:&#92;&#92;&#34;TROPICAL TOURS LTDA.&#92;&#92;&#34;,&#92;&#92;&#34;monto&#92;&#92;&#34;:&#92;&#92;&#34;24869.95&#92;&#92;&#34;,&#92;&#92;&#34;periodo&#92;&#92;&#34;:&#92;&#92;&#34;201311&#92;&#92;&#34;,&#92;&#92;&#34;idSeguimiento&#92;&#92;&#34;:&#92;&#92;&#34;20067&#92;&#92;&#34;,&#92;&#92;&#34;nit&#92;&#92;&#34;:&#92;&#92;&#34;1013389020&#92;&#92;&#34;,&#92;&#92;&#34;nro&#92;&#92;&#34;:&#92;&#92;&#34;45664793&#92;&#92;&#34;},&#92;&#92;&#34;nombre&#92;&#92;&#34;:[&#92;&#92;&#34;periodo&#92;&#92;&#34;,&#92;&#92;&#34;loteDosificacion&#92;&#92;&#34;,&#92;&#92;&#34;numeroRenta&#92;&#92;&#34;,&#92;&#92;&#34;agrupador&#92;&#92;&#34;,&#92;&#92;&#34;montoAPagar&#92;&#92;&#34;,&#92;&#92;&#34;tipoFactura&#92;&#92;&#34;,&#92;&#92;&#34;razonSocial&#92;&#92;&#34;,&#92;&#92;&#34;nit&#92;&#92;&#34;,&#92;&#92;&#34;factura&#92;&#92;&#34;,&#92;&#92;&#34;detalle&#92;&#92;&#34;,&#92;&#92;&#34;cantidad&#92;&#92;&#34;],&#92;&#92;&#34;valor&#92;&#92;&#34;:[&#92;&#92;&#34;201311&#92;&#92;&#34;,&#92;&#92;&#34;S30 - T48 - B15 - A14&#92;&#92;&#34;,&#92;&#92;&#34;88142 - 2100 - 2440 - 448&#92;&#92;&#34;,&#92;&#92;&#34;1&#92;&#92;&#34;,&#92;&#92;&#34;24869.95&#92;&#92;&#34;,&#92;&#92;&#34;ENTEL - TELECEL MOVIL - BOLIVIATEL MOVIL - AXS MOVIL&#92;&#92;&#34;,&#92;&#92;&#34;TROPICAL TOURS LTDA.&#92;&#92;&#34;,&#92;&#92;&#34;1013389020&#92;&#92;&#34;,&#92;&#92;&#34;45450135 - 45449165 - 45667247 - 45664793&#92;&#92;&#34;,&#92;&#92;&#34;ENTEL = 24845.91 Bs - TELECEL MOVIL = 14.87 Bs - BOLIVIATEL MOVIL = 6.1 Bs - AXS MOVIL = 3.07 Bs&#92;&#92;&#34;,&#92;&#92;&#34;4&#92;&#92;&#34;]}"}]},{"select":[{"nombre":"factura","valor":"45841676 - 45840761"},{"nombre":"monto","valor":"29051.45"},{"nombre":"periodo","valor":"201312"},{"nombre":"unic_value_string","valor":"{&#92;&#92;&#34;mapVar&#92;&#92;&#34;:{&#92;&#92;&#34;razonSocial&#92;&#92;&#34;:&#92;&#92;&#34;TROPICAL TOURS LTDA.&#92;&#92;&#34;,&#92;&#92;&#34;monto&#92;&#92;&#34;:&#92;&#92;&#34;29051.45&#92;&#92;&#34;,&#92;&#92;&#34;periodo&#92;&#92;&#34;:&#92;&#92;&#34;201312&#92;&#92;&#34;,&#92;&#92;&#34;idSeguimiento&#92;&#92;&#34;:&#92;&#92;&#34;20067&#92;&#92;&#34;,&#92;&#92;&#34;nit&#92;&#92;&#34;:&#92;&#92;&#34;1013389020&#92;&#92;&#34;,&#92;&#92;&#34;nro&#92;&#92;&#34;:&#92;&#92;&#34;45840761&#92;&#92;&#34;},&#92;&#92;&#34;nombre&#92;&#92;&#34;:[&#92;&#92;&#34;periodo&#92;&#92;&#34;,&#92;&#92;&#34;loteDosificacion&#92;&#92;&#34;,&#92;&#92;&#34;numeroRenta&#92;&#92;&#34;,&#92;&#92;&#34;agrupador&#92;&#92;&#34;,&#92;&#92;&#34;montoAPagar&#92;&#92;&#34;,&#92;&#92;&#34;tipoFactura&#92;&#92;&#34;,&#92;&#92;&#34;razonSocial&#92;&#92;&#34;,&#92;&#92;&#34;nit&#92;&#92;&#34;,&#92;&#92;&#34;factura&#92;&#92;&#34;,&#92;&#92;&#34;detalle&#92;&#92;&#34;,&#92;&#92;&#34;cantidad&#92;&#92;&#34;],&#92;&#92;&#34;valor&#92;&#92;&#34;:[&#92;&#92;&#34;201312&#92;&#92;&#34;,&#92;&#92;&#34;S30 - T48&#92;&#92;&#34;,&#92;&#92;&#34;375720 - 2942&#92;&#92;&#34;,&#92;&#92;&#34;1&#92;&#92;&#34;,&#92;&#92;&#34;29051.45&#92;&#92;&#34;,&#92;&#92;&#34;ENTEL - TELECEL MOVIL&#92;&#92;&#34;,&#92;&#92;&#34;TROPICAL TOURS LTDA.&#92;&#92;&#34;,&#92;&#92;&#34;1013389020&#92;&#92;&#34;,&#92;&#92;&#34;45841676 - 45840761&#92;&#92;&#34;,&#92;&#92;&#34;ENTEL = 29045.97 Bs - TELECEL MOVIL = 5.48 Bs&#92;&#92;&#34;,&#92;&#92;&#34;2&#92;&#92;&#34;]}"}]}]'
    }];

  }

  dameJson() {

    return [
      {
        "nombre": "Facturas",
        "valor": [
          {
            "select": [
              {
                "nombre": "factura",
                "valor": "45450135 - 45449165 - 45667247 - 45664793"
              },
              {
                "nombre": "monto",
                "valor": "24869.95"
              },
              {
                "nombre": "periodo",
                "valor": "201311"
              },
              {
                "nombre": "unic_value_string",
                "valor": "{\"mapVar\":{\"razonSocial\":\"TROPICAL TOURS LTDA.\",\"monto\":\"24869.95\",\"periodo\":\"201311\",\"idSeguimiento\":\"20067\",\"nit\":\"1013389020\",\"nro\":\"45664793\"},\"nombre\":[\"periodo\",\"loteDosificacion\",\"numeroRenta\",\"agrupador\",\"montoAPagar\",\"tipoFactura\",\"razonSocial\",\"nit\",\"factura\",\"detalle\",\"cantidad\"],\"valor\":[\"201311\",\"S30 - T48 - B15 - A14\",\"88142 - 2100 - 2440 - 448\",\"1\",\"24869.95\",\"ENTEL - TELECEL MOVIL - BOLIVIATEL MOVIL - AXS MOVIL\",\"TROPICAL TOURS LTDA.\",\"1013389020\",\"45450135 - 45449165 - 45667247 - 45664793\",\"ENTEL = 24845.91 Bs - TELECEL MOVIL = 14.87 Bs - BOLIVIATEL MOVIL = 6.1 Bs - AXS MOVIL = 3.07 Bs\",\"4\"]}"
              }
            ]
          },
          {
            "select": [
              {
                "nombre": "factura",
                "valor": "45841676 - 45840761"
              },
              {
                "nombre": "monto",
                "valor": "29051.45"
              },
              {
                "nombre": "periodo",
                "valor": "201312"
              },
              {
                "nombre": "unic_value_string",
                "valor": "{\"mapVar\":{\"razonSocial\":\"TROPICAL TOURS LTDA.\",\"monto\":\"29051.45\",\"periodo\":\"201312\",\"idSeguimiento\":\"20067\",\"nit\":\"1013389020\",\"nro\":\"45840761\"},\"nombre\":[\"periodo\",\"loteDosificacion\",\"numeroRenta\",\"agrupador\",\"montoAPagar\",\"tipoFactura\",\"razonSocial\",\"nit\",\"factura\",\"detalle\",\"cantidad\"],\"valor\":[\"201312\",\"S30 - T48\",\"375720 - 2942\",\"1\",\"29051.45\",\"ENTEL - TELECEL MOVIL\",\"TROPICAL TOURS LTDA.\",\"1013389020\",\"45841676 - 45840761\",\"ENTEL = 29045.97 Bs - TELECEL MOVIL = 5.48 Bs\",\"2\"]}"
              }
            ]
          }
        ]
      }
    ]

  }

  dataConsumirPruebasObtenerDataRequest() {

    return [{ "paso": 1, "tipo": "table_list", "idComp": "table_list1", "data": { "nombre": "seleccionC", "valor": [{ "select": [{ "nombre": "id", "valor": "2" }] }] } }, { "paso": 2, "tipo": "table_list", "idComp": "table_list2", "data": { "nombre": "seleccionE", "valor": [{ "select": [{ "nombre": "id", "valor": "2" }] }] } }, { "paso": 3, "tipo": "table_list", "idComp": "table_list3", "data": { "nombre": "seleccionS", "valor": [{ "select": [{ "nombre": "id", "valor": "2" }] }] } }, { "paso": 4, "tipo": "table_list", "idComp": "table_list4", "data": { "nombre": "valorBusqueda", "valor": [{ "select": [{ "nombre": "valorBusqueda", "valor": "2", "visible": false, "tipo": "table_list", "tituloVista": "", "titulo": "Cuenta" }] }] } }, { "paso": 4, "tipo": "texbox", "idComp": "texbox1", "data": [{ "nombre": "codAcceso", "valor": "30193917", "tituloVista": "" }] }];

  }

  pruebas1() {

    return {
      "objeto": [
        {
          "accion": "ADEUDAS",
          "idService": "3",
          "pasosTotales": 2,
          "pasoActual": 1,
          "envioFactura": "false",
          "moneda": "BOB",
          "monto": "monto",
          "campoMoneda": "",
          "campoMonto": "",
          "nombre": "ENTEL Postpago (Paso 2)",
          "tipoServicio": "2",
          "componentes": [
            {
              "nombre": "",
              "tipo": "label",
              "atributos": [
                {
                  "idPtnConfirmacion": "Cuenta",
                  "dependiente": "false",
                  "camposDependientes": "[]",
                  "textInputMode": "",
                  "parametro": "",
                  "min": "",
                  "max": "",
                  "maxTextLength": "",
                  "minTextLength": "",
                  "restrictCharactersSet": "",
                  "formatCaracterSet": "",
                  "placeholder": "",
                  "secureTextEntry": "",
                  "selectDependienteParametro": "",
                  "values": "[]",
                  "multipleSeleccion": "",
                  "titulo": "Seleccione una cuenta:"
                }
              ]
            },
            {
              "nombre": "",
              "tipo": "table",
              "atributos": [
                {
                  "idPtnConfirmacion": "",
                  "dependiente": "false",
                  "camposDependientes": "[]",
                  "textInputMode": "",
                  "parametro": "Cuentas",
                  "min": "",
                  "max": "",
                  "maxTextLength": "",
                  "minTextLength": "",
                  "restrictCharactersSet": "",
                  "formatCaracterSet": "",
                  "placeholder": "",
                  "secureTextEntry": "",
                  "selectDependienteParametro": "",
                  "values": "[{\"lblT0\":{\"text\":\"Monto Deuda\",\"parametro\":\"\",\"isVisible\":\"true\"},\"lblV0\":{\"text\":\"53,921.4\",\"parametro\":\"montoDeuda\",\"isVisible\":\"true\"},\"lblT1\":{\"text\":\"Nombre\",\"parametro\":\"\",\"isVisible\":\"true\"},\"lblV1\":{\"text\":\"TROPICAL TOURS LTDA.\",\"parametro\":\"nombre\",\"isVisible\":\"true\"},\"lblT2\":{\"text\":\"Cuenta\",\"parametro\":\"\",\"isVisible\":\"true\"},\"lblV2\":{\"text\":\"30193917\",\"parametro\":\"cuenta\",\"isVisible\":\"true\"},\"lblT3\":{\"text\":\"unic_value_string\",\"parametro\":\"\",\"isVisible\":\"false\"},\"lblV3\":{\"text\":\"{\\\"mapVar\\\":{\\\"descripcion\\\":\\\"TROPICAL TOURS LTDA.\\\",\\\"idSeguimiento\\\":\\\"20067\\\",\\\"codCuenta\\\":\\\"30193917\\\"},\\\"nombre\\\":[\\\"montoDeuda\\\",\\\"tipoCuenta\\\",\\\"nombre\\\",\\\"cuenta\\\"],\\\"valor\\\":[\\\"53921.4\\\",\\\"TC_POST\\\",\\\"TROPICAL TOURS LTDA.\\\",\\\"30193917\\\"]}\",\"parametro\":\"unic_value_string\",\"isVisible\":\"false\"}}]",
                  "multipleSeleccion": "false",
                  "titulo": ""
                }
              ]
            }
          ]
        }
      ],
      "codigo": "OK",
      "mensaje": "Exito en la Transacci&#243;n",
      "opstatus": 0,
      "httpStatusCode": 200,
      "httpresponse": {
        "headers": {
          "Cache-Control": "no-cache,no-store,must-revalidate",
          "Expires": "0",
          "Pragma": "no-cache",
          "Access-Control-Allow-Methods": "GET, HEAD, POST, TRACE, OPTIONS, PUT, DELETE, PATCH",
          "X-Kony-RequestId": "611f6a54-744c-40d0-a437-7cbefb9c642d",
          "X-Android-Response-Source": "NETWORK 200",
          "X-Android-Sent-Millis": "1602273208388",
          "Content-Type": "application/json;charset=UTF-8",
          "Date": "Fri, 09 Oct 2020 19:55:01 GMT",
          "X-Content-Type-Options": "nosniff",
          "Server": "Kony",
          "Content-Length": "2096",
          "Strict-Transport-Security": "max-age=expireTime; includeSubDomains",
          "X-Android-Received-Millis": "1602273209002",
          "X-XSS-Protection": "1; mode=block",
          "X-Frame-Options": "SAMEORIGIN",
          "Access-Control-Allow-Origin": "*"
        },
        "url": "https://dbx426.sybven.com:8443/services/data/v1/RBObjectsBMSC/operations/PagoEstandar/GetFormulario",
        "responsecode": 200
      }
    }

  }

  pruebasFormatoMonto() {

    return [
      {
        "paso": 1,
        "tipo": "ListSimple",
        "idComp": "ListSimple1",
        "accion": "",
        "data": {
          "nombre": "seleccione",
          "valor": [
            {
              "select": [
                {
                  "nombre": "descripcion",
                  "valor": "Servicios de Telefonía, Internet, Cable, etc",
                  "visible": true,
                  "tipo": "ListSimple",
                  "tituloVista": "Categoria",
                  "titulo": "Descripcion"
                },
                {
                  "nombre": "codCuenta",
                  "valor": "Telecomunicaciones",
                  "visible": true,
                  "tipo": "ListSimple",
                  "tituloVista": "Categoria",
                  "titulo": "Nombre"
                },
                {
                  "nombre": "idCategoria",
                  "valor": "2",
                  "visible": false,
                  "tipo": "ListSimple",
                  "tituloVista": "Categoria",
                  "titulo": "idCategoria"
                }
              ]
            }
          ]
        }
      },
      {
        "paso": 2,
        "tipo": "ListSimple",
        "idComp": "ListSimple2",
        "accion": "",
        "data": {
          "nombre": "seleccione",
          "valor": [
            {
              "select": [
                {
                  "nombre": "idEmpresa",
                  "valor": "3",
                  "visible": false,
                  "tipo": "ListSimple",
                  "tituloVista": "Empresas",
                  "titulo": "idEmpresa"
                },
                {
                  "nombre": "descripcion",
                  "valor": "Distribuidora de Electricidad La Paz S.A.",
                  "visible": true,
                  "tipo": "ListSimple",
                  "tituloVista": "Empresas",
                  "titulo": "Descripcion"
                },
                {
                  "nombre": "codCuenta",
                  "valor": "ELECTROPAZ",
                  "visible": true,
                  "tipo": "ListSimple",
                  "tituloVista": "Empresas",
                  "titulo": "Nombre"
                },
                {
                  "nombre": "razonSocial",
                  "valor": "Distribuidora de Electricidad La Paz S.A.",
                  "visible": true,
                  "tipo": "ListSimple",
                  "tituloVista": "Empresas",
                  "titulo": "RazonSocial"
                }
              ]
            }
          ]
        }
      },
      {
        "paso": 3,
        "tipo": "ListSimple",
        "idComp": "ListSimple3",
        "accion": "",
        "data": {
          "nombre": "seleccione",
          "valor": [
            {
              "select": [
                {
                  "nombre": "codigoServicio",
                  "valor": "95",
                  "visible": false,
                  "tipo": "ListSimple",
                  "tituloVista": "Servicios",
                  "titulo": "codigoServicio"
                },
                {
                  "nombre": "descripcion",
                  "valor": "POSTPAGO",
                  "visible": true,
                  "tipo": "ListSimple",
                  "tituloVista": "Servicios",
                  "titulo": "Descripcion"
                },
                {
                  "nombre": "codCuenta",
                  "valor": "POSTPAGO_CESSA",
                  "visible": true,
                  "tipo": "ListSimple",
                  "tituloVista": "Servicios",
                  "titulo": "Nombre"
                },
                {
                  "nombre": "idServicio",
                  "valor": "9",
                  "visible": false,
                  "tipo": "ListSimple",
                  "tituloVista": "Servicios",
                  "titulo": "idServicio"
                },
                {
                  "nombre": "idEmpresa",
                  "valor": "8",
                  "visible": false,
                  "tipo": "ListSimple",
                  "tituloVista": "Servicios",
                  "titulo": "idEmpresa"
                }
              ]
            }
          ]
        }
      },
      {
        "paso": 4,
        "tipo": "texbox",
        "idComp": "texbox1",
        "accion": "ACUENTAS",
        "data": [
          {
            "nombre": "codAcceso",
            "valor": "1231231231",
            "tituloVista": "Codigo Fijó"
          }
        ]
      },
      {
        "paso": 5,
        "tipo": "table_select",
        "idComp": "DropList4",
        "accion": "ADEUDAS",
        "data": {
          "nombre": "seleccione",
          "valor": [
            {
              "select": [
                {
                  "nombre": " codParam ",
                  "valor": "text Line3",
                  "visible": false,
                  "tipo": "table_select",
                  "tituloVista": "Tipo de Busqueda",
                  "titulo": " cod param ",
                  "tituloVisible": "Prueba Texto"
                },
                {
                  "nombre": "valorBusqueda",
                  "valor": "002",
                  "visible": false,
                  "tipo": "table_select",
                  "tituloVista": "Tipo de Busqueda",
                  "titulo": "Valor busqueda",
                  "tituloVisible": "Prueba Texto"
                }
              ]
            }
          ]
        }
      },
      {
        "paso": 6,
        "tipo": "ListMultiple",
        "idComp": "ListMultiple1",
        "accion": "APROCESAR",
        "data": {
          "nombre": "seleccione",
          "valor": [
            {
              "select": [
                {
                  "nombre": "montoUSD",
                  "valor": "20",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "pruebaMO1"
                },
                {
                  "nombre": "mensaje",
                  "valor": "",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "Mensaje"
                },
                {
                  "nombre": "monto",
                  "valor": "1000.5",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "Monto"
                },
                {
                  "nombre": "nit",
                  "valor": "20007",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "NIT"
                },
                {
                  "nombre": "monto",
                  "valor": "4160.77",
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "4160.77"
                },
                {
                  "nombre": "nro",
                  "valor": "0000001",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "Nro"
                },
                {
                  "nombre": "montoBOB",
                  "valor": "1000.5",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "pruebaMO2"
                },
                {
                  "nombre": "periodo",
                  "valor": "01-2019",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "Periodo"
                },
                {
                  "nombre": "nombre",
                  "valor": "20007",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "Nombre"
                }
              ]
            },
            {
              "select": [
                {
                  "nombre": "montoUSD",
                  "valor": "20",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "pruebaMO1"
                },
                {
                  "nombre": "mensaje",
                  "valor": "",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "Mensaje"
                },
                {
                  "nombre": "monto",
                  "valor": "1000.5",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "Monto"
                },
                {
                  "nombre": "nit",
                  "valor": "20007",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "NIT"
                },
                {
                  "nombre": "monto",
                  "valor": "4160.77",
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "4160.77"
                },
                {
                  "nombre": "nro",
                  "valor": "0000001",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "Nro"
                },
                {
                  "nombre": "montoBOB",
                  "valor": "1000.5",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "pruebaMO2"
                },
                {
                  "nombre": "periodo",
                  "valor": "02-2019",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "Periodo"
                },
                {
                  "nombre": "nombre",
                  "valor": "20007",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "Nombre"
                }
              ]
            },
            {
              "select": [
                {
                  "nombre": "montoUSD",
                  "valor": "20",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "pruebaMO1"
                },
                {
                  "nombre": "mensaje",
                  "valor": "",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "Mensaje"
                },
                {
                  "nombre": "monto",
                  "valor": "1000.5",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "Monto"
                },
                {
                  "nombre": "nit",
                  "valor": "20007",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "NIT"
                },
                {
                  "nombre": "monto",
                  "valor": "4160.77",
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "4160.77"
                },
                {
                  "nombre": "nro",
                  "valor": "0000001",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "Nro"
                },
                {
                  "nombre": "montoBOB",
                  "valor": "1000.5",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "pruebaMO2"
                },
                {
                  "nombre": "periodo",
                  "valor": "03-2019",
                  "visible": true,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "Periodo"
                },
                {
                  "nombre": "nombre",
                  "valor": "20007",
                  "visible": false,
                  "tipo": "ListMultiple",
                  "tituloVista": "Facturas o Deudas",
                  "titulo": "Nombre"
                }
              ]
            }
          ]
        }
      }
    ];
  }

  formato1() {
    return {
      "codigo": "OK",
      "opstatus": 0,
      "objeto": [
        {
          "accion": "ACTION01",
          "idService": "1",
          "pasosTotales": "6",
          "pasoActual": "1",
          "envioFactura": "false",
          "moneda": "BOB",
          "monto": "monto",
          "campoMoneda": null,
          "campoMonto": null,
          "nombre": "",
          "componentes": [
            {
              "nombre": "",
              "tipo": "table",
              "atributos": [
                {
                  "dependiente": "",
                  "camposDependientes": [],
                  "textInputMode": "",
                  "parametro": "seleccione",
                  "maxTextLength": "",
                  "idPtnConfirmacion": "Categoria",
                  "minTextLength": "",
                  "restrictCharactersSet": "",
                  "formatCaracterSet": "",
                  "placeholder": "",
                  "secureTextEntry": "",
                  "selectDependienteParametro": "",
                  "values": [{

                    "lblT0": { "text": "Nombre", "isVisible": "true" },

                    "lblV0": { "text": "Servicios básicos", "parametro": "codCuenta", "isVisible": "true" },

                    "lblT1": { "text": "Descripcion", "isVisible": "true" },

                    "lblV1": { "text": "Servicios básicos: electricidad, agua, gas, etc.", "parametro": "descripcion", "isVisible": "true" },

                    "lblT2": { "text": "idCategoria", "isVisible": "false" },

                    "lblV2": { "text": "1", "parametro": "idCategoria", "isVisible": "false" }
                  },
                  {

                    "lblT0": { "text": "Nombre", "isVisible": "true" },

                    "lblV0": { "text": "Telecomunicaciones", "parametro": "codCuenta", "isVisible": "true" },

                    "lblT1": { "text": "Descripcion", "isVisible": "true" },

                    "lblV1": { "text": "Servicios de Telefonía, Internet, Cable, etc", "parametro": "descripcion", "isVisible": "true" },

                    "lblT2": { "text": "idCategoria", "isVisible": "false" },

                    "lblV2": { "text": "2", "parametro": "idCategoria", "isVisible": "false" }
                  },
                  {

                    "lblT0": { "text": "Nombre", "isVisible": "true" },

                    "lblV0": { "text": "Educación", "parametro": "codCuenta", "isVisible": "true" },

                    "lblT1": { "text": "Descripcion", "isVisible": "true" },

                    "lblV1": { "text": "Servicios educativos: colegios, universidades e Institutos", "parametro": "descripcion", "isVisible": "true" },

                    "lblT2": { "text": "idCategoria", "isVisible": "false" },

                    "lblV2": { "text": "3", "parametro": "idCategoria", "isVisible": "false" }
                  },
                  {

                    "lblT0": { "text": "Nombre", "isVisible": "true" },

                    "lblV0": { "text": "Banca", "parametro": "codCuenta", "isVisible": "true" },

                    "lblT1": { "text": "Descripcion", "isVisible": "true" },

                    "lblV1": { "text": "Servicios bancarios: pago de créditos, impuestos, etc", "parametro": "descripcion", "isVisible": "true" },

                    "lblT2": { "text": "idCategoria", "isVisible": "false" },

                    "lblV2": { "text": "4", "parametro": "idCategoria", "isVisible": "false" }
                  }
                  ],
                  "multipleSeleccion": "false",
                  "titulo": "Seleccione una Categoria ",
                }
              ]
            }
          ]
        }
      ],
      "mensaje": "Exito en la TransacciÃ³n",
      "httpStatusCode": 200
    }
  }

  formato2() {

    return {
      "codigo": "OK",
      "opstatus": 0,
      "objeto": [
        {
          "accion": "ACTION01",
          "idService": "1",
          "pasosTotales": "6",
          "pasoActual": "2",
          "envioFactura": "false",
          "moneda": "BOB",
          "monto": "monto",
          "campoMoneda": null,
          "campoMonto": null,
          "nombre": "",
          "componentes": [
            {
              "nombre": "",
              "tipo": "table",
              "atributos": [{
                "dependiente": "",
                "camposDependientes": [],
                "textInputMode": "",
                "parametro": "seleccione",
                "maxTextLength": "",
                "minTextLength": "",
                "restrictCharactersSet": "",
                "formatCaracterSet": "",
                "idPtnConfirmacion": "Empresas",
                "placeholder": "",
                "secureTextEntry": "",
                "selectDependienteParametro": "",
                "values": [{

                  "lblT0": { "text": "Nombre", "isVisible": "true" },

                  "lblV0": { "text": "ELFEC", "parametro": "codCuenta", "isVisible": "true" },

                  "lblT1": { "text": "Descripcion", "isVisible": "true" },

                  "lblV1": { "text": "Empresa de Luz y Fuerza Eléctrica Cochabamba S.A.", "parametro": "descripcion", "isVisible": "true" },

                  "lblT2": { "text": "RazonSocial", "isVisible": "true" },

                  "lblV2": { "text": "Empresa de Luz y Fuerza Eléctrica Cochabamba S.A.", "parametro": "razonSocial", "isVisible": "true" },

                  "lblT3": { "text": "idEmpresa", "isVisible": "false" },

                  "lblV3": { "text": "1", "parametro": "idEmpresa", "isVisible": "false" }

                }, {

                  "lblT0": { "text": "Nombre", "isVisible": "true" },

                  "lblV0": { "text": "ELECTROPAZ", "parametro": "codCuenta", "isVisible": "true" },

                  "lblT1": { "text": "Descripcion", "isVisible": "true" },

                  "lblV1": { "text": "Distribuidora de Electricidad La Paz S.A.", "parametro": "descripcion", "isVisible": "true" },

                  "lblT2": { "text": "RazonSocial", "isVisible": "true" },

                  "lblV2": { "text": "Distribuidora de Electricidad La Paz S.A.", "parametro": "razonSocial", "isVisible": "true" },

                  "lblT3": { "text": "idEmpresa", "isVisible": "false" },

                  "lblV3": { "text": "3", "parametro": "idEmpresa", "isVisible": "false" }

                }, {

                  "lblT0": { "text": "Nombre", "isVisible": "true" },

                  "lblV0": { "text": "ELFEC", "parametro": "codCuenta", "isVisible": "true" },

                  "lblT1": { "text": "Descripcion", "isVisible": "true" },

                  "lblV1": { "text": "Empresa de Luz y Fuerza Eléctrica Cochabamba S.A.", "parametro": "descripcion", "isVisible": "true" },

                  "lblT2": { "text": "RazonSocial", "isVisible": "true" },

                  "lblV2": { "text": "Empresa de Luz y Fuerza Eléctrica Cochabamba S.A.", "parametro": "razonSocial", "isVisible": "true" },

                  "lblT3": { "text": "idEmpresa", "isVisible": "false" },

                  "lblV3": { "text": "1", "parametro": "idEmpresa", "isVisible": "false" }

                }, {

                  "lblT0": { "text": "Nombre", "isVisible": "true" },

                  "lblV0": { "text": "SAGUAPAC", "parametro": "codCuenta", "isVisible": "true" },

                  "lblT1": { "text": "Descripcion", "isVisible": "true" },

                  "lblV1": { "text": "Servicio de Agua Potable y Alcantarillado", "parametro": "descripcion", "isVisible": "true" },

                  "lblT2": { "text": "RazonSocial", "isVisible": "true" },

                  "lblV2": { "text": "Servicio de Agua Potable y Alcantarillado", "parametro": "razonSocial", "isVisible": "true" },

                  "lblT3": { "text": "idEmpresa", "isVisible": "false" },

                  "lblV3": { "text": "4", "parametro": "idEmpresa", "isVisible": "false" }

                }, {

                  "lblT0": { "text": "Nombre", "isVisible": "true" },

                  "lblV0": { "text": "CRE", "parametro": "codCuenta", "isVisible": "true" },

                  "lblT1": { "text": "Descripcion", "isVisible": "true" },

                  "lblV1": { "text": "Cooperativa Rural de Electrificación RL", "parametro": "descripcion", "isVisible": "true" },

                  "lblT2": { "text": "RazonSocial", "isVisible": "true" },

                  "lblV2": { "text": "Cooperativa Rural de Electrificación RL", "parametro": "razonSocial", "isVisible": "true" },

                  "lblT3": { "text": "idEmpresa", "isVisible": "false" },

                  "lblV3": { "text": "5", "parametro": "idEmpresa", "isVisible": "false" }

                }, {

                  "lblT0": { "text": "Nombre", "isVisible": "true" },

                  "lblV0": { "text": "CESSA", "parametro": "codCuenta", "isVisible": "true" },

                  "lblT1": { "text": "Descripcion", "isVisible": "true" },

                  "lblV1": { "text": "Compañía Eléctrica Sucre SA", "parametro": "descripcion", "isVisible": "true" },

                  "lblT2": { "text": "RazonSocial", "isVisible": "true" },

                  "lblV2": { "text": "Compañía Eléctrica Sucre SA", "parametro": "razonSocial", "isVisible": "true" },

                  "lblT3": { "text": "idEmpresa", "isVisible": "false" },

                  "lblV3": { "text": "8", "parametro": "idEmpresa", "isVisible": "false" }

                }
                ],
                "multipleSeleccion": "false",
                "titulo": "Seleccione una Empresa",
              }]
            }
          ]
        }
      ],
      "mensaje": "Exito en la TransacciÃ³n",
      "httpStatusCode": 200
    }
  }

  formato3() {
    return {
      "codigo": "OK",
      "opstatus": 0,
      "objeto": [
        {
          "accion": "ACTION01",
          "idService": "1",
          "pasosTotales": "6",
          "pasoActual": "3",
          "envioFactura": "false",
          "moneda": "BOB",
          "monto": "monto",
          "campoMoneda": null,
          "campoMonto": null,
          "nombre": "",
          "componentes": [
            {
              "nombre": "",
              "tipo": "table",
              "atributos": [{
                "dependiente": "",
                "camposDependientes": [],
                "textInputMode": "",
                "parametro": "seleccione",
                "maxTextLength": "",
                "minTextLength": "",
                "idPtnConfirmacion": "Servicios",
                "restrictCharactersSet": "",
                "formatCaracterSet": "",
                "placeholder": "",
                "secureTextEntry": "",
                "selectDependienteParametro": "",
                "values": [
                  {

                    "lblT0": { "text": "Nombre", "isVisible": "true" },
                    "lblV0": { "text": "POSTPAGO_ELFEC", "parametro": "codCuenta", "isVisible": "true" },

                    "lblT1": { "text": "Descripcion", "isVisible": "true" },
                    "lblV1": { "text": "POSTPAGO", "parametro": "descripcion", "isVisible": "true" },

                    "lblT2": { "text": "idServicio", "isVisible": "false" },
                    "lblV2": { "text": "1", "parametro": "idServicio", "isVisible": "false" },

                    "lblT3": { "text": "codigoServicio", "isVisible": "false" },
                    "lblV3": { "text": "76", "parametro": "codigoServicio", "isVisible": "false" },

                    "lblT4": { "text": "idEmpresa", "isVisible": "false" },
                    "lblV4": { "text": "1", "parametro": "idEmpresa", "isVisible": "false" }
                  }, {

                    "lblT0": { "text": "Nombre", "isVisible": "true" },
                    "lblV0": { "text": "POSTPAGO_DELAPAZ", "parametro": "codCuenta", "isVisible": "true" },

                    "lblT1": { "text": "Descripcion", "isVisible": "true" },
                    "lblV1": { "text": "POSTPAGO", "parametro": "descripcion", "isVisible": "true" },

                    "lblT2": { "text": "idServicio", "isVisible": "false" },
                    "lblV2": { "text": "4", "parametro": "idServicio", "isVisible": "false" },

                    "lblT3": { "text": "codigoServicio", "isVisible": "false" },
                    "lblV3": { "text": "31", "parametro": "codigoServicio", "isVisible": "false" },

                    "lblT4": { "text": "idEmpresa", "isVisible": "false" },
                    "lblV4": { "text": "3", "parametro": "idEmpresa", "isVisible": "false" }
                  }, {

                    "lblT0": { "text": "Nombre", "isVisible": "true" },
                    "lblV0": { "text": "POSTPAGO_SAGUAPAC", "parametro": "codCuenta", "isVisible": "true" },

                    "lblT1": { "text": "Descripcion", "isVisible": "true" },
                    "lblV1": { "text": "POSTPAGO", "parametro": "descripcion", "isVisible": "true" },

                    "lblT2": { "text": "idServicio", "isVisible": "false" },
                    "lblV2": { "text": "6", "parametro": "idServicio", "isVisible": "false" },

                    "lblT3": { "text": "codigoServicio", "isVisible": "false" },
                    "lblV3": { "text": "7", "parametro": "codigoServicio", "isVisible": "false" },

                    "lblT4": { "text": "idEmpresa", "isVisible": "false" },
                    "lblV4": { "text": "4", "parametro": "idEmpresa", "isVisible": "false" }
                  }, {

                    "lblT0": { "text": "Nombre", "isVisible": "true" },
                    "lblV0": { "text": "POSTPAGO_CRE", "parametro": "codCuenta", "isVisible": "true" },

                    "lblT1": { "text": "Descripcion", "isVisible": "true" },
                    "lblV1": { "text": "POSTPAGO", "parametro": "descripcion", "isVisible": "true" },

                    "lblT2": { "text": "idServicio", "isVisible": "false" },
                    "lblV2": { "text": "5", "parametro": "idServicio", "isVisible": "false" },

                    "lblT3": { "text": "codigoServicio", "isVisible": "false" },
                    "lblV3": { "text": "24", "parametro": "codigoServicio", "isVisible": "false" },

                    "lblT4": { "text": "idEmpresa", "isVisible": "false" },
                    "lblV4": { "text": "5", "parametro": "idEmpresa", "isVisible": "false" }
                  }, {

                    "lblT0": { "text": "Nombre", "isVisible": "true" },
                    "lblV0": { "text": "POSTPAGO_CESSA", "parametro": "codCuenta", "isVisible": "true" },

                    "lblT1": { "text": "Descripcion", "isVisible": "true" },
                    "lblV1": { "text": "POSTPAGO", "parametro": "descripcion", "isVisible": "true" },

                    "lblT2": { "text": "idServicio", "isVisible": "false" },
                    "lblV2": { "text": "9", "parametro": "idServicio", "isVisible": "false" },

                    "lblT3": { "text": "codigoServicio", "isVisible": "false" },
                    "lblV3": { "text": "95", "parametro": "codigoServicio", "isVisible": "false" },

                    "lblT4": { "text": "idEmpresa", "isVisible": "false" },
                    "lblV4": { "text": "8", "parametro": "idEmpresa", "isVisible": "false" }
                  }
                ],
                "multipleSeleccion": "false",
                "titulo": "Seleccione un Servicio",
              }]
            }
          ]
        }
      ],
      "mensaje": "Exito en la TransacciÃ³n",
      "httpStatusCode": 200
    }
  }

  ejemploGibreska() {
    return {
      "codigo": "OK",
      "opstatus": 0,
      "objeto": [
        {
          "accion": "ACUENTAS",
          "campoMoneda": "",
          "campoMonto": "",
          "componentes": [
            {
              "tipo": "label",
              "nombre": "",
              "atributos": [
                {
                  "max": "",
                  "parametro": "",
                  "values": "[]",
                  "secureTextEntry": "",
                  "titulo": "Seleccione el tipo de bÃºsqueda e introduzca la cuenta correspondiente:",
                  "maxTextLength": "",
                  "formatCaracterSet": "",
                  "multipleSeleccion": "",
                  "camposDependientes": "[]",
                  "minTextLength": "",
                  "idPtnConfirmacion": "Tipo de bÃºsqueda",
                  "min": "",
                  "restrictCharactersSet": "",
                  "selectDependienteParametro": "",
                  "dependiente": "false",
                  "placeholder": "",
                  "textInputMode": ""
                }
              ]
            },
            {
              "tipo": "table_list",
              "nombre": "",
              "atributos": [
                {
                  "max": "",
                  "parametro": "valorBusqueda",
                  "values": "[{\"lblT0\":{\"text\":\"Cliente\",\"parametro\":\"\",\"isVisible\":\"true\"},\"lblV0\":{\"text\":\"1\",\"parametro\":\"valorBusqueda\",\"isVisible\":\"false\"},\"lblT1\":{\"text\":\"\",\"parametro\":\"\",\"isVisible\":\"\"},\"lblV1\":{\"text\":\"\",\"parametro\":\"\",\"isVisible\":\"\"},\"lblT2\":{\"text\":\"\",\"parametro\":\"\",\"isVisible\":\"\"},\"lblV2\":{\"text\":\"\",\"parametro\":\"\",\"isVisible\":\"\"},\"lblT3\":{\"text\":\"\",\"parametro\":\"\",\"isVisible\":\"\"},\"lblV3\":{\"text\":\"\",\"parametro\":\"\",\"isVisible\":\"\"}},{\"lblT0\":{\"text\":\"Cuenta\",\"parametro\":\"\",\"isVisible\":\"true\"},\"lblV0\":{\"text\":\"2\",\"parametro\":\"valorBusqueda\",\"isVisible\":\"false\"},\"lblT1\":{\"text\":\"\",\"parametro\":\"\",\"isVisible\":\"\"},\"lblV1\":{\"text\":\"\",\"parametro\":\"\",\"isVisible\":\"\"},\"lblT2\":{\"text\":\"\",\"parametro\":\"\",\"isVisible\":\"\"},\"lblV2\":{\"text\":\"\",\"parametro\":\"\",\"isVisible\":\"\"},\"lblT3\":{\"text\":\"\",\"parametro\":\"\",\"isVisible\":\"\"},\"lblV3\":{\"text\":\"\",\"parametro\":\"\",\"isVisible\":\"\"}},{\"lblT0\":{\"text\":\"TelÃ©fono\",\"parametro\":\"\",\"isVisible\":\"true\"},\"lblV0\":{\"text\":\"3\",\"parametro\":\"valorBusqueda\",\"isVisible\":\"false\"},\"lblT1\":{\"text\":\"\",\"parametro\":\"\",\"isVisible\":\"\"},\"lblV1\":{\"text\":\"\",\"parametro\":\"\",\"isVisible\":\"\"},\"lblT2\":{\"text\":\"\",\"parametro\":\"\",\"isVisible\":\"\"},\"lblV2\":{\"text\":\"\",\"parametro\":\"\",\"isVisible\":\"\"},\"lblT3\":{\"text\":\"\",\"parametro\":\"\",\"isVisible\":\"\"},\"lblV3\":{\"text\":\"\",\"parametro\":\"\",\"isVisible\":\"\"}}]",
                  "secureTextEntry": "",
                  "titulo": "",
                  "maxTextLength": "",
                  "formatCaracterSet": "",
                  "multipleSeleccion": "false",
                  "camposDependientes": "[]",
                  "minTextLength": "",
                  "idPtnConfirmacion": "",
                  "min": "",
                  "restrictCharactersSet": "",
                  "selectDependienteParametro": "",
                  "dependiente": "false",
                  "placeholder": "",
                  "textInputMode": ""
                }
              ]
            },
            {
              "tipo": "input",
              "nombre": "Cuenta",
              "atributos": [
                {
                  "max": "",
                  "parametro": "codAcceso",
                  "values": "[]",
                  "secureTextEntry": "false",
                  "titulo": "",
                  "maxTextLength": "15",
                  "formatCaracterSet": "^\\d+$",
                  "multipleSeleccion": "",
                  "camposDependientes": "[]",
                  "minTextLength": "1",
                  "idPtnConfirmacion": "",
                  "min": "",
                  "restrictCharactersSet": "!@#$%",
                  "selectDependienteParametro": "",
                  "dependiente": "false",
                  "placeholder": "Ingresar Cuenta",
                  "textInputMode": "N"
                }
              ]
            }
          ],
          "monto": "monto",
          "tipoServicio": "2",
          "pasoActual": "1",
          "envioFactura": "false",
          "idService": "3",
          "moneda": "BOB",
          "pasosTotales": "3",
          "nombre": "ENTEL Postpago (Paso 1)"
        }
      ],
      "mensaje": "Exito en la TransacciÃ³n",
      "httpStatusCode": 200
    }
  }

  saguaPostPruebasInput() {

    return {
      "codigo": "OK",
      "opstatus": 0,
      "objeto": [
        {
          "accion": "ACTION01",
          "idService": "1",
          "pasosTotales": "3",
          "pasoActual": "1",
          "tipoServicio": "postpago",
          "envioFactura": "false",
          "moneda": "BOB",
          "monto": "monto",
          "campoMoneda": null,
          "campoMonto": null,
          "nombre": "",
          "componentes": [
            {
              "nombre": "",    // este campo representara un label para la estructura tipo input podrá colocar el mensaje a mostrar  aquí .      
              "tipo": "label",
              "atributos": [{
                "dependiente": "",
                "camposDependientes": [],
                "selectDependienteParametro": "",
                "textInputMode": "",
                "parametro": "",
                "maxTextLength": "",
                "minTextLength": "",
                "restrictCharactersSet": "",
                "formatCaracterSet": "",
                "placeholder": "",
                "secureTextEntry": "",
                "values": [],
                "multipleSeleccion": "",
                "titulo": "Introduzca su código fijo:"
              }]
            },
            {
              "nombre": "Código Fijo (ej. 0)",    // este campo representara un label para la estructura tipo input podrá colocar el mensaje a mostrar  aquí .      
              "tipo": "input",
              "atributos": [{
                "dependiente": "",
                "camposDependientes": [],
                "selectDependienteParametro": "",
                "textInputMode": "N",
                "parametro": "codAcceso",
                "min": "1",
                "max": "50000",
                "maxTextLength": "6",
                "minTextLength": "2",
                "restrictCharactersSet": "~!@#$%^&*()_+=-`,.<>/?;:'[]{}|/asdfghjklqwertyuiopzxcvbnmñáéíúóQWERTYUIOPASDFGHJKLZXCVBNMÁÉÚÍÓÑ",
                "formatCaracterSet": "",
                "placeholder": "Código fijo",
                "secureTextEntry": "",
                "values": [],
                "multipleSeleccion": "",
                "titulo": "Introduzca su codigo Impo:"
              }]
            },
            {
              "nombre": "Código Fijo (ej. 1)",    // este campo representara un label para la estructura tipo input podrá colocar el mensaje a mostrar  aquí .      
              "tipo": "input",
              "atributos": [{
                "dependiente": "",
                "camposDependientes": [],
                "selectDependienteParametro": "",
                "textInputMode": "N",
                "parametro": "codAcceso",
                "min": "",
                "max": "",
                "maxTextLength": "6",
                "minTextLength": "2",
                "restrictCharactersSet": "~!@#$%^&*()_+=-`,.<>/?;:'[]{}|/asdfghjklqwertyuiopzxcvbnmñáéíúóQWERTYUIOPASDFGHJKLZXCVBNMÁÉÚÍÓÑ",
                "formatCaracterSet": "",
                "placeholder": "Código fijo",
                "secureTextEntry": "",
                "values": [],
                "multipleSeleccion": "",
                "titulo": "Introduzca su codigo chuun:"
              }]
            },
            {
              "nombre": "Código Fijo (ej. 2)",    // este campo representara un label para la estructura tipo input podrá colocar el mensaje a mostrar  aquí .      
              "tipo": "input",
              "atributos": [{
                "dependiente": "",
                "camposDependientes": [],
                "selectDependienteParametro": "",
                "textInputMode": "N",
                "parametro": "codAcceso",
                "min": "10",
                "max": "20",
                "maxTextLength": "4",
                "minTextLength": "2",
                "restrictCharactersSet": "~!@#$%^&*()_+=-`,.<>/?;:'[]{}|/asdfghjklqwertyuiopzxcvbnmñáéíúóQWERTYUIOPASDFGHJKLZXCVBNMÁÉÚÍÓÑ",
                "formatCaracterSet": "",
                "placeholder": "Código fijo",
                "secureTextEntry": "",
                "values": [],
                "multipleSeleccion": "",
                "titulo": "Introduzca su codigo rombo:"
              }]
            },
            {
              "nombre": "Código Fijo (ej. 3)",    // este campo representara un label para la estructura tipo input podrá colocar el mensaje a mostrar  aquí .      
              "tipo": "input",
              "atributos": [{
                "dependiente": "",
                "camposDependientes": [],
                "selectDependienteParametro": "",
                "textInputMode": "N",
                "parametro": "codAcceso",
                "min": "5",
                "max": "20",
                "maxTextLength": "6",
                "minTextLength": "2",
                "restrictCharactersSet": "~!@#$%^&*()_+=-`,.<>/?;:'[]{}|/asdfghjklqwertyuiopzxcvbnmñáéíúóQWERTYUIOPASDFGHJKLZXCVBNMÁÉÚÍÓÑ",
                "formatCaracterSet": "",
                "placeholder": "Código fijo",
                "secureTextEntry": "",
                "values": [],
                "multipleSeleccion": "",
                "titulo": "Introduzca su codigo criollo:"
              }]
            }
          ]
        }
      ],
      "mensaje": "Exito en la TransacciÃ³n",
      "httpStatusCode": 200
    }
  }

  saguaPost1() {

    return {
      "codigo": "OK",
      "opstatus": 0,
      "objeto": [
        {
          "accion": "ACTION01",
          "idService": "1",
          "pasosTotales": "6",
          "pasoActual": "4",
          "tipoServicio": "postpago",
          "envioFactura": "false",
          "moneda": "BOB",
          "monto": "monto",
          "campoMoneda": null,
          "campoMonto": null,
          "nombre": "",
          "componentes": [
            {
              "nombre": "",    // este campo representara un label para la estructura tipo input podrá colocar el mensaje a mostrar  aquí .      
              "tipo": "label",
              "atributos": [{
                "dependiente": "",
                "camposDependientes": [],
                "selectDependienteParametro": "",
                "textInputMode": "",
                "parametro": "",
                "maxTextLength": "",
                "minTextLength": "",
                "restrictCharactersSet": "",
                "formatCaracterSet": "",
                "placeholder": "",
                "secureTextEntry": "",
                "values": [],
                "multipleSeleccion": "",
                "titulo": "Introduzca su código fijo:"
              }]
            },
            {
              "nombre": "Código Fijo (ej. 2415364)",    // este campo representara un label para la estructura tipo input podrá colocar el mensaje a mostrar  aquí .      
              "tipo": "input",
              "atributos": [{
                "dependiente": "",
                "camposDependientes": [],
                "selectDependienteParametro": "",
                "textInputMode": "N",
                "idPtnConfirmacion": "Codigo Fijó",
                "parametro": "codAcceso",
                "maxTextLength": "15",
                "minTextLength": "10",
                "restrictCharactersSet": "~!@#$%^&*()_+=-`,.<>/?;:'[]{}|/asdfghjklqwertyuiopzxcvbnmñáéíúóQWERTYUIOPASDFGHJKLZXCVBNMÁÉÚÍÓÑ",
                "formatCaracterSet": "",
                "placeholder": "Código fijo",
                "secureTextEntry": "",
                "values": [],
                "multipleSeleccion": "",
                "titulo": "Introduzca su codigo :"
              }]
            }
          ]
        }
      ],
      "mensaje": "Exito en la TransacciÃ³n",
      "httpStatusCode": 200
    }
  }

  saguaPost2() {

    return {
      "codigo": "OK",
      "opstatus": 0,
      "objeto": [
        {
          "accion": "ACTION01",
          "idService": "1",
          "pasosTotales": "6",
          "pasoActual": "5",
          "tipoServicio": "postpago",
          "envioFactura": "false",
          "moneda": "BOB",
          "monto": "monto",
          "campoMoneda": null,
          "campoMonto": null,
          "nombre": "",
          "componentes": [
            {
              "nombre": "",
              "tipo": "table_select",
              "atributos": [{
                "dependiente": "",
                "camposDependientes": [],
                "textInputMode": "",
                "parametro": "seleccione",
                "maxTextLength": "",
                "minTextLength": "",
                "restrictCharactersSet": "",
                "formatCaracterSet": "",
                "placeholder": "",
                "idPtnConfirmacion": "Tipo de Busqueda",
                "secureTextEntry": "",
                "selectDependienteParametro": "",
                "values": [{
                  "lblV2": {
                    "parametro": " codParam ",
                    "isVisible": "false",
                    "text": "text Line3"
                  },
                  "lblV1": {
                    "parametro": "valorBusqueda",
                    "isVisible": "false",
                    "text": "001"
                  },
                  "lblV0": {
                    "parametro": "",
                    "isVisible": "true",
                    "text": "Cuenta"
                  }
                },
                {
                  "lblV2": {
                    "parametro": " codParam ",
                    "isVisible": "false",
                    "text": "text Line3"
                  },
                  "lblV1": {
                    "parametro": "valorBusqueda",
                    "isVisible": "false",
                    "text": "002"
                  },
                  "lblV0": {
                    "parametro": "",
                    "isVisible": "true",
                    "text": "Prueba Texto"
                  }
                }
                ],
                "multipleSeleccion": "false",
                "titulo": "Seleccione una Cuenta",
              }]
            }
          ]

        }
      ],
      "mensaje": "Exito en la TransacciÃ³n",
      "httpStatusCode": 200
    }
  }

  saguaPost3() {

    return {
      "codigo": "OK",
      "opstatus": 0,
      "objeto": [
        {
          "accion": "ACTION01",
          "idService": "1",
          "pasosTotales": "6",
          "pasoActual": "6",
          "tipoServicio": "postpago",
          "envioFactura": "true",
          "moneda": "BOB",
          "monto": "monto",
          "campoMoneda": null,
          "campoMonto": null,
          "nombre": "",
          "componentes": [
            {
              "nombre": "",
              "tipo": "table",
              "atributos": [{
                "dependiente": "true",
                "camposDependientes": [],
                "textInputMode": "",
                "parametro": "seleccione",
                "maxTextLength": "",
                "minTextLength": "",
                "restrictCharactersSet": "",
                "formatCaracterSet": "",
                "placeholder": "",
                "idPtnConfirmacion": "Facturas o Deudas",
                "secureTextEntry": "",
                "selectDependienteParametro": "periodo",
                "values": [
                  {
                    "lblT0": { "text": "Nro", "isVisible": "true" },

                    "lblV0": { "text": "0000001", "parametro": "nro", "isVisible": "true" },

                    "lblT1": { "text": "Monto", "isVisible": "true" },

                    "lblV1": { "text": "100", "parametro": "monto", "isVisible": "true" },

                    "lblT2": { "text": "Periodo", "isVisible": "true" },

                    "lblV2": { "text": "01-2019", "parametro": "periodo", "isVisible": "true" },

                    "lblT3": { "text": "Mensaje", "isVisible": "false" },

                    "lblV3": { "text": "", "parametro": "mensaje", "isVisible": "false" },

                    "lblT4": { "text": "Nombre", "isVisible": "false" },

                    "lblV4": { "text": "20007", "parametro": "nombre", "isVisible": "false" },

                    "lblT5": { "text": "NIT", "isVisible": "false" },

                    "lblV5": { "text": "20007", "parametro": "nit", "isVisible": "false" },

                    "lblT6": { "text": "pruebaMO1", "isVisible": "false" },

                    "lblV6": { "text": "20", "parametro": "montoUSD", "isVisible": "false" },

                    "lblT7": { "text": "pruebaMO2", "isVisible": "false" },

                    "lblV7": { "text": "1000.5", "parametro": "montoBOB", "isVisible": "false" }
                  },
                  {
                    "lblT0": { "text": "Nro", "isVisible": "true" },

                    "lblV0": { "text": "0000001", "parametro": "nro", "isVisible": "true" },

                    "lblT1": { "text": "Monto", "isVisible": "true" },

                    "lblV1": { "text": "100", "parametro": "monto", "isVisible": "true" },

                    "lblT2": { "text": "Periodo", "isVisible": "true" },

                    "lblV2": { "text": "02-2018", "parametro": "periodo", "isVisible": "true" },

                    "lblT3": { "text": "Mensaje", "isVisible": "false" },

                    "lblV3": { "text": "", "parametro": "mensaje", "isVisible": "false" },

                    "lblT4": { "text": "Nombre", "isVisible": "false" },

                    "lblV4": { "text": "20007", "parametro": "nombre", "isVisible": "false" },

                    "lblT5": { "text": "NIT", "isVisible": "false" },

                    "lblV5": { "text": "20007", "parametro": "nit", "isVisible": "false" },

                    "lblT6": { "text": "pruebaMO1", "isVisible": "false" },

                    "lblV6": { "text": "20", "parametro": "montoUSD", "isVisible": "false" },

                    "lblT7": { "text": "pruebaMO2", "isVisible": "false" },

                    "lblV7": { "text": "1000.5", "parametro": "montoBOB", "isVisible": "false" }
                  },
                  {
                    "lblT0": { "text": "Nro", "isVisible": "true" },

                    "lblV0": { "text": "0000001", "parametro": "nro", "isVisible": "true" },

                    "lblT1": { "text": "Monto", "isVisible": "true" },

                    "lblV1": { "text": "100", "parametro": "monto", "isVisible": "true" },

                    "lblT2": { "text": "Periodo", "isVisible": "true" },

                    "lblV2": { "text": "05-2020", "parametro": "periodo", "isVisible": "true" },

                    "lblT3": { "text": "Mensaje", "isVisible": "false" },

                    "lblV3": { "text": "", "parametro": "mensaje", "isVisible": "false" },

                    "lblT4": { "text": "Nombre", "isVisible": "false" },

                    "lblV4": { "text": "20007", "parametro": "nombre", "isVisible": "false" },

                    "lblT5": { "text": "NIT", "isVisible": "false" },

                    "lblV5": { "text": "20007", "parametro": "nit", "isVisible": "false" },

                    "lblT6": { "text": "pruebaMO1", "isVisible": "false" },

                    "lblV6": { "text": "20", "parametro": "montoUSD", "isVisible": "false" },

                    "lblT7": { "text": "pruebaMO2", "isVisible": "false" },

                    "lblV7": { "text": "1000.5", "parametro": "montoBOB", "isVisible": "false" }
                  },
                  {
                    "lblT0": { "text": "Nro", "isVisible": "true" },

                    "lblV0": { "text": "0000001", "parametro": "nro", "isVisible": "true" },

                    "lblT1": { "text": "Monto", "isVisible": "true" },

                    "lblV1": { "text": "4160.77", "parametro": "monto", "isVisible": "true" },

                    "lblT2": { "text": "Periodo", "isVisible": "true" },

                    "lblV2": { "text": "09-2017", "parametro": "periodo", "isVisible": "true" },

                    "lblT3": { "text": "Mensaje", "isVisible": "false" },

                    "lblV3": { "text": "", "parametro": "mensaje", "isVisible": "false" },

                    "lblT4": { "text": "Nombre", "isVisible": "false" },

                    "lblV4": { "text": "20007", "parametro": "nombre", "isVisible": "false" },

                    "lblT5": { "text": "NIT", "isVisible": "false" },

                    "lblV5": { "text": "20007", "parametro": "nit", "isVisible": "false" },

                    "lblT6": { "text": "pruebaMO1", "isVisible": "false" },

                    "lblV6": { "text": "20", "parametro": "montoUSD", "isVisible": "false" },

                    "lblT7": { "text": "pruebaMO2", "isVisible": "false" },

                    "lblV7": { "text": "1000.5", "parametro": "montoBOB", "isVisible": "false" }
                  },
                  {
                    "lblT0": { "text": "Nro", "isVisible": "true" },

                    "lblV0": { "text": "0000001", "parametro": "nro", "isVisible": "true" },

                    "lblT1": { "text": "Monto", "isVisible": "true" },

                    "lblV1": { "text": "4160.77", "parametro": "monto", "isVisible": "true" },

                    "lblT2": { "text": "Periodo", "isVisible": "true" },

                    "lblV2": { "text": "10-2017", "parametro": "periodo", "isVisible": "true" },

                    "lblT3": { "text": "Mensaje", "isVisible": "false" },

                    "lblV3": { "text": "", "parametro": "mensaje", "isVisible": "false" },

                    "lblT4": { "text": "Nombre", "isVisible": "false" },

                    "lblV4": { "text": "20007", "parametro": "nombre", "isVisible": "false" },

                    "lblT5": { "text": "NIT", "isVisible": "false" },

                    "lblV5": { "text": "20007", "parametro": "nit", "isVisible": "false" },

                    "lblT6": { "text": "pruebaMO1", "isVisible": "false" },

                    "lblV6": { "text": "20", "parametro": "montoUSD", "isVisible": "false" },

                    "lblT7": { "text": "pruebaMO2", "isVisible": "false" },

                    "lblV7": { "text": "1000.01", "parametro": "montoBOB", "isVisible": "false" }
                  },
                  {
                    "lblT0": { "text": "Nro", "isVisible": "true" },

                    "lblV0": { "text": "0000001", "parametro": "nro", "isVisible": "true" },

                    "lblT1": { "text": "Monto", "isVisible": "true" },

                    "lblV1": { "text": "4160.77", "parametro": "monto", "isVisible": "true" },

                    "lblT2": { "text": "Periodo", "isVisible": "true" },

                    "lblV2": { "text": "06-2018", "parametro": "periodo", "isVisible": "true" },

                    "lblT3": { "text": "Mensaje", "isVisible": "false" },

                    "lblV3": { "text": "", "parametro": "mensaje", "isVisible": "false" },

                    "lblT4": { "text": "Nombre", "isVisible": "false" },

                    "lblV4": { "text": "20007", "parametro": "nombre", "isVisible": "false" },

                    "lblT5": { "text": "NIT", "isVisible": "false" },

                    "lblV5": { "text": "20007", "parametro": "nit", "isVisible": "false" },

                    "lblT6": { "text": "pruebaMO1", "isVisible": "false" },

                    "lblV6": { "text": "20", "parametro": "montoUSD", "isVisible": "false" },

                    "lblT7": { "text": "pruebaMO2", "isVisible": "false" },

                    "lblV7": { "text": "1000.01", "parametro": "montoBOB", "isVisible": "false" }
                  },
                  {
                    "lblT0": { "text": "Nro", "isVisible": "true" },

                    "lblV0": { "text": "0000001", "parametro": "nro", "isVisible": "true" },

                    "lblT1": { "text": "Monto", "isVisible": "true" },

                    "lblV1": { "text": "4160.77", "parametro": "monto", "isVisible": "true" },

                    "lblT2": { "text": "Periodo", "isVisible": "true" },

                    "lblV2": { "text": "03-2015", "parametro": "periodo", "isVisible": "true" },

                    "lblT3": { "text": "Mensaje", "isVisible": "false" },

                    "lblV3": { "text": "", "parametro": "mensaje", "isVisible": "false" },

                    "lblT4": { "text": "Nombre", "isVisible": "false" },

                    "lblV4": { "text": "20007", "parametro": "nombre", "isVisible": "false" },

                    "lblT5": { "text": "NIT", "isVisible": "false" },

                    "lblV5": { "text": "20007", "parametro": "nit", "isVisible": "false" },

                    "lblT6": { "text": "pruebaMO1", "isVisible": "false" },

                    "lblV6": { "text": "20", "parametro": "montoUSD", "isVisible": "false" },

                    "lblT7": { "text": "pruebaMO2", "isVisible": "false" },

                    "lblV7": { "text": "1000", "parametro": "montoBOB", "isVisible": "false" }
                  }
                ],
                "multipleSeleccion": "true",
                "titulo": "Seleccione la deuda a ser pagada:",
              }]
            }
          ]

        }
      ],
      "mensaje": "Exito en la TransacciÃ³n",
      "httpStatusCode": 200
    }
  }

  entelPre1() {

    return {
      "codigo": "OK",
      "opstatus": 0,
      "objeto": [
        {
          "accion": "ACTION01",
          "idService": "1",
          "pasosTotales": "2",
          "pasoActual": "0",
          "tipoServicio": "Prepago",
          "nombre": "",
          "componentes": [
            {
              "nombre": "",
              "tipo": "label",
              "atributos": [{
                "dependiente": "",
                "camposDependientes": [],
                "selectDependienteParametro": "",
                "textInputMode": "",
                "parametro": "",
                "maxTextLength": "",
                "minTextLength": "",
                "restrictCharactersSet": "",
                "formatCaracterSet": "",
                "placeholder": "",
                "secureTextEntry": "",
                "values": [],
                "multipleSeleccion": "",
                "titulo": "Introduzca su cuenta :"
              }]
            },
            {
              "nombre": "Cuenta (ej. 2415364)",    // este campo representara un label para la estructura tipo input podrá colocar el mensaje a mostrar  aquí .      
              "tipo": "input",
              "atributos": [{
                "dependiente": "",
                "camposDependientes": [],
                "selectDependienteParametro": "",
                "textInputMode": "N",
                "parametro": "codAcceso",
                "maxTextLength": "10",
                "minTextLength": "8",
                "restrictCharactersSet": "~!@#$%^&*()_+=-`,.<>/?;:'[]{}|/asdfghjklqwertyuiopzxcvbnmñáéíúóQWERTYUIOPASDFGHJKLZXCVBNMÁÉÚÍÓÑ",
                "formatCaracterSet": "",
                "placeholder": "Cuenta",
                "secureTextEntry": "",
                "values": [],
                "multipleSeleccion": "",
                "titulo": "Introduzca su cuenta :"
              }]
            }

          ]

        }
      ],
      "mensaje": "Exito en la TransacciÃ³n",
      "httpStatusCode": 200
    }
  }

  entelPre2() {

    return {
      "codigo": "OK",
      "opstatus": 0,
      "objeto": [
        {
          "accion": "ACTION02",
          "idService": 2,
          "pasosTotales": 2,
          "pasoActual": 1,
          "tipoServicio": "Prepago",
          "nombre": "",
          "componentes": [
            {
              "nombre": "",
              "tipo": "table",
              "atributos": [{
                "dependiente": "",
                "camposDependientes": [],
                "textInputMode": "",
                "parametro": "seleccione",
                "maxTextLength": "",
                "minTextLength": "",
                "restrictCharactersSet": "",
                "formatCaracterSet": "",
                "placeholder": "",
                "secureTextEntry": "",
                "selectDependienteParametro": "",
                "values": [{
                  "lblT0": { "text": "Código", "isVisible": "true" },

                  "lblV0": { "text": "67047288", "parametro": "codCuenta", "isVisible": "true" },

                  "lblT1": { "text": "Saldo", "isVisible": "true" },

                  "lblV1": { "text": "102.07", "parametro": "saldo", "isVisible": "true" },

                  "lblT2": { "text": "Monto Mínimo", "isVisible": "true" },

                  "lblV2": { "text": "5", "parametro": "montoMinimo", "isVisible": "true" },

                  "lblT3": { "text": "descripcion", "isVisible": "false" },

                  "lblV3": { "text": "PRE_PAGO_MOVIL", "parametro": "descripcion", "isVisible": "false" },

                  "lblT4": { "text": "idSeguimiento", "parametro": "", "isVisible": "false" },

                  "lblV4": { "text": "20021", "parametro": "idSeguimiento", "isVisible": "false" },

                  "lblT5": { "text": "tipoCuenta", "parametro": "", "isVisible": "false" },

                  "lblV5": { "text": "TC_PREMOV", "parametro": "tipoCuenta", "isVisible": "false" },

                  "lblT6": { "text": "tipoCuentaDescripcion", "parametro": "", "isVisible": "false" },

                  "lblV6": { "text": "PRE_PAGO_MOVIL", "parametro": "tipoCuentaDescripcion", "isVisible": "false" }
                }
                ],
                "multipleSeleccion": "false",
                "titulo": "Seleccione una Cuenta",
              }]
            },
            {
              "nombre": "",    // este campo representara un label para la estructura tipo input podrá colocar el mensaje a mostrar  aquí .      
              "tipo": "label",
              "atributos": [{
                "dependiente": "",
                "camposDependientes": [],
                "selectDependienteParametro": "",
                "textInputMode": "",
                "parametro": "",
                "maxTextLength": "",
                "minTextLength": "",
                "restrictCharactersSet": "",
                "formatCaracterSet": "",
                "placeholder": "",
                "secureTextEntry": "",
                "values": [],
                "multipleSeleccion": "",
                "titulo": "Introduzca sus datos para facturación y el monto :"
              }]
            },
            {
              "nombre": "Razón Social ",    // este campo representara un label para la estructura tipo input podrá colocar el mensaje a mostrar  aquí .      
              "tipo": "input",
              "atributos": [{
                "dependiente": "",
                "camposDependientes": [],
                "selectDependienteParametro": "",
                "textInputMode": "A",
                "parametro": "razónSocial",
                "maxTextLength": "30",
                "minTextLength": "5",
                "restrictCharactersSet": "~!@#$%^&*()_+=-`,<>/?;:'[]{}|/0123456789",
                "formatCaracterSet": "",
                "placeholder": "Introduzca la Razón Social",
                "secureTextEntry": "",
                "values": [],
                "multipleSeleccion": "",
                "titulo": "Introduzca su cuenta :"
              }]
            },
            {
              "nombre": "NIT (ej. 8330408)",    // este campo representara un label para la estructura tipo input podrá colocar el mensaje a mostrar  aquí .      
              "tipo": "input",
              "atributos": [{
                "dependiente": "",
                "camposDependientes": [],
                "selectDependienteParametro": "",
                "textInputMode": "N",
                "parametro": "nit",
                "maxTextLength": "10",
                "minTextLength": "7",
                "restrictCharactersSet": "~!@#$%^&*()_+=-`.,<>/?;:'[]{}|/asdfghjklqwertyuiopzxcvbnmñáéíúóQWERTYUIOPASDFGHJKLZXCVBNMÁÉÚÍÓÑ",
                "formatCaracterSet": "^[\\d]+$",
                "placeholder": "Introduzca el Monto",
                "secureTextEntry": "",
                "values": [],
                "multipleSeleccion": "",
                "titulo": "Introduzca su cuenta :"
              }]
            },
            {
              "nombre": "Monto (ej. 25.02)",    // este campo representara un label para la estructura tipo input podrá colocar el mensaje a mostrar  aquí .      
              "tipo": "input",
              "atributos": [{
                "dependiente": "",
                "camposDependientes": [],
                "selectDependienteParametro": "",
                "textInputMode": "N",
                "parametro": "monto",
                "maxTextLength": "10",
                "minTextLength": "8",
                "restrictCharactersSet": "~!@#$%^&*()_+=-`,<>/?;:'[]{}|/asdfghjklqwertyuiopzxcvbnmñáéíúóQWERTYUIOPASDFGHJKLZXCVBNMÁÉÚÍÓÑ",
                "formatCaracterSet": "^[+]?([0-9]+(?:[\\.][0-9]*)?|\\.[0-9]+)$",
                "placeholder": "Monto",
                "secureTextEntry": "",
                "values": [],
                "multipleSeleccion": "",
                "titulo": "Introduzca su cuenta :"
              }]
            }

          ]

        }
      ],
      "mensaje": "Exito en la TransacciÃ³n",
      "httpStatusCode": 200
    }
  }

  entelPre3() {

    return {
      "codigo": "OK",
      "opstatus": 0,
      "objeto": [
        {
          "accion": "ACTION03",
          "idService": 2,
          "pasosTotales": 2,
          "pasoActual": 2,
          "tipoServicio": "Prepago",
          "nombre": "",
          "componentes": [
            {
              "nombre": "",
              "tipo": "table",
              "atributos": [{
                "dependiente": "",
                "camposDependientes": [],
                "textInputMode": "",
                "parametro": "seleccione",
                "maxTextLength": "",
                "minTextLength": "",
                "restrictCharactersSet": "",
                "formatCaracterSet": "",
                "placeholder": "",
                "secureTextEntry": "",
                "selectDependienteParametro": "",
                "values": [{
                  "lblT0": { "text": "Nro", "isVisible": "true" },

                  "lblV0": { "text": "67047288", "parametro": "nro", "isVisible": "true" },

                  "lblT1": { "text": "Monto", "isVisible": "true" },

                  "lblV1": { "text": "25", "parametro": "monto", "isVisible": "true" },

                  "lblT2": { "text": "Periodo", "isVisible": "true" },

                  "lblV2": { "text": "", "parametro": "periodo", "isVisible": "true" },

                  "lblT3": { "text": "", "isVisible": "false" },

                  "lblV3": { "text": "20021", "parametro": "idSeguimiento", "isVisible": "false" },

                  "lblT4": { "text": "", "parametro": "", "isVisible": "false" },

                  "lblV4": { "text": "8330408", "parametro": "nit", "isVisible": "false" },

                  "lblT5": { "text": "", "parametro": "", "isVisible": "false" },

                  "lblV5": { "text": "TC_PREMOV", "parametro": "tipoCuenta", "isVisible": "false" },

                  "lblT6": { "text": "", "parametro": "", "isVisible": "false" },

                  "lblV6": { "text": "Yampasi", "parametro": "razonSocial", "isVisible": "false" },
                }, {
                  "lblT0": { "text": "Nro", "isVisible": "true" },

                  "lblV0": { "text": "111333222", "parametro": "nro", "isVisible": "true" },

                  "lblT1": { "text": "Monto", "isVisible": "true" },

                  "lblV1": { "text": "50", "parametro": "monto", "isVisible": "true" },

                  "lblT2": { "text": "Periodo", "isVisible": "true" },

                  "lblV2": { "text": "", "parametro": "periodo", "isVisible": "true" },

                  "lblT3": { "text": "", "isVisible": "false" },

                  "lblV3": { "text": "20022", "parametro": "idSeguimiento", "isVisible": "false" },

                  "lblT4": { "text": "", "parametro": "", "isVisible": "false" },

                  "lblV4": { "text": "8330999", "parametro": "nit", "isVisible": "false" },

                  "lblT5": { "text": "", "parametro": "", "isVisible": "false" },

                  "lblV5": { "text": "TC_PREMOV", "parametro": "tipoCuenta", "isVisible": "false" },

                  "lblT6": { "text": "", "parametro": "", "isVisible": "false" },

                  "lblV6": { "text": "Yampasimo", "parametro": "razonSocial", "isVisible": "false" },
                }

                ],
                "multipleSeleccion": "true",
                "titulo": "Seleccione una Cuenta",
              }]
            }

          ]

        }
      ],
      "mensaje": "Exito en la TransacciÃ³n",
      "httpStatusCode": 200
    }
  }

  entelPost1() {
    return {
      "codigo": "OK",
      "opstatus": 0,
      "objeto": [
        {
          "accion": "ACTION01",
          "idService": "2",
          "pasosTotales": "3",
          "pasoActual": "1",
          "tipoServicio": "Prepago",
          "nombre": "",
          "componentes": [
            {
              "nombre": "",
              "tipo": "table_select",
              "atributos": [{
                "values": [
                  {
                    "lblV2": {
                      "parametro": " codParam ",
                      "isVisible": "false",
                      "text": "text Line3"
                    },
                    "lblV1": {
                      "parametro": "valorBusqueda",
                      "isVisible": "false",
                      "text": "001"
                    },
                    "lblV0": {
                      "parametro": "",
                      "isVisible": "true",
                      "text": "Cuenta"
                    }
                  },
                  {
                    "lblV2": {
                      "parametro": " codParam ",
                      "isVisible": "false",
                      "text": "text Line3"
                    },
                    "lblV1": {
                      "parametro": "valorBusqueda",
                      "isVisible": "false",
                      "text": "002"
                    },
                    "lblV0": {
                      "parametro": "",
                      "isVisible": "true",
                      "text": "Prueba Texto"
                    }
                  }
                ],
                "formatCaracterSet": "",
                "minTextLength": "",
                "placeholder": "",
                "maxTextLength": "",
                "idPtnConfirmacion": "Tipo de busqueda",
                "selectDependienteParametro": "valorBusqueda",
                "parametro": "seleccion",
                "secureTextEntry": "",
                "camposDependientes": [],
                "textInputMode": "",
                "restrictCharactersSet": "",
                "multipleSeleccion": "false",
                "dependiente": "",
                "titulo": "Tipo de Busqueda "
              }]
            },
            {
              "nombre": "Busqueda",
              "tipo": "input",
              "atributos": [{
                "values": [],
                "formatCaracterSet": "",
                "minTextLength": 0,
                "idPtnConfirmacion": "Codigo Fijó",
                "placeholder": "Seleccione tipo de filtro",
                "maxTextLength": 0,
                "selectDependienteParametro": "",
                "parametro": "",
                "secureTextEntry": "false",
                "camposDependientes": [
                  {
                    "formatCaracterSet": "",
                    "placeholder": "Introduzca Código",
                    "minTextLength": 8,
                    "valorBusqueda": "001",
                    "maxTextLength": 10,
                    "textInputMode": "N",
                    "parametro": "codAcceso",
                    "restrictCharactersSet": "~!@#$%^&*()_+=-`.,<>/?;:'[]{}|/asdfghjklqwertyuiopzxcvbnmñáéíúóQWERTYUIOPASDFGHJKLZXCVBNMÁÉÚÍÓÑ",
                    "secureTextEntry": "true"
                  },
                  {
                    "formatCaracterSet": "",
                    "placeholder": "Ingresar datos solo texto",
                    "minTextLength": 5,
                    "valorBusqueda": "002",
                    "maxTextLength": 10,
                    "textInputMode": "A",
                    "parametro": "Introduzca  PruebaTexto",
                    "restrictCharactersSet": "~!@#$%^&*()_+=-`,<>/?;:'[]{}|/0123456789",
                    "secureTextEntry": "false"
                  }
                ],
                "textInputMode": "",
                "restrictCharactersSet": "",
                "multipleSeleccion": "",
                "dependiente": "true",
                "titulo": ""
              }]
            }
          ]
        }
      ],
      "mensaje": "Exito en la TransacciÃ³n",
      "httpStatusCode": 200
    }
  }

  entelPost2() {

    return {
      "codigo": "OK",
      "opstatus": 0,
      "objeto": [
        {
          "accion": "ACTION02",
          "idService": "2",
          "pasosTotales": "3",
          "pasoActual": "2",
          "tipoServicio": "Prepago",
          "nombre": "",
          "componentes": [
            {
              "nombre": "",
              "tipo": "table",
              "atributos": [{
                "dependiente": "",
                "camposDependientes": [],
                "textInputMode": "",
                "parametro": "seleccione",
                "maxTextLength": "",
                "minTextLength": "",
                "idPtnConfirmacion": "Servicio",
                "restrictCharactersSet": "",
                "formatCaracterSet": "",
                "placeholder": "",
                "secureTextEntry": "",
                "selectDependienteParametro": "",
                "values": [{

                  "lblT0": { "text": "Monto Deuda", "isVisible": "true" },
                  "lblV0": { "text": "67047288", "parametro": "montoDeuda", "isVisible": "true" },

                  "lblT1": { "text": "nombre", "isVisible": "true" },
                  "lblV1": { "text": "TROPICAL TOURS LTDA.", "parametro": "nombre", "isVisible": "true" },

                  "lblT2": { "text": "cuenta", "isVisible": "true" },
                  "lblV2": { "text": "30193917", "parametro": "cuenta", "isVisible": "true" },

                  "lblT3": { "text": "tipoCuenta", "isVisible": "false" },
                  "lblV3": { "text": "TC?POST", "parametro": "tipoCuenta", "isVisible": "false" },

                  "lblT4": { "text": "idSeguimiento", "parametro": "", "isVisible": "false" },
                  "lblV4": { "text": "20019", "parametro": "idSeguimiento", "isVisible": "false" },

                  "lblT5": { "text": "descripcion", "parametro": "", "isVisible": "false" },
                  "lblV5": { "text": "TROPICAL TOURS LTDA.", "parametro": "descripcion", "isVisible": "false" },

                  "lblT6": { "text": "codCuenta", "parametro": "", "isVisible": "false" },
                  "lblV6": { "text": "30193917", "parametro": "codCuenta", "isVisible": "false" },

                }
                ],
                "multipleSeleccion": "false",
                "titulo": "Seleccione una Cuenta",
              }]
            },

          ]
        }
      ],
      "mensaje": "Exito en la TransacciÃ³n",
      "httpStatusCode": 200
    }
  }

  entelPost3() {

    return {
      "codigo": "OK",
      "opstatus": 0,
      "objeto": [
        {
          "accion": "ACTION03",
          "idService": "2",
          "pasosTotales": "3",
          "pasoActual": "3",
          "tipoServicio": "Postpago",
          "envioFactura": "false",
          "moneda": "BOB",
          "monto": "monto",
          "campoMoneda": "BOB",
          "campoMonto": { "BOB": "montoBOB", "USD": "montoUSD" },
          "nombre": "",
          "componentes": [
            {
              "nombre": "",
              "tipo": "table",
              "atributos": [{
                "dependiente": "false",
                "camposDependientes": [],
                "textInputMode": "",
                "parametro": "seleccione",
                "idPtnConfirmacion": "Facturas o Deudas",
                "maxTextLength": "",
                "minTextLength": "",
                "restrictCharactersSet": "",
                "formatCaracterSet": "",
                "placeholder": "",
                "secureTextEntry": "",
                "selectDependienteParametro": "periodo",
                "values": [
                  {

                    "lblT0": { "text": "Nro", "isVisible": "true" },
                    "lblV0": { "text": "0000001", "parametro": "nro", "isVisible": "true" },

                    "lblT1": { "text": "Monto", "isVisible": "true" },
                    "lblV1": { "text": "100", "parametro": "monto", "isVisible": "true" },

                    "lblT2": { "text": "Periodo", "isVisible": "true" },
                    "lblV2": { "text": "01-2019", "parametro": "periodo", "isVisible": "true" },

                    "lblT3": { "text": "Mensaje", "isVisible": "false" },
                    "lblV3": { "text": "", "parametro": "mensaje", "isVisible": "false" },

                    "lblT4": { "text": "Nombre", "isVisible": "false" },
                    "lblV4": { "text": "20007", "parametro": "nombre", "isVisible": "false" },

                    "lblT5": { "text": "NIT", "isVisible": "false" },
                    "lblV5": { "text": "20007", "parametro": "nit", "isVisible": "false" },

                    "lblT6": { "text": "pruebaMO1", "isVisible": "false" },
                    "lblV6": { "text": "20", "parametro": "montoUSD", "isVisible": "false" },

                    "lblT7": { "text": "pruebaMO2", "isVisible": "false" },
                    "lblV7": { "text": "1000.5", "parametro": "montoBOB", "isVisible": "false" }

                  }, {

                    "lblT0": { "text": "Nro", "isVisible": "true" },
                    "lblV0": { "text": "0000002", "parametro": "nro", "isVisible": "true" },

                    "lblT1": { "text": "Monto", "isVisible": "true" },
                    "lblV1": { "text": "100", "parametro": "monto", "isVisible": "true" },

                    "lblT2": { "text": "Periodo", "isVisible": "true" },
                    "lblV2": { "text": "02-2019", "parametro": "periodo", "isVisible": "true" },

                    "lblT3": { "text": "Mensaje", "isVisible": "false" },
                    "lblV3": { "text": "", "parametro": "mensaje", "isVisible": "false" },

                    "lblT4": { "text": "Nombre", "isVisible": "false" },
                    "lblV4": { "text": "20007", "parametro": "nombre", "isVisible": "false" },

                    "lblT5": { "text": "NIT", "isVisible": "false" },
                    "lblV5": { "text": "20007", "parametro": "nit", "isVisible": "false" },

                    "lblT6": { "text": "pruebaMO1", "isVisible": "false" },
                    "lblV6": { "text": "20", "parametro": "montoUSD", "isVisible": "false" },

                    "lblT7": { "text": "pruebaMO2", "isVisible": "false" },
                    "lblV7": { "text": "1000.5", "parametro": "montoBOB", "isVisible": "false" }

                  }, {

                    "lblT0": { "text": "Nro", "isVisible": "true" },
                    "lblV0": { "text": "0000003", "parametro": "nro", "isVisible": "true" },

                    "lblT1": { "text": "Monto", "isVisible": "true" },
                    "lblV1": { "text": "100", "parametro": "monto", "isVisible": "true" },

                    "lblT2": { "text": "Periodo", "isVisible": "true" },
                    "lblV2": { "text": "03-2019", "parametro": "periodo", "isVisible": "true" },

                    "lblT3": { "text": "Mensaje", "isVisible": "false" },
                    "lblV3": { "text": "", "parametro": "mensaje", "isVisible": "false" },

                    "lblT4": { "text": "Nombre", "isVisible": "false" },
                    "lblV4": { "text": "20007", "parametro": "nombre", "isVisible": "false" },

                    "lblT5": { "text": "NIT", "isVisible": "false" },
                    "lblV5": { "text": "20007", "parametro": "nit", "isVisible": "false" },

                    "lblT6": { "text": "pruebaMO1", "isVisible": "false" },
                    "lblV6": { "text": "20", "parametro": "montoUSD", "isVisible": "false" },

                    "lblT7": { "text": "pruebaMO2", "isVisible": "false" },
                    "lblV7": { "text": "1000.5", "parametro": "montoBOB", "isVisible": "false" }

                  }, {

                    "lblT0": { "text": "Nro", "isVisible": "true" },
                    "lblV0": { "text": "0000004", "parametro": "nro", "isVisible": "true" },

                    "lblT1": { "text": "Monto", "isVisible": "true" },
                    "lblV1": { "text": "100", "parametro": "monto", "isVisible": "true" },

                    "lblT2": { "text": "Periodo", "isVisible": "true" },
                    "lblV2": { "text": "04-2019", "parametro": "periodo", "isVisible": "true" },

                    "lblT3": { "text": "Mensaje", "isVisible": "false" },
                    "lblV3": { "text": "", "parametro": "mensaje", "isVisible": "false" },

                    "lblT4": { "text": "Nombre", "isVisible": "false" },
                    "lblV4": { "text": "20007", "parametro": "nombre", "isVisible": "false" },

                    "lblT5": { "text": "NIT", "isVisible": "false" },
                    "lblV5": { "text": "20007", "parametro": "nit", "isVisible": "false" },

                    "lblT6": { "text": "pruebaMO1", "isVisible": "false" },
                    "lblV6": { "text": "20", "parametro": "montoUSD", "isVisible": "false" },

                    "lblT7": { "text": "pruebaMO2", "isVisible": "false" },
                    "lblV7": { "text": "1000.5", "parametro": "montoBOB", "isVisible": "false" }

                  }, {

                    "lblT0": { "text": "Nro", "isVisible": "true" },
                    "lblV0": { "text": "0000005", "parametro": "nro", "isVisible": "true" },

                    "lblT1": { "text": "Monto", "isVisible": "true" },
                    "lblV1": { "text": "100", "parametro": "monto", "isVisible": "true" },

                    "lblT2": { "text": "Periodo", "isVisible": "true" },
                    "lblV2": { "text": "05-2019", "parametro": "periodo", "isVisible": "true" },

                    "lblT3": { "text": "Mensaje", "isVisible": "false" },
                    "lblV3": { "text": "", "parametro": "mensaje", "isVisible": "false" },

                    "lblT4": { "text": "Nombre", "isVisible": "false" },
                    "lblV4": { "text": "20007", "parametro": "nombre", "isVisible": "false" },

                    "lblT5": { "text": "NIT", "isVisible": "false" },
                    "lblV5": { "text": "20007", "parametro": "nit", "isVisible": "false" },

                    "lblT6": { "text": "pruebaMO1", "isVisible": "false" },
                    "lblV6": { "text": "20", "parametro": "montoUSD", "isVisible": "false" },

                    "lblT7": { "text": "pruebaMO2", "isVisible": "false" },
                    "lblV7": { "text": "1000.5", "parametro": "montoBOB", "isVisible": "false" }

                  }, {

                    "lblT0": { "text": "Nro", "isVisible": "true" },
                    "lblV0": { "text": "0000006", "parametro": "nro", "isVisible": "true" },

                    "lblT1": { "text": "Monto", "isVisible": "true" },
                    "lblV1": { "text": "100", "parametro": "monto", "isVisible": "true" },

                    "lblT2": { "text": "Periodo", "isVisible": "true" },
                    "lblV2": { "text": "06-2019", "parametro": "periodo", "isVisible": "true" },

                    "lblT3": { "text": "Mensaje", "isVisible": "false" },
                    "lblV3": { "text": "", "parametro": "mensaje", "isVisible": "false" },

                    "lblT4": { "text": "Nombre", "isVisible": "false" },
                    "lblV4": { "text": "20007", "parametro": "nombre", "isVisible": "false" },

                    "lblT5": { "text": "NIT", "isVisible": "false" },
                    "lblV5": { "text": "20007", "parametro": "nit", "isVisible": "false" },

                    "lblT6": { "text": "pruebaMO1", "isVisible": "false" },
                    "lblV6": { "text": "20", "parametro": "montoUSD", "isVisible": "false" },

                    "lblT7": { "text": "pruebaMO2", "isVisible": "false" },
                    "lblV7": { "text": "1000.5", "parametro": "montoBOB", "isVisible": "false" }

                  }, {

                    "lblT0": { "text": "Nro", "isVisible": "true" },
                    "lblV0": { "text": "0000007", "parametro": "nro", "isVisible": "true" },

                    "lblT1": { "text": "Monto", "isVisible": "true" },
                    "lblV1": { "text": "100", "parametro": "monto", "isVisible": "true" },

                    "lblT2": { "text": "Periodo", "isVisible": "true" },
                    "lblV2": { "text": "07-2019", "parametro": "periodo", "isVisible": "true" },

                    "lblT3": { "text": "Mensaje", "isVisible": "false" },
                    "lblV3": { "text": "", "parametro": "mensaje", "isVisible": "false" },

                    "lblT4": { "text": "Nombre", "isVisible": "false" },
                    "lblV4": { "text": "20007", "parametro": "nombre", "isVisible": "false" },

                    "lblT5": { "text": "NIT", "isVisible": "false" },
                    "lblV5": { "text": "20007", "parametro": "nit", "isVisible": "false" },

                    "lblT6": { "text": "pruebaMO1", "isVisible": "false" },
                    "lblV6": { "text": "20", "parametro": "montoUSD", "isVisible": "false" },

                    "lblT7": { "text": "pruebaMO2", "isVisible": "false" },
                    "lblV7": { "text": "1000.5", "parametro": "montoBOB", "isVisible": "false" }

                  },

                ],
                "multipleSeleccion": "true",
                "titulo": "Seleccione una Cuenta",
              }]
            },
          ]
        }
      ],
      "mensaje": "Exito en la TransacciÃ³n",
      "httpStatusCode": 200
    }
  }

  jsonFlujo1paso1() {
    return {
      "codigo": "OK",
      "opstatus": 0,
      "objeto": [
        {
          "siguiente": "ACUENTAS",
          "idService": 1,//id del servicio 
          "pasosTotales": 5,
          "pasoActual": 0,
          "nombre": "Buscar Suscriptor",
          "componentes": [{
            "nombre": "",
            "tipo": "label",
            "atributos": [{
              "dependiente": "",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "",
              "parametro": "",
              "maxTextLength": "",
              "minTextLength": "",
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "",
              "secureTextEntry": "",
              "values": [],
              "multipleSeleccion": "",
              "titulo": "Empresas Disponibles:"
            }]
          }]
        }
      ],
      "mensaje": "Exito en la TransacciÃ³n",
      "httpStatusCode": 200
    }
  }

  jsonFlujo1paso2() {

    return {
      "codigo": "OK",
      "opstatus": 0,
      "objeto": [
        {
          "siguiente": "ACUENTAS",
          "idService": 1,//id del servicio 
          "pasosTotales": 5,
          "pasoActual": 1,
          "nombre": "Buscar Suscriptor",
          "componentes": [{
            "nombre": "",
            "tipo": "label",
            "atributos": [{
              "dependiente": "",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "",
              "parametro": "",
              "maxTextLength": "",
              "minTextLength": "",
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "",
              "secureTextEntry": "",
              "values": [],
              "multipleSeleccion": "",
              "titulo": "Servicios Disponibles:"
            }]
          }, {
            "nombre": "Seleccionar Servicios:",
            "tipo": "table_list",
            "atributos": [{
              "dependiente": "",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "",
              "parametro": "seleccion",
              "maxTextLength": "",
              "minTextLength": "",
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "",
              "secureTextEntry": "",
              "values": [{
                "lblT0": { "text": "Servicio", "isVisible": "true" },

                "lblV0": { "text": "Telefonia", "parametro": "", "isVisible": "true" },

                "lblT1": { "text": "", "isVisible": "false" },

                "lblV1": { "text": "001", "parametro": "idServicio", "isVisible": "false" }

              }, {
                "lblT0": { "text": "Servicio", "isVisible": "true" },

                "lblV0": { "text": "Internet", "parametro": "", "isVisible": "true" },

                "lblT1": { "text": "Descripción", "isVisible": "false" },

                "lblV1": { "text": "002", "parametro": "idServicio", "isVisible": "false" }

              },
              {
                "lblT0": { "text": "Servicio", "isVisible": "true" },

                "lblV0": { "text": "Televisión", "parametro": "", "isVisible": "true" },

                "lblT1": { "text": "", "isVisible": "false" },

                "lblV1": { "text": "003", "parametro": "idServicio", "isVisible": "false" }

              }],
              "multipleSeleccion": "false",
              "titulo": ""
            }],
          }]
        }
      ],
      "mensaje": "Exito en la TransacciÃ³n",
      "httpStatusCode": 200
    }
  }

  jsonFlujo1paso3() {

    return {
      "codigo": "OK",
      "opstatus": 0,
      "objeto": [
        {
          "siguiente": "ACUENTAS",
          "idService": 1,//id del servicio 
          "pasosTotales": 5,
          "pasoActual": 2,
          "nombre": "Buscar Suscriptor",
          "componentes": [{
            "nombre": "",
            "tipo": "label",
            "atributos": [{
              "dependiente": "",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "",
              "parametro": "",
              "maxTextLength": "",
              "minTextLength": "",
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "",
              "secureTextEntry": "",
              "values": [],
              "multipleSeleccion": "",
              "titulo": "Tipos de servicios disponibles:"
            }]
          }, {
            "nombre": "Seleccionar tipo servicios:",
            "tipo": "table_list",
            "atributos": [{
              "dependiente": "",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "",
              "parametro": "seleccion",
              "maxTextLength": "",
              "minTextLength": "",
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "",
              "secureTextEntry": "",
              "values": [{
                "lblT0": { "text": "Tipo Servicio", "isVisible": "true" },

                "lblV0": { "text": "Prepago", "parametro": "", "isVisible": "true" },

                "lblT1": { "text": "", "isVisible": "false" },

                "lblV1": { "text": "001", "parametro": "idTipoServicio", "isVisible": "false" }

              }, {
                "lblT0": { "text": "Tipo Servicio", "isVisible": "true" },

                "lblV0": { "text": "Postpago", "parametro": "", "isVisible": "true" },

                "lblT1": { "text": "Descripción", "isVisible": "false" },

                "lblV1": { "text": "002", "parametro": "idTipoServicio", "isVisible": "false" }

              }],
              "multipleSeleccion": "false",
              "titulo": ""
            }],
          }]
        }
      ],
      "mensaje": "Exito en la TransacciÃ³n",
      "httpStatusCode": 200
    }
  }

  jsonFlujo1paso4() {

    return {
      "codigo": "OK",
      "opstatus": 0,
      "objeto": [
        {
          "siguiente": "ACUENTAS",
          "idService": 1,//id del servicio 
          "pasosTotales": 5,
          "pasoActual": 3,
          "nombre": "Busqueda Suscriptor",
          "componentes": [{
            "nombre": "",
            "tipo": "label",
            "atributos": [{
              "dependiente": "",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "",
              "parametro": "",
              "maxTextLength": "",
              "minTextLength": "",
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "",
              "secureTextEntry": "",
              "values": [],
              "multipleSeleccion": "",
              "titulo": "Introduzca su código fijo:"
            }]
          }, {
            "nombre": "Código fijo:",
            "tipo": "input",
            "atributos": [{
              "dependiente": "false",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "N",
              "parametro": "codAcceso",
              "maxTextLength": 5,
              "minTextLength": 5,
              "restrictCharactersSet": "!@#$%",
              "formatCaracterSet": "^\\d{5}$",
              "placeholder": "Ingresa 5 numeros",
              "secureTextEntry": "false",
              "values": [],
              "multipleSeleccion": "",
              "titulo": ""
            }],
          }, {
            "nombre": "Código otro:",
            "tipo": "input",
            "atributos": [{
              "dependiente": "false",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "A",
              "parametro": "codAcceso2",
              "maxTextLength": 5,
              "minTextLength": 5,
              "restrictCharactersSet": "!@#$%",
              "formatCaracterSet": "^\\w{3}er$",
              "placeholder": "Ingresa un formato xxx + er",
              "secureTextEntry": "false",
              "values": [],
              "multipleSeleccion": "",
              "titulo": ""
            }],
          }]
        }
      ],
      "mensaje": "Exito en la TransacciÃ³n",
      "httpStatusCode": 200
    }
  }

  jsonFlujo1paso5() {

    return {
      "codigo": "OK",
      "opstatus": 0,
      "objeto": [
        {
          "accion": "ADEUDAS",
          "idService": 5,
          "pasosTotales": 5,
          "pasoActual": 4,
          "nombre": "CRE PostPago",
          "componentes": [{
            "nombre": "",
            "tipo": "label",
            "atributos": [{
              "dependiente": "",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "",
              "parametro": "",
              "maxTextLength": "",
              "minTextLength": "",
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "",
              "secureTextEntry": "",
              "values": [],
              "multipleSeleccion": "",
              "titulo": "Seleccione una cuenta:"
            }]
          }, {
            "nombre": "",
            "tipo": "table",
            "atributos": [{
              "dependiente": "",
              "camposDependientes": [],
              "textInputMode": "",
              "parametro": "seleccione",
              "maxTextLength": "",
              "minTextLength": "",
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "",
              "secureTextEntry": "",
              "selectDependienteParametro": "codCuenta",
              "values": [{
                "lblT0": { "text": "Código cuenta", "isVisible": "true" },

                "lblV0": { "text": "614063", "parametro": "codCuenta", "isVisible": "true" },

                "lblT1": { "text": "Descripción", "isVisible": "true" },

                "lblV1": { "text": "BALCAZAR GIL EDGAR", "parametro": "descripcion", "isVisible": "true" },

                "lblT2": { "text": "Dirección", "isVisible": "true" },

                "lblV2": { "text": "SANTA ROSA DEL PALMAR   , UV: 0000- , MZ: 0000- ", "parametro": "direccion", "isVisible": "true" },

                "lblT3": { "text": "Mensaje", "isVisible": "true" },

                "lblV3": { "text": "", "parametro": "mensaje", "isVisible": "true" },

                "lblT4": { "text": "idSeguimiento", "parametro": "", "isVisible": "false" },

                "lblV4": { "text": "20063", "parametro": "idSeguimiento", "isVisible": "false" },

              },
              {
                "lblT0": { "text": "Código cuenta2", "isVisible": "true" },

                "lblV0": { "text": "614064", "parametro": "codCuenta", "isVisible": "true" },

                "lblT1": { "text": "Descripción2", "isVisible": "true" },

                "lblV1": { "text": "BALCAZAR GIL EDGAR", "parametro": "descripcion", "isVisible": "true" },

                "lblT2": { "text": "Dirección2", "isVisible": "true" },

                "lblV2": { "text": "SANTA ROSA DEL PALMAR   , UV: 0000- , MZ: 0000- ", "parametro": "direccion", "isVisible": "true" },

                "lblT3": { "text": "Mensaje2", "isVisible": "true" },

                "lblV3": { "text": "", "parametro": "mensaje", "isVisible": "true" },

                "lblT4": { "text": "idSeguimiento", "parametro": "", "isVisible": "false" },

                "lblV4": { "text": "20063", "parametro": "idSeguimiento", "isVisible": "false" },

              }
              ],
              "multipleSeleccion": "false",
              "titulo": "",
            }]
          },
          {
            "nombre": "Código fijo:",
            "tipo": "input",
            "atributos": [{
              "dependiente": "true",
              "camposDependientes": [{
                "codCuenta": "614063",
                "textInputMode": "A",
                "parametro": "codAcceso",
                "maxTextLength": 5,
                "minTextLength": 5,
                "restrictCharactersSet": "0123456789!@#$%",
                "formatCaracterSet": "",
                "placeholder": "Ingresar datos solo letras",
                "secureTextEntry": "false"
              },
              {
                "codCuenta": "614064",
                "textInputMode": "N",
                "parametro": "codAcceso",
                "maxTextLength": 10,
                "minTextLength": 5,
                "restrictCharactersSet": "qwertyuiopasdfghjklzxcvbnm,./[]{}-=_+!@#$%",
                "formatCaracterSet": "",
                "placeholder": "Ingresar datos solo numeros",
                "secureTextEntry": "true"
              },
              ],
              "selectDependienteParametro": "",
              "textInputMode": "",
              "parametro": "",
              "maxTextLength": 0,
              "minTextLength": 0,
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "Seleccion Cuenta",
              "secureTextEntry": "false",
              "values": [],
              "multipleSeleccion": "",
              "titulo": ""
            }],
          },
          {
            "nombre": "Código otro:",
            "tipo": "input",
            "atributos": [{
              "dependiente": "false",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "N",
              "parametro": "codAcceso2",
              "maxTextLength": 5,
              "minTextLength": 5,
              "restrictCharactersSet": "!@#$%",
              "formatCaracterSet": "",
              "placeholder": "Ingresar datos",
              "secureTextEntry": "false",
              "values": [],
              "multipleSeleccion": "",
              "titulo": ""
            }],
          }
          ]//finComponentes
        }
      ],
      "mensaje": "Exito en la TransacciÃ³n",
      "httpStatusCode": 200
    }
  }

  jsonFlujo1paso6() {

    return {
      "codigo": "OK",
      "opstatus": 0,
      "objeto": [
        {
          "accion": "APROCESAR",//no se ve en el flujo este valor
          "idService": 5,
          "pasosTotales": 5,
          "pasoActual": 5,
          "nombre": "CRE PostPago",
          "componentes": [{
            "nombre": "",
            "tipo": "label",
            "atributos": [{
              "dependiente": "",
              "camposDependientes": [],
              "selectDependienteParametro": "",
              "textInputMode": "",
              "parametro": "",
              "maxTextLength": "",
              "minTextLength": "",
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "",
              "secureTextEntry": "",
              "values": [],
              "multipleSeleccion": "",
              "titulo": "Seleccione una cuenta:"
            }]
          }, {
            "nombre": "",
            "tipo": "table",
            "atributos": [{
              "dependiente": "true",
              "camposDependientes": [],
              "selectDependienteParametro": "monto",
              "textInputMode": "",
              "parametro": "",
              "maxTextLength": "",
              "minTextLength": "",
              "restrictCharactersSet": "",
              "formatCaracterSet": "",
              "placeholder": "",
              "secureTextEntry": "",
              "values": [{
                "lblT0": { "text": "Nro", "isVisible": "true" },

                "lblV0": { "text": "010097235265", "parametro": "nro", "isVisible": "true" },

                "lblT1": { "text": "Monto", "isVisible": "true" },

                "lblV1": { "text": "283.50", "parametro": "monto", "isVisible": "true" },

                "lblT2": { "text": "Periodo", "isVisible": "true" },

                "lblV2": { "text": "08-2010", "parametro": "periodo", "isVisible": "true" },

                "lblT3": { "text": "", "isVisible": "false" },

                "lblV3": { "text": "20063", "parametro": "idSeguimiento", "isVisible": "false" },

                "lblT4": { "text": "", "parametro": "", "isVisible": "false" },

                "lblV4": { "text": "", "parametro": "nombre", "isVisible": "false" },

                "lblT5": { "text": "", "parametro": "", "isVisible": "false" },

                "lblV5": { "text": "", "parametro": "mensaje", "isVisible": "false" },

                "lblT6": { "text": "", "parametro": "", "isVisible": "false" },

                "lblV6": { "text": "", "parametro": "nit", "isVisible": "false" },

              },
              {
                "lblT0": { "text": "Nro", "isVisible": "true" },

                "lblV0": { "text": "010097829042", "parametro": "nro", "isVisible": "true" },

                "lblT1": { "text": "Monto", "isVisible": "true" },

                "lblV1": { "text": "77.30", "parametro": "monto", "isVisible": "true" },

                "lblT2": { "text": "Periodo", "isVisible": "true" },

                "lblV2": { "text": "07-2020", "parametro": "periodo", "isVisible": "true" },

                "lblT3": { "text": "", "isVisible": "false" },

                "lblV3": { "text": "20063", "parametro": "idSeguimiento", "isVisible": "false" },

                "lblT4": { "text": "", "parametro": "", "isVisible": "false" },

                "lblV4": { "text": "", "parametro": "nombre", "isVisible": "false" },

                "lblT5": { "text": "", "parametro": "", "isVisible": "false" },

                "lblV5": { "text": "", "parametro": "mensaje", "isVisible": "false" },

                "lblT6": { "text": "", "parametro": "", "isVisible": "false" },

                "lblV6": { "text": "", "parametro": "nit", "isVisible": "false" },

              },
              {
                "lblT0": { "text": "Nro", "isVisible": "true" },

                "lblV0": { "text": "010098495949", "parametro": "nro", "isVisible": "true" },

                "lblT1": { "text": "Monto", "isVisible": "true" },

                "lblV1": { "text": "323.90", "parametro": "monto", "isVisible": "true" },

                "lblT2": { "text": "Periodo", "isVisible": "true" },

                "lblV2": { "text": "10-2019", "parametro": "periodo", "isVisible": "true" },

                "lblT3": { "text": "", "isVisible": "false" },

                "lblV3": { "text": "20063", "parametro": "idSeguimiento", "isVisible": "false" },

                "lblT4": { "text": "", "parametro": "", "isVisible": "false" },

                "lblV4": { "text": "", "parametro": "nombre", "isVisible": "false" },

                "lblT5": { "text": "", "parametro": "", "isVisible": "false" },

                "lblV5": { "text": "", "parametro": "mensaje", "isVisible": "false" },

                "lblT6": { "text": "", "parametro": "", "isVisible": "false" },

                "lblV6": { "text": "", "parametro": "nit", "isVisible": "false" },

              },
              {
                "lblT0": { "text": "Nro", "isVisible": "true" },

                "lblV0": { "text": "010099100158", "parametro": "nro", "isVisible": "true" },

                "lblT1": { "text": "Monto", "isVisible": "true" },

                "lblV1": { "text": "354.90", "parametro": "monto", "isVisible": "true" },

                "lblT2": { "text": "Periodo", "isVisible": "true" },

                "lblV2": { "text": "02-2019", "parametro": "periodo", "isVisible": "true" },

                "lblT3": { "text": "", "isVisible": "false" },

                "lblV3": { "text": "20063", "parametro": "idSeguimiento", "isVisible": "false" },

                "lblT4": { "text": "", "parametro": "", "isVisible": "false" },

                "lblV4": { "text": "", "parametro": "nombre", "isVisible": "false" },

                "lblT5": { "text": "", "parametro": "", "isVisible": "false" },

                "lblV5": { "text": "", "parametro": "mensaje", "isVisible": "false" },

                "lblT6": { "text": "", "parametro": "", "isVisible": "false" },

                "lblV6": { "text": "", "parametro": "nit", "isVisible": "false" },

              },
              {
                "lblT0": { "text": "Nro", "isVisible": "true" },

                "lblV0": { "text": "010099625027", "parametro": "nro", "isVisible": "true" },

                "lblT1": { "text": "Monto", "isVisible": "true" },

                "lblV1": { "text": "311.80", "parametro": "monto", "isVisible": "true" },

                "lblT2": { "text": "Periodo", "isVisible": "true" },

                "lblV2": { "text": "05-2019", "parametro": "periodo", "isVisible": "true" },

                "lblT3": { "text": "", "isVisible": "false" },

                "lblV3": { "text": "20063", "parametro": "idSeguimiento", "isVisible": "false" },

                "lblT4": { "text": "", "parametro": "", "isVisible": "false" },

                "lblV4": { "text": "", "parametro": "nombre", "isVisible": "false" },

                "lblT5": { "text": "", "parametro": "", "isVisible": "false" },

                "lblV5": { "text": "", "parametro": "mensaje", "isVisible": "false" },

                "lblT6": { "text": "", "parametro": "", "isVisible": "false" },

                "lblV6": { "text": "", "parametro": "nit", "isVisible": "false" },

              }
              ],
              "multipleSeleccion": "true",
              "titulo": "",
            }]
          }]//finComponentes
        }
      ],
      "mensaje": "Exito en la TransacciÃ³n",
      "httpStatusCode": 200
    }
  }

}

module.exports = dataPruebas;
class manejarArchivos {

    constructor() {
        this.formatoRespaldo = { personas: [], empresas: [] };
        this.rutaArchivo = `dinamico/paraAbril.json`;
        this.file = require("fs");
    }

    generarPersonas() {

        let dataServicio = [
            {
                estructura: ["nombre", "apellido", "Cedula", "titulos"],
                data: ["Javier", "Molines", "26645922", ["Informatica", "Odontologia", "Mecanica", "Misticos"]]
            },
            {
                estructura: ["nombre", "apellido", "Cedula", "titulos"],
                data: ["Abril", "Guevara", "19999999", ["Ingeniera de Sistemas"]]
            },
            {
                estructura: ["nombre", "apellido", "Cedula", "titulos"],
                data: ["Roxana", "Guevara", "25495762", ["Turismo", "Arrecha", "Habilidosa"]]
            },
            {
                estructura: ["nombre", "apellido", "Cedula", "titulos"],
                data: ["Gabriela", "Tellez", "26969696", ["Puta", "Loca", "Perra"]]
            }
        ];

        this.crearNuevo(dataServicio, this.formatoRespaldo, 0);

    }

    generarEmpresas() {

        let dataServicio = [
            {
                estructura: ["nombre", "valoracion"],
                data: ["OREO", "$1.000.000.000"]
            },
            {
                estructura: ["nombre", "valoracion"],
                data: ["YOUTUBE", "$1.000.000"]
            }
        ];

        this.crearNuevo(dataServicio, this.formatoRespaldo, 1);

    }

    crearNuevo(dataServicio, objetoRespaldar, flujo) {

        let itemRespaldar   = Object.keys(objetoRespaldar);
        let objetoUbicacion = itemRespaldar[flujo];
        let dataCreada      = this.leerExistente(this.rutaArchivo);

        for (let contenidoServicio of dataServicio) {
            let objeto = {};
            for (let index = 0; index < contenidoServicio.estructura.length; index++) {
                let arreglo = contenidoServicio.estructura;
                let arreDat = contenidoServicio.data;
                for (let destino = 0; destino < contenidoServicio.estructura.length; destino++) {
                    let clave = arreglo[destino];
                    let valor = arreDat[destino];
                    objeto[clave] = valor;
                }
            }
            objetoRespaldar[objetoUbicacion].push(objeto);
        }

        for (let iteradorContenidoRespaldo of itemRespaldar) {
            let instanciaObjeto = objetoRespaldar[iteradorContenidoRespaldo];
            let instanciaRespal = dataCreada[iteradorContenidoRespaldo];
            instanciaObjeto = instanciaRespal.concat(instanciaObjeto);
        }

        this.guardarData(objetoRespaldar);

    }

    guardarData(data) {

        let guardar = JSON.stringify(data, null, '\t');
        this.file.writeFileSync(this.rutaArchivo, guardar);

    }

    limpiarRespaldo() {

        this.guardarData(this.formatoRespaldo);

    }

    leerExistente(nombreArchivo) {

        let objeto = {};
        let dataArchivo = this.file.readFileSync(nombreArchivo, "utf8");
        let dataTipo = typeof (dataArchivo);
        dataArchivo = dataTipo === "string" ? dataArchivo.replace(/\n/g, "").replace(/\t/g, "") : dataArchivo;

        try {
            objeto = JSON.parse(dataArchivo);
        } catch (error) {
            console.log("ERROR AL TRANSFORMAR CONTENIDO: " + error);
        }

        return objeto;
    }

}

module.exports = manejarArchivos;
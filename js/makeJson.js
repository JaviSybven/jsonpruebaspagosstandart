class makeJson {

    constructor() {
        const crearDataCableada = require("./jsonExample.js");
        this.file = require("fs");
        this.muestreoJson = new crearDataCableada();
        this.global = {
            "enviarFactura": "true",
            "moneda": "USD",
            "monto": "monto",
            "campoMoneda": null,//"USD",
            "campoMonto": null//{BOB: "montoBOB", USD: "montoUSD"}
        };
    }

    pruebasDelPan() {

        let data = this.muestreoJson.arreglarPorCulpaDeGenteQueNoCoopera();
        let resultado = this.validacionServicio(data);
        let totales = resultado.dataArreglada.objeto[0].componentes;
        let enviar = {
            datos: []
        };

        // BUSCAR ELEMENTOS DENTRO DEL COMPONENTE PARA SABER QUE ESTA CREANDO
        for (let widget of totales) {
            let detalle_tipo = widget.tipo;

            // SOLO DISPARAR FLUJOS DE LISTADOS
            if (detalle_tipo !== "label" && detalle_tipo !== "input") {

                let detalles = widget.atributos[0];
                let tipo_compo = detalles.multipleSeleccion === "true" ? "ListMultiple" : detalle_tipo === "table_select" ? "table_select" : "table";
                detalles.tipo = tipo_compo;
                let final = this.prepararDatosSegmentoSimple(detalles, detalles.tipo);
                enviar.datos.push(final);
            }
        }

        let paraMostrar = JSON.stringify(enviar, null, "\t");
        console.log(paraMostrar);

    }

    detectarDiferencias(data1, data2) {
        let noDetectados = [];
        let claveDeData1 = Object.keys(data1);
        let claveDeData2 = Object.keys(data2);
        for (let elementoData1 of claveDeData1) {
          let detectado = false;
          for (let elementoData2 of claveDeData2) {
            if (elementoData1 === elementoData2) { // NO LO AGREGA
              detectado = true;
              break;
            }
          }
          if (detectado !== true) {
            noDetectados.push(elementoData1);
          }
        }
        return noDetectados;
      }

    pruebasGenerarContenido() {

        let deta = this.muestreoJson.ghost();
        let resultado = this.generarDataNuevaRequest("id2", "4", "table_list", deta);
        console.log(resultado);

    }

    generarDataNuevaRequest(parametroBuscador, parametroValues, tipoComponente, contenidoTotal) {

        kony.print("JAV - generarDataNuevaRequest - parametroBuscador: " + parametroBuscador);
        kony.print("JAV - generarDataNuevaRequest - parametroValues: " + parametroValues);
        kony.print("JAV - generarDataNuevaRequest - tipoComponente: " + tipoComponente);
        kony.print("JAV - generarDataNuevaRequest - contenidoTotal");
        jsonJavier(contenidoTotal);

        let enviar = {};
        let detectado = false;
        let componentes = contenidoTotal.objeto[0].componentes;
        let filtrado = componentes.filter(informacion => informacion.tipo === tipoComponente);

        kony.print("JAV - generarDataNuevaRequest - filtrado");
        jsonJavier(filtrado);

        for (let data of filtrado) {
            let valorNormal = "";
            let valores = [];

            kony.print("JAV - generarDataNuevaRequest - convertir json");
            try {
                valorNormal = data.atributos[0].values;
                valores = JSON.parse(valorNormal);
            } catch (error) {
                //kony.print("JAV - CONVERTIR ERRADO: " + error);
            }

            kony.print("JAV - generarDataNuevaRequest - buscando data");
            for (let valor of valores) {
                kony.print("JAV - generarDataNuevaRequest - valor dentro de JSON transformado");
                jsonJavier(valor);
                let label = Object.keys(valor);
                for (let contador = 0; contador < label.length; contador++) {
                    let objetoKey = label[contador];
                    let objetoValor = valor[objetoKey];
                    if (parametroBuscador === objetoValor.parametro && parametroValues === objetoValor.text) {
                        enviar = valor;
                        detectado = true;
                        kony.print("JAV - generarDataNuevaRequest - DETECTADO");
                        break;
                    }
                }
                if (detectado) break;
            }
            if (detectado) break;
        }

        return enviar;

    }

    construirDataPorPaso() {

        let enviar = {};
        let arregloGeneral = _ControllerPrincipal.arregloRespaldoPasos;
        let pasoActual = _ControllerPrincipal.pasoActual;
        let porAgregar = _ControllerPrincipal.componentePorAgregar;
        let resServicePrev = porAgregar[0].objeto[0];
        let requestPasos = formateoPrincipalRequest(_ControllerPrincipal.dataRequestXpaso);
        let ultimoPaso = sencilloRequest(requestPasos, "r");
        let respaldo = {
            paso: pasoActual,
            objeto: {
                entrePasos: true,
                accion: resServicePrev.accion,
                idservicio: resServicePrev.idService,
                values: JSON.stringify(ultimoPaso)//invocar funcion con data estructura nombre valor
            }
        };

        if (arregloGeneral.length > 0) {
            //for busque dentro de arreglogeneral el paso actual y retorne el objeto
            for (let contenido of arregloGeneral) {
                if (pasoActual == contenido.paso) {
                    enviar = contenido.objeto;
                    break;
                }
            }
        } else {
            enviar = respaldo.objeto;
            _ControllerPrincipal.arregloRespaldoPasos.push(respaldo);
        }

        return enviar;

    }

    obtenerSiguientePasoServicio() {

        let pagoEstandar = kony.mvc.MDAApplication.getSharedInstance().getModuleManager().getModule("StandardPaymentModule");
        let obj = construirDataPorPaso();
        pagoEstandar.presentationController.consumirGetFormulario(obj);

    };

    minimoMaximoInformacionDeSegmento(texto) {

        //kony.os.deviceInfo().deviceHeight+"x"
        let maximo = 0;
        let validador = 1000;
        let ancho = kony.os.deviceInfo().deviceWidth;
        let limite = texto.length;
        let dispositivo = applicationManager.getPresentationFormUtility().getDeviceName();
        let enviar = "";
        let arregloAncho = {
            limiteIphone: {
                maxCaracter: 44,
                minCaracter: 36
            },
            limiteAndroid:
            {
                maxCaracter: 40,
                minCaracter: 36
            }
        };

        if (ancho > validador) {// es grande
            switch (dispositivo) {
                case "iPhone":
                    maximo = arregloAncho.limiteIphone.maxCaracter;
                    break;
                default:
                    maximo = arregloAncho.limiteAndroid.maxCaracter;
                    break;
            }
        } else {// es pequeño
            switch (dispositivo) {
                case "iPhone":
                    maximo = arregloAncho.limiteIphone.minCaracter;
                    break;

                default:
                    maximo = arregloAncho.limiteAndroid.minCaracter;
                    break;
            }
        }

        if (limite > maximo) {
            enviar = limite >= maximo ? texto.slice(0, maximo - 3) + "..." : texto;
        } else {
            enviar = texto;
        }

        return enviar;
    }

    recorteMonto(monto) {
        let montoT = monto.replace(/,/ig, "");
        let trans = parseFloat(montoT);
        return trans;
    }

    probarFiltro() {

        let ejemplo = [{
            "mostrar": false,
            "lblTitle": "",
            "lblValue1": "29,051.45 45841676 - 45840761 201312",
            "lblValue2": {
                "isVisible": false
            },
            "titulos": "Monto Factura Periodo"
        }];

        let retornar = this.agregarFiltroArregloCaracter(ejemplo, "Bs");
        console.log(retornar);

    }

    testing() {

        let porPas = this.muestreoJson.dataConsuscripcion02();
        let primerCorte = this.formateoPrincipalRequest(porPas);
        let informacion = this.sencilloRequest(primerCorte, "s");
        let string = JSON.stringify(informacion, null, "\t");
        console.log(string);

    }

    codificadorHTMLString(objeto) {
        let encode = JSON.stringify(objeto.values);
        encode = encode.replace(/\\\"/ig, "&#92;&#92;&#34;");
        encode = encode.replace(/\"/ig, "&#92;&#34;");
        objeto.values = encode;
        return objeto;
    }

    decodificarHTMLString(objeto) {
        let encode = objeto.values.replace(/&#92;&#92;&#34;/ig, '\\\"');
        encode = encode.replace(/&#92;&#34;/ig, '\"');
        objeto.values = encode;
        let sss = JSON.parse(encode);
        console.log(sss);
        return objeto;
    }

    probartestearRespuestaGetFormulario() {

        let fino = this.muestreoJson.dataProbarValidacionObjetoResponseGetFormularioCorrecto();
        let pruebas = {
            "codigo": "0",
            "opstatus": 0,
            "objeto": [
                {
                    "componentes": []
                }
            ],
            "mensaje": "Debe completar el campo 'accion'.",
            "httpStatusCode": 200
        };

        let result = this.validacionServicio(fino);
        console.log(result);

    }

    validacionServicio(respuesta) {

        let paraRetornar = { isValid: false, dataArreglada: {} };
        let propiedadesObjetoInterno = ["accion", "idService", "pasosTotales", "tipoServicio", "pasoActual", "envioFactura", "moneda", "monto", "campoMoneda", "campoMonto", "nombre", "componentes"];
        let propiedadesObjeto = ["objeto", "codigo", "mensaje", "httpStatusCode"];
        let total = propiedadesObjeto.length;
        let contadorKeysPropiedades = 0;

        if (typeof (respuesta) === "object" && Array.isArray(respuesta) === false) {
            let contenido = Object.keys(respuesta);
            for (let stringKeys of contenido) {
                for (let validator of propiedadesObjeto) {
                    if (validator === stringKeys) {
                        if (propiedadesObjeto[0] === stringKeys) { // VALIDAR DATA para atributo objeto 
                            let instaObjeto = respuesta[stringKeys];
                            if (typeof (instaObjeto) === "object") {
                                respuesta[stringKeys] = [instaObjeto];
                                //if (Array.isArray(instaObjeto) === true && instaObjeto.length == 1) {
                                let principal = respuesta[propiedadesObjeto[0]][0];
                                let contenidoInterno = Object.keys(principal).length;
                                let totalDelInterno = propiedadesObjetoInterno.length;
                                let instanciaComponente = principal[propiedadesObjetoInterno[propiedadesObjetoInterno.length - 1]];
                                if (contenidoInterno === totalDelInterno && instanciaComponente.length > 0) {
                                    for (let index = 0; index < instanciaComponente.length; index++) {
                                        let cadaComponente = instanciaComponente[index];
                                        instanciaComponente[index].atributos = [cadaComponente.atributos];
                                    }
                                    contadorKeysPropiedades++;
                                }
                                //}
                            }

                        } else { // como la validacion fue positiva sumar contador
                            contadorKeysPropiedades++;
                        }
                        break;
                    }
                }
            }
        }

        paraRetornar.dataArreglada = respuesta;
        paraRetornar.isValid = contadorKeysPropiedades == total ? true : false;
        return paraRetornar;
    }

    probarFiltroPropiedades() {

        let validar = ["nombre", "apellido", "edad", "estado", "ocupacion", "kekkei"];
        let obj = {

            nombre: "Javier",
            apellido: "Molines",
            edad: "23",
            ocupacion: "respirar",
            estado: "ocupado",
            kekkei: "rinnengan"

        };

        this.filtroPropiedades(obj, validar);
    }

    filtroPropiedades(obj, validar) {

        let ghost = { modificado: false, resultado: obj };
        let info = Object.keys(ghost.resultado);
        for (let interno of info) {
            let paraModificar = false;
            for (let detalle of validar) {
                if (detalle === interno) {
                    paraModificar = true;
                    break;
                }
            }
            if (paraModificar === false) {
                ghost.modificado = true;
                ghost.resultado[interno] = "";
            }
        }

        return ghost;
    }

    probarFiltroArregloCaracterVisibilidad() {

        // SAGUAPACK - 912933000
        let conSus = [];
        let requestPorPaso = this.muestreoJson.pruebasNuevoSimple();
        let formato1 = this.formateoPrincipalRequestVista(requestPorPaso);
        let formato2Vista = this.sencilloRequestVista(formato1, "dataS");
        conSus = this.generarEstructuraConfirmar(formato2Vista);
        let dataPrueba = this.verificarVisibilidad(requestPorPaso, conSus);
        //let ultima = this.agregarFiltroArregloCaracter(dataPrueba.complejos, "USD");
        console.log(dataPrueba);

    }

    probarFiltroRequest() {

        let arregloPruebas = this.muestreoJson.dataProbarNuevoFiltro();
        let primerCorte = this.formateoPrincipalRequest(arregloPruebas);
        let ghost = primerCorte[5].data.valor;
        let filString = JSON.stringify(ghost);
        //let requestFiltrado = this.sencilloRequest(primerCorte, "r");

    }

    sencilloRequest(data, tipo) {
        let enviar = [];
        switch (tipo) {
            case "s": // cuando se requiera llenar el caso de pListadoSuscripcion
                for (let consumo of data) {
                    //console.log(consumo);
                    if (consumo.accion === "ACUENTAS" || consumo.accion === "ADEUDAS") {
                        enviar.push(consumo.data);
                    }
                }
                break;
            case "r": // cuando se requiera el pagoDinamico ( DEVUELVE LA DATA DEL ULTIMO PASO )
                let limite = 0;
                let filtrado = data.filter(objeto => objeto.paso == limite);
                for (let contenido of filtrado) {
                    enviar.push(contenido.data);
                }
                break;
            default:
                for (let consumo of data) {
                    enviar.push(consumo.data);
                }
                break;
        }
        return enviar;
    }

    formateoPrincipalRequest(data) {
        let enviar = [];
        for (let informacion of data) {
            let nuevoArreglo = {
                "paso": informacion.paso,
                "tipo": informacion.tipo,
                "idComp": informacion.idComp,
                "data": { "nombre": informacion.data.nombre, "valor": [{ "select": [] }] }
            }
            switch (nuevoArreglo.tipo) {
                case "texbox":
                    let resNombre = informacion.data[0].nombre;
                    let resValor = informacion.data[0].valor;
                    nuevoArreglo.data = { "nombre": resNombre, "valor": resValor };
                    break;
                default:
                    let dataGeneral = informacion.data.valor[0].select;
                    for (let info of dataGeneral) {
                        let objetoNuevo = {};
                        let objetoArreglo = Object.keys(info);

                        for (let parametroObjetoArreglo of objetoArreglo) {
                            if (parametroObjetoArreglo === "nombre" || parametroObjetoArreglo === "valor" || parametroObjetoArreglo === "accion") {
                                objetoNuevo[parametroObjetoArreglo] = info[parametroObjetoArreglo];
                            }
                        }

                        let limiteTotalObjetoNuevo = Object.keys(objetoNuevo).length;
                        if (limiteTotalObjetoNuevo > 0) {
                            nuevoArreglo.data.valor[0].select.push(objetoNuevo);
                        }
                    }
                    break;
            }
            enviar.push(nuevoArreglo);
        }
        return enviar;
    }

    verificarVisibilidad(arregloJSON, conContenidoSuscripcion) {

        let mostrar = { simples: conContenidoSuscripcion, complejos: [] };

        for (let a = 0; a < arregloJSON.length; a++) {
            let interno = arregloJSON[a];
            let flujoTable = interno.tipo;
            let datos = interno.data;

            if (flujoTable === "texbox") {
                datos.valor = [{ select: [{ "titulo": datos[0].nombre, "valor": datos[0].valor, "tituloVista": datos[0].tituloVista }] }];
            }

            if (datos.hasOwnProperty("valor") === true && Array.isArray(datos.valor) === true) {
                for (let conta = 0; conta < datos.valor.length; conta++) {
                    let detalle = datos.valor[conta];

                    if (detalle.hasOwnProperty("select") === true && Array.isArray(detalle.select) === true && detalle.select.length > 0) {

                        let cadena1 = "";
                        let cadena2 = "";
                        let arregloBuscador = [];
                        let comidaJson = {};
                        let tituloVista = "";
                        let contenidoIndicador = "";

                        if (flujoTable === "ListMultiple" || flujoTable === "table_list" || flujoTable === "ListSimple") {
                            switch (flujoTable) {
                                case "table_list":
                                case "ListSimple":
                                case "ListMultiple":
                                    arregloBuscador = detalle.select.filter(elemento => elemento.visible === true);
                                    break;
                                default:
                                    break;
                            }
                        } else {
                            arregloBuscador = detalle.select;
                        }

                        if (arregloBuscador.length > 0) {
                            for (let contador = 0; contador < arregloBuscador.length; contador++) {
                                let elemento = arregloBuscador[contador];
                                contenidoIndicador = elemento.tituloVisible;
                                if (elemento.valor !== "") {
                                    let unificador = ">";
                                    let filtradoValor = elemento.valor.trim().replace(/ /ig, unificador);
                                    let filtradoTitulo = elemento.titulo.trim().replace(/ /ig, unificador);
                                    cadena1 += filtradoTitulo;
                                    cadena2 += filtradoValor;
                                    if (contador !== arregloBuscador.length - 1) {
                                        cadena1 += " ";
                                        cadena2 += " ";
                                    }
                                }
                            }// end for arregloBuscador.length

                            tituloVista = arregloBuscador[0].tituloVista;

                            switch (flujoTable) {
                                case 'table_select':
                                    comidaJson = {
                                        "mostrar": true,
                                        "lblTitle": tituloVista,
                                        "lblValue": contenidoIndicador
                                    };
                                    mostrar.simples.push(comidaJson);
                                    break;
                                case 'texbox':
                                    comidaJson = {
                                        "mostrar": true,
                                        "lblTitle": tituloVista,
                                        "lblValue": cadena2
                                    };
                                    mostrar.simples.push(comidaJson);
                                    break;
                                case "ListMultiple":
                                    comidaJson = {
                                        "mostrar": true,
                                        "lblTitle": tituloVista,
                                        "lblValue1": { isVisible: false },
                                        "lblValue2": cadena2,
                                        "titulos": cadena1
                                    };
                                    mostrar.complejos.push(comidaJson);
                                    break;
                                default:
                                    comidaJson = {
                                        "mostrar": true,
                                        "lblTitle": tituloVista,
                                        "lblValue1": { isVisible: false },
                                        "lblValue2": cadena2
                                    };
                                    mostrar.complejos.push(comidaJson);
                                    break;
                            }
                        }
                    }
                }// end for datos.valor cantidad deudas
            }
        }// end for existe

        return mostrar;

    }

    agregarFiltroArregloCaracter(data, reemplazo) {

        let retornar = [];
        let sustituto = { mostrar: true, lblTitle: "", lblValue1: "", lblValue2: "" };
        let mensaje = "";
        let tituloF = "";
        let haddler = " ";
        let getTitu = "lblTitle";
        let itemTit = "titulos";
        let itemVal = "lblValue2";
        let testReg = new RegExp("monto", "ig");
        let agregar = reemplazo;
        let arreglo = data;

        // RECORRER ARREGLO PRINCIPAL PARA DIVIDIR POR ITEM
        for (let countPrincipal = 0; countPrincipal < arreglo.length; countPrincipal++) {

            let posVal = [];
            let item = arreglo[countPrincipal];
            let esMonto = false;

            // VALIDAR EXISTENCIA DEL TITULO Y VALOR PARA CONFIRMAR QUE CUMPLE CON LOS REQUISITOS
            if (typeof (item) === "object" && item.hasOwnProperty(itemTit) === true && item.hasOwnProperty(itemVal) === true) {
                let original = item[itemTit];
                let picado = original.split(haddler);
                // CADA ITEM APLICA UN SPLIT PARA PICAR POR ELEMENTOS ANIDADOS
                for (let countSecundario = 0; countSecundario < picado.length; countSecundario++) {
                    let texto = picado[countSecundario];
                    // SI EL TEXTO COINCIDE CON EL PARAMETRO SOLICITADO SE CAPTURA LA POSICION PARA ANEXAR MAS TARDE EL CURRENCY
                    if (testReg.test(texto) === true) {
                        posVal.push(countSecundario);
                        esMonto = true;
                    }
                }
            }

            if (esMonto === false) {

                let respaV1 = item.lblValue1;
                let respaV2 = item.lblValue2;
                item.lblValue1 = respaV2.replace(/>/ig, " ");
                item.lblValue2 = respaV1;
                item.lblTitle = this.validarTituloMostrarConfirmacion(item.lblTitle);
                retornar.push(item);

            } else {

                tituloF = this.validarTituloMostrarConfirmacion(item[getTitu]);
                let cadena = item[itemVal].replace(/ - /ig, "*");
                cadena = cadena.trim();
                let auxiliar = cadena.split(haddler);
                for (let posicion of posVal) {
                    auxiliar[posicion] = agregar + auxiliar[posicion];
                }
                let unificado = auxiliar.join(" | ");
                unificado = unificado.replace(/\*/ig, " - ");
                unificado = unificado.replace(/>/ig, " ");
                // VALIDAR TEXTO CAIMAN PARA DISPOSITIVOS
                //unificado = this.minimoMaximoInformacionDeSegmento(unificado);
                mensaje += unificado;
                if (countPrincipal != arreglo.length - 1) {
                    mensaje += "\n";
                }
            }
        }

        sustituto.lblTitle = tituloF;
        sustituto.lblValue1 = mensaje;
        sustituto.lblValue2 = { isVisible: false };
        retornar.push(sustituto);

        return retornar;
    }

    formateoPrincipalRequestVista(data) {

        let enviar = [];
        for (let informacion of data) {
            // console.log(informacion);
            let nuevoArreglo = {
                "visibles": informacion.hasOwnProperty("dataVisible") ? informacion.dataVisible : {},
                "accion": informacion.accion,
                "paso": informacion.paso,
                "tipo": informacion.tipo,
                "idComp": informacion.idComp,
                "data": {
                    "nombre": informacion.data.nombre,
                    "valor": [{
                        "select": []
                    }]
                }
            };
            switch (nuevoArreglo.tipo) {
                case "texbox":
                    let resNombre = informacion.data[0].nombre;
                    let resValor = informacion.data[0].valor;
                    nuevoArreglo.data = {
                        "nombre": resNombre,
                        "valor": resValor
                    };
                    break;
                default:
                    let dataGeneral = informacion.data.valor[0].select;
                    for (let info of dataGeneral) {

                        let objetoNuevo = {};
                        let objetoArreglo = Object.keys(info);

                        for (let parametroObjetoArreglo of objetoArreglo) {
                            if (parametroObjetoArreglo === "nombre" || parametroObjetoArreglo === "valor" || parametroObjetoArreglo === "accion" || parametroObjetoArreglo === "tituloVista") {
                                objetoNuevo[parametroObjetoArreglo] = info[parametroObjetoArreglo];
                            }
                        }

                        let limiteTotalObjetoNuevo = Object.keys(objetoNuevo).length;
                        if (limiteTotalObjetoNuevo > 0) {
                            nuevoArreglo.data.valor[0].select.push(objetoNuevo);
                        }
                    }
                    break;
            }
            enviar.push(nuevoArreglo);
        }
        return enviar;
    }

    sencilloRequestVista(data, tipo) {
        let enviar = [];
        switch (tipo) {
            case "dataS": // cuando se requiera llenar el caso de pListadoSuscripcion
                for (let Sinsuscrip of data) {
                    if ((Sinsuscrip.accion !== "ACUENTAS" && Sinsuscrip.accion !== "ADEUDAS" && Sinsuscrip.accion !== "APROCESAR")
                        && (Sinsuscrip.paso == 1 || Sinsuscrip.paso == 2 || Sinsuscrip.paso == 3)) {

                        let estructura = {
                            parametro: Sinsuscrip.data.nombre,
                            nombre: Sinsuscrip.data.valor[0].select[0].nombre,
                            valor: Sinsuscrip.data.valor[0].select[0].valor,
                            tituloVista: Sinsuscrip.data.valor[0].select[0].tituloVista,
                            //dataOriginal:Sinsuscrip.data,
                            textoVisible: Sinsuscrip.visibles.lblV0.text
                        }
                        enviar.push(estructura);
                    }
                }
                break;

            default:
                for (let consumo of data) {
                    enviar.push(consumo.data);
                }
                break;
        }
        return enviar;
    }

    validarTituloMostrarConfirmacion(textoIngresado) {
        let meandoF = "idPtnConfirmacion vacio: ";
        if (typeof (textoIngresado) === "string" && textoIngresado.trim() !== "") {
            meandoF = textoIngresado;
        }
        return meandoF;
    }

    generarEstructuraConfirmar(dataInformacion) {
        let retornar = [];
        for (let iterator of dataInformacion) {
            let comidaJson = {
                "mostrar": true,
                "lblTitle": iterator.tituloVista,
                "lblValue": iterator.textoVisible
            };
            retornar.push(comidaJson);
        }
        return retornar;
    }

    formatearCaracteres(arregloFormatear) {

        let respaldo = [];

        try {

            for (let i = 0; i < arregloFormatear.length; i++) {

                let item = arregloFormatear[i];
                let picado = item.lblValue2.split("\n");
                let arreglo = "";

                for (let contador = 0; contador < picado.length; contador++) {
                    let item = picado[contador];
                    let resultado = this.cortarMaximoCaracteres(item);
                    if (contador !== 0) {
                        arreglo += "\n";
                    }
                    arreglo += resultado;
                }

                item.lblValue2 = arreglo;

            }

            respaldo = arregloFormatear;

        } catch (error) {
            this.manejadorExplosivos(error, "formatearCaracteres");
        }

        return respaldo;

    }

    cortarMaximoCaracteres(textoRecibido) {
        let enviar = "";
        let limite = textoRecibido.length;
        if (limite >= 23) {
            enviar = textoRecibido.slice(0, 20) + "...";
        } else {
            enviar = textoRecibido;
        }
        return enviar;
    }

    ejecutar_flujo_preparar_datos_segmento() {

        let arrayFlow = ["entelPost", "entelPre", "saguaPost"];
        //let arrayFlow = ["dataQueMeDejoLoco"];
        let detalles = this.generar_historial_ejecutado();

        for (let generar of arrayFlow) {
            let mostrar = {
                autor: detalles.autor,
                fecha: detalles.hoy,
                hora: detalles.hora,
                conte: [],
            };

            //let vueltas = generar === arrayFlow[arrayFlow.length - 1] ? 2 : 6;
            let vueltas = 3;
            for (let contador = 1; contador <= vueltas; contador++) {

                let indicador = `${generar}${contador}`;
                let primario = this.muestreoJson[indicador]();
                let totales = primario.objeto[0].componentes;
                let enviar = {
                    flujo: indicador,
                    datos: []
                };

                // BUSCAR ELEMENTOS DENTRO DEL COMPONENTE PARA SABER QUE ESTA CREANDO
                for (let widget of totales) {
                    let detalle_tipo = widget.tipo;

                    // SOLO DISPARAR FLUJOS DE LISTADOS
                    if (detalle_tipo !== "label" && detalle_tipo !== "input") {

                        let detalles = widget.atributos[0];
                        let tipo_compo = detalles.multipleSeleccion === "true" ? "ListMultiple" : detalle_tipo === "table_select" ? "table_select" : "table";
                        detalles.tipo = tipo_compo;
                        let final = this.prepararDatosSegmentoSimple(detalles, detalles.tipo);
                        enviar.datos.push(final);
                    }
                }

                mostrar.conte.push(enviar);
            }

            let nameFile = `dinamico/dinamico${generar}.json`;
            let guardar = JSON.stringify(mostrar, null, '\t');
            this.file.writeFileSync(nameFile, guardar);
        }
    }

    prepararDatosSegmentoSimple(data, tipoComponente) {

        let datosSeg = [];
        let validar = Array.isArray(data.values);
        let datos = validar === true ? data.values : JSON.parse(data.values);

        for (let contenido of datos) {
            try {
                let enviar = this.elementosInternos(contenido, data, tipoComponente);
                datosSeg.push(enviar);
            } catch (error) {
                //this.manejadorExplosivos(error, "prepararDatosSegmentoSimple");
            }
        }

        return datosSeg;
    }

    elementosInternos(objetoPrincipal, componente, tipoComponente) {

        let limite = this.generarLimiteArreglo(objetoPrincipal);
        let elementos = this.generarContenidoDinamicoJSON(limite, tipoComponente);
        let json = this.crearJson(elementos, objetoPrincipal, componente);
        let modificado = this.agregarContenidoInexistenteDinamico(json, componente);
        let jsonToggle = this.toggleFlxSeperator(modificado);
        let final = componente.multipleSeleccion == "true" ? this.cambiarMonedaMostrar(jsonToggle) : jsonToggle;

        return final;

    }

    generarLimiteArreglo(objetoPrincipal) {

        let limite = 0;
        let keysInternos = Object.keys(objetoPrincipal);
        let valuInternos = Object.values(objetoPrincipal);
        let totalKeys = keysInternos.length;
        let totalValu = valuInternos.length;

        if (totalKeys === totalValu) {
            limite = totalKeys;
        } else {
            if (totalKeys > totalValu) {
                limite = totalValu;
            } else {
                limite = totalKeys;
            }
        }

        return limite;

    }

    generarContenidoDinamicoJSON(parametro, flujo) {

        let limite = parametro;
        let prefijo = "lbl";
        let flujos = ["T", "V"];
        let elementos = [];

        if (flujo === "table_select") {
            flujos = flujos.slice(1);
        }

        for (let i = 0; i <= limite; i++) {
            for (let elemento of flujos) {

                let texto = prefijo + elemento + i;
                elementos.push(texto);

            }
        }

        return elementos;

    }

    crearJson(propiedades, respuestaServicio, componente) {

        let enviar = {};
        let original = "{";
        let aux = respuestaServicio;
        let claves = Object.keys(aux);

        for (let contador = 0; contador < claves.length; contador++) {

            let elemento = propiedades[contador];
            let item = aux[elemento];

            // VALIDAR QUE TODO SE CUMPLA PARA DETECTAR ELEMENTOS A TRABAJAR
            if (item !== null && item !== "" && typeof (item) === "object" && Array.isArray(item) === false) {

                let texto = `""`;
                let parametro = "";
                let visible = false;

                if (item.hasOwnProperty("text") === true && item.text !== "") {
                    let validar = item.text;
                    let filtro = /^{.*}$/ig;
                    texto = filtro.test(validar) === true ? JSON.stringify(validar) : `"${validar}"`;
                }

                if (item.hasOwnProperty("parametro") === true && item.parametro !== "") {
                    parametro = item.parametro;
                }

                if (item.hasOwnProperty("isVisible") === true && item.isVisible !== "" && item.isVisible === "true") {
                    visible = true;
                }

                let modificador = `"${elemento}":{"text":${texto},"parametro":"${parametro}","isVisible":${visible}},`;
                original += modificador;

            }
        }

        original += `"montoAuxiliar":{},`;

        if (componente.multipleSeleccion === true || componente.multipleSeleccion == "true") {
            original += `"imgSelect":"checkinactivo.png"}`;
        } else {
            original += `"imgSelect":"radiobuttoninactivo.png"}`;
        }

        try {
            enviar = JSON.parse(original);
        } catch (error) {
            //this.manejadorExplosivos(error, "crearJson");
        }

        return enviar;
    }

    nuevoObtenerMonto(parametro){
        let globalMoneda = this.global;//_ControllerPrincipal.datosGlobales;
        let parametroDeCaptura = this.obtenerParametroMontoBuscador(globalMoneda);
        let retornar = parametro === parametroDeCaptura ? true : false;
        return retornar;        
    }

    toggleFlxSeperator(modificado) {
        let mostrar = false;
        let objetos = Object.keys(modificado);
        for (let i = 0; i < objetos.length; i++) {
            let contenido = modificado[objetos[i]];
            if (contenido.hasOwnProperty("isVisible") === true && contenido.isVisible === true) {
                mostrar = true;
                i = objetos.length;
            }
        }
        modificado.flxSeperator = { isVisible: mostrar };
        return modificado;
    }

    agregarContenidoInexistenteDinamico(jsonCreado, com) {

        let totalJson = Object.keys(jsonCreado);
        let prefijo = "lbl";
        let flujos = ["T", "V"];
        let vueltas = 15;
        let limitJson = totalJson.length - 2;

        if (com.tipo === "table_select") {
            flujos = ["V"];
            vueltas = 5;
        } else {
            limitJson /= 2;
        }

        for (let i = limitJson; i <= vueltas; i++) {
            for (let elemento of flujos) {

                let texto = prefijo + elemento + i;
                jsonCreado[texto] = { isVisible: false };

            }
        }

        return jsonCreado;
    }

    cambiarMonedaMostrar(jsonCompleto) {

        let globalMoneda = this.global;
        let parametro = this.obtenerParametroMontoBuscador(globalMoneda);
        let totales = Object.keys(jsonCompleto);

        // VALIDACION PARAMETRO
        for (let intera of totales) {
            let itemObjeto = jsonCompleto[intera];
            if (itemObjeto.parametro === parametro) {
                for (let elemento of totales) {
                    let monto = jsonCompleto[elemento];
                    if (monto.parametro === globalMoneda.monto) {
                        let aux = monto;
                        jsonCompleto['montoAuxiliar'].text = aux.text;
                        jsonCompleto['montoAuxiliar'].parametro = aux.parametro;
                        jsonCompleto[elemento].text = itemObjeto.text;
                        break;
                    }
                }
            }
        }

        return jsonCompleto;

    }

    obtenerParametroMontoBuscador(globalMoneda) {
        let parametro = "";
        if (globalMoneda.campoMoneda === null) {
            if (globalMoneda.campoMonto !== null) {
                parametro = validarPorFlujo(globalMoneda.moneda);
            } else {
                parametro = globalMoneda.monto;
            }
        } else {
            if (globalMoneda.campoMonto !== null) {
                parametro = validarPorFlujo(globalMoneda.campoMoneda);
            } else {
                parametro = globalMoneda.monto;
            }
        }
        return parametro;
    }

    validarPorFlujo(criterio) {

        let globalMoneda = _ControllerPrincipal.datosGlobales;
        let parametroDeCaptura = "";

        if (globalMoneda.campoMonto !== null && Object.keys(globalMoneda.campoMonto).length > 0) {
            let keyMonto = Object.keys(globalMoneda.campoMonto);
            for (let elemento of keyMonto) {
                if (elemento === criterio) {
                    parametroDeCaptura = globalMoneda.campoMonto[elemento];
                }
            }
        } else {
            parametroDeCaptura = globalMoneda.monto;
        }

        return parametroDeCaptura;

    }

    generar_historial_ejecutado() {

        let hoy = new Date();
        let dia = hoy.getDate();
        let mes = hoy.getMonth() + 1;
        let año = hoy.getFullYear();
        let fecha = `${dia}/${mes}/${año}`;
        let hora = `${hoy.getHours()}:${hoy.getMinutes()}:${hoy.getSeconds()}`;

        let propiedades = {
            hoy: fecha,
            autor: "Javier Molines",
            hora: hora
        };

        return propiedades;
    }

    ordenarArregloPorFechaAlternativo(contenido) {

        contenido.sort((primero, segundo) => {

            let cuenta = 0;
            let ordenamiento = { direccionA: 1, direccionB: -1 };
            let detallesOne = primero.lblV2.text.split("-");
            let detallesTwo = segundo.lblV2.text.split("-");
            let mesPrimero = parseInt(detallesOne[0]);
            let mesSegundo = parseInt(detallesTwo[0]);
            let añoPrimero = parseInt(detallesOne[1]);
            let añoSegundo = parseInt(detallesTwo[1]);

            if (añoPrimero > añoSegundo) {
                cuenta = ordenamiento.direccionA;
            } else if (añoPrimero < añoSegundo) {
                cuenta = ordenamiento.direccionB;
            }

            if (cuenta == 0) {
                if (mesPrimero > mesSegundo) {
                    cuenta = ordenamiento.direccionA;
                } else {
                    cuenta = ordenamiento.direccionB;
                }
            }

            return cuenta;

        });

    }

    manejadorExplosivos(error, explosivo) {

        console.log(`JAV - Mori en >>> ${explosivo} por >>> ${error}`);

    }

}

module.exports = makeJson;